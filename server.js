module.exports = function(){
  var app = {};
  //http://expressjs.com/
  app.express     = require('express');
  // http://www.embeddedjs.com/
  app.ejs = require('ejs');
  //https://nodejs.org/api/path.html
  app.path      = require('path');
  //https://nodejs.org/api/http.html#http_http
  app.http      = require('http');
  //https://github.com/expressjs/morgan
  app.morgan          = require('morgan');
  //Esse é o que deixa eu usar req.body https://github.com/expressjs/body-parser
  app.bodyParser      = require('body-parser');
  //https://github.com/expressjs/method-override
  app.methodOverride  = require('method-override');
  //https://www.npmjs.com/package/bcryptjs
  app.bcrypt = require('bcryptjs');
  //https://github.com/auth0/node-jsonwebtoken
  app.jwt = require('jsonwebtoken');
  //https://github.com/nodemailer/nodemailer
  app.nodeMailer = require('nodemailer');
  //arquivo de criptografia 
  app.crypto = require('crypto');
  // Arquivo de configuracoes
  app.config = require('./config')(app.nodeMailer);
  // CORS
  app.cors = require('cors');

  //Db
  var db = {};
  db.mongo = require('mongodb').MongoClient;
  db.mongoose = require('mongoose');
  db.mongoose.connect(app.config.mongoURI());
  db.mongoose.connection.once('error', console.error);
  db.mongoose.connection.db.once('open', function() {
    console.log("Mongo Connected");
  });

  //Schema
  var schema = {};
  schema.mongoose = db.mongoose;
  schema.account = require(__dirname + '/models/account.js')(db.mongoose);
  schema.user = require(__dirname + '/models/user.js')(db.mongoose);
  schema.desafio = require(__dirname + '/models/desafio.js')(db.mongoose);
  schema.quiz = require(__dirname + '/models/quiz.js')(db.mongoose);
  schema.empresa = require(__dirname + '/models/empresa.js')(db.mongoose);
  schema.conteudo = require(__dirname + '/models/conteudo.js')(db.mongoose);
  schema.chat = require(__dirname + '/models/chat.js')(db.mongoose);
  schema.likes = require(__dirname + '/models/likes.js')(db.mongoose);
  schema.comments = require(__dirname + '/models/comment.js')(db.mongoose);
 
  //Modulo User
  var user = {};
  user.controllers = {};
  user.controllers.user = require(__dirname + '/modules/usuario/user/user-controller.js')(schema, app.bcrypt);
  
  //Modulo Ranking
  var rank = {};
  rank.controllers = {};
  rank.controllers.rank = require(__dirname + '/modules/usuario/rank/rank-controller.js')(schema);

  //Modulo feed
  var feed = {};
  feed.controllers = {};
  feed.controllers.feed = require(__dirname + '/modules/usuario/feed/feed-controller.js')(schema);


  //Modulo Account
  var account = {};
  account.controllers = {};
  account.controllers.createAccounts = require(__dirname + 
    '/modules/admin/account/create-accounts-controller.js')(schema, app.bcrypt, app.config, app.crypto, user);
  account.controllers.login = require(__dirname + 
    '/modules/usuario/account/login-controller.js')(schema, app.bcrypt, app.jwt, app.config);
  account.controllers.changePassword = require(__dirname + 
    '/modules/usuario/account/password-controller.js')(schema, app.bcrypt, app.config, app.crypto);
  account.controllers.resetPassword = require(__dirname + 
    '/modules/usuario/account/password-controller.js')(schema, app.bcrypt, app.config, app.crypto);
  account.controllers.deleteAccounts = require(__dirname + 
    '/modules/admin/account/delete-account-controller.js')(schema, app.bcrypt, app.config, app.crypto, user);

  //Modulo Autenticacao
  var autenticacao = {};
  autenticacao.controllers = {};
  autenticacao.controllers.autenticacao = require(__dirname + 
    '/modules/autenticacao/autenticacao-controller.js')(app.jwt, app.config);

  //Modulo Conteudo de Usuário
  var conteudo = {};
  conteudo.controllers = {};
  conteudo.controllers.conteudo = require(__dirname + '/modules/usuario/conteudo/conteudo-controller.js')(schema);  
  conteudo.controllers.conteudoAdm = require(__dirname + '/modules/admin/conteudo/conteudo-controller.js')(schema);  

  //Modulo Desafio
  var desafio = {};
  desafio.controllers = {};
  desafio.controllers.desafio = require(__dirname + '/modules/usuario/desafio/desafio-controller.js')(schema);  
  
  //Modulo Quiz
  var quiz = {};
  quiz.controllers = {};
  quiz.controllers.quiz = require(__dirname + '/modules/usuario/quiz/quiz-controller.js')(schema);  
  quiz.controllers.quizAdm = require(__dirname + '/modules/admin/quiz/quiz-controller.js')(schema);  

  //Modulo Empresa
  var empresa = {};
  empresa.controllers = {};
  empresa.controllers.empresa = require(__dirname + '/modules/admin/empresa/empresa-controller.js')(schema); 

  //Modulo Chat
  var chat = {};
  chat.controllers = {};
  chat.controllers.chat = require(__dirname + '/modules/usuario/chat/chat-controller.js')(schema, app.config);

  //Modulo Empresa User
  var empresaUser = {};
  empresaUser.controllers = {};
  empresaUser.controllers.empresaUser = require(__dirname + '/modules/usuario/empresa/empresaUser-controller.js')(schema, app.config);

  //Modulo Likes
  var likes = {};
  likes.controllers = {};
  likes.controllers.likes = require(__dirname + '/modules/usuario/likes/likes-controller.js')(schema, app.config);

  //Modulo Commnets
  var comments = {};
  comments.controllers = {};
  comments.controllers.comments = require(__dirname + '/modules/usuario/comments/comments-controller.js')(schema, app.config);

  //Modulo Chat Empresa
  var chatEmpresa = {};
  chatEmpresa.controllers = {};
  chatEmpresa.controllers.chatEmpresa = require(__dirname + '/modules/admin/chat/chat-controller.js')(schema);

  //Modulo s3Config
  var s3Config = {};
  s3Config.controllers = {};
  s3Config.controllers.s3Config = require(__dirname + '/modules/usuario/s3Config/s3Config-controller.js')(app.crypto, app.config);

  //Modulo Relatorio
  var relatorio = {};
  relatorio.controllers = {};
  relatorio.controllers.relatorio = require(__dirname + '/modules/admin/relatorio/relatorio-controller.js')(schema);  

  //Rotas
  var routes = {};
  // No auth
  routes.routes = require(__dirname + '/routes/router.js')(app.express, routes, autenticacao);
  routes.v1_no_auth = {};
  routes.v1_no_auth.account = require(__dirname + '/routes/v1-no-auth/account.js')(account);
  routes.v1_no_auth.s3Config = require(__dirname + '/routes/v1-no-auth/s3Config.js')(s3Config);
  routes.v1_no_auth.reset_password = require(__dirname + '/routes/v1-no-auth/reset-password.js')(account);

  // Auth comum
  routes.v1_auth = {};
  routes.v1_auth.user = require(__dirname + '/routes/v1-auth/user.js')(user);
  routes.v1_auth.desafios = require(__dirname + '/routes/v1-auth/desafios.js')(desafio);
  routes.v1_auth.quiz = require(__dirname + '/routes/v1-auth/quiz.js')(quiz);
  routes.v1_auth.password = require(__dirname + '/routes/v1-auth/password.js')(account);
  routes.v1_auth.conteudo = require(__dirname + '/routes/v1-auth/conteudo.js')(conteudo);
  routes.v1_auth.feed = require(__dirname + '/routes/v1-auth/feed.js')(feed);
  routes.v1_auth.rank = require(__dirname + '/routes/v1-auth/rank.js')(rank);
  routes.v1_auth.chat = require(__dirname + '/routes/v1-auth/chat.js')(chat);
  routes.v1_auth.likes = require(__dirname + '/routes/v1-auth/likes.js')(likes);
  routes.v1_auth.comments = require(__dirname + '/routes/v1-auth/comments.js')(comments);
  routes.v1_auth.empresaUser = require(__dirname + '/routes/v1-auth/empresa.js')(empresaUser);


  // Auth Admin
  routes.v1_auth_admin = {};
  routes.v1_auth_admin.account = require(__dirname + '/routes/v1-auth-admin/account.js')(account);
  routes.v1_auth_admin.empresa = require(__dirname + '/routes/v1-auth-admin/empresa.js')(empresa);
  routes.v1_auth_admin.conteudo = require(__dirname + '/routes/v1-auth-admin/conteudo.js')(conteudo);
  routes.v1_auth_admin.quiz = require(__dirname + '/routes/v1-auth-admin/quiz.js')(quiz);
  routes.v1_auth_admin.chatEmpresa = require(__dirname + '/routes/v1-auth-admin/chat.js')(chatEmpresa);
  routes.v1_auth_admin.relatorio = require(__dirname + '/routes/v1-auth-admin/relatorio.js')(relatorio);

  // FrontEnd
  routes.frontEnd = {};
  routes.frontEnd.views = require(__dirname + '/routes/v1-no-auth/views.js')();

  return {
    app: app,
    router: routes.routes
  }

}
