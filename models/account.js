module.exports = function(mongoose){

	var Schema = mongoose.Schema;

	var accountSchema = new Schema({
		email: String,
		password: String,
		userID: Schema.Types.ObjectId,
		admin: Boolean,
		programa_id: Number
	});

	return mongoose.model('Account', accountSchema);
}