module.exports = function(mongoose){

	var Schema = mongoose.Schema;

	var chatSchema = new Schema({
		user_id: Schema.Types.ObjectId,
		programa_id: Number,
		user_photo: String,
		user_email: String,
		empresa: String,
		nao_respondidas: Number,
		nao_visualizadas: Number,
		conceito_a_photo: String,
		mensagens: [{
			mensagem: String,
			is_user: Boolean
		}]
	});

	return mongoose.model('Chat', chatSchema);
}