module.exports = function(mongoose){

	var Schema = mongoose.Schema;

	var empresaSchema = new Schema({
		nome: String,
		departamento: [String],
		unidade: [String],
		data: Date,
		programa_id: Number,
		ativo: Boolean 
	});
	return mongoose.model('Empresa', empresaSchema);
}