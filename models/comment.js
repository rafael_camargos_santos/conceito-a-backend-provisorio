module.exports = function(mongoose){

    var Schema = mongoose.Schema;

    var CommentSchema = new Schema({
        id_post: Schema.Types.ObjectId,
        id_empresa: Schema.Types.ObjectId,
        id_usuario: Schema.Types.ObjectId,
        programa_id: Number,
        comment: String,
        date: Date,
        name: String
    })
    return mongoose.model('Comentario', CommentSchema);
}
