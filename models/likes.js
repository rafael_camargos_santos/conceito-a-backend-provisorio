module.exports = function(mongoose){

	var Schema = mongoose.Schema;

	var likesSchema = new Schema({
		id_post: Schema.Types.ObjectId,
		id_empresa: Schema.Types.ObjectId,
		id_usuario: Schema.Types.ObjectId,
		programa_id: Number
	})

		return mongoose.model('Likes', likesSchema);
}