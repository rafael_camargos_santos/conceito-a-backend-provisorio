module.exports = function(mongoose){

	var Schema = mongoose.Schema;

	var quizSchema = new Schema({
		programa_id: Number,
		conteudo: String,
		dia: Number,
		empresa: Schema.Types.ObjectId,
		semana: Number,
		tipo: Number,
		opcoes: [{
			id: Number,
			resposta: String
		}]
	});
	return mongoose.model('Quiz', quizSchema);
}