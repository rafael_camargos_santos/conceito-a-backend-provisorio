module.exports = function(mongoose){

	var Schema = mongoose.Schema;

	var conteudoSchema = new Schema({
		programa_id: Number, //1 nossa mesa, 2 simplesmente
		titulo: String,
		conteudo: String,
		tipo: Number, //1 desafio, 2 conteudo, 3 complemento
		dia: Number,
		semana: Number,
		midia: String,
		video: String,
		texto1: String,
		texto2: String,
		texto3: String,
		imagem1: String,
		imagem2: String,
		imagem3: String,
		link: String
	});

	return mongoose.model('Conteudo', conteudoSchema);
}