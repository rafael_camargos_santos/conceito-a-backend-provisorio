module.exports = function(mongoose){

	var Schema = mongoose.Schema;

	var userSchema = new Schema({
		nome : String,
		email : String,
		empresa_nome: String,
		empresa : Schema.Types.ObjectId,
		programa_id: Number,
		primeiroLogin: {
			type: Boolean,
			default: true
		},
		departamento : String,
		unidade : String,
		foto : String,
		score: Number,
		score_semanal: Number,
		rankingGeral: Number,
		rankingDepartamento: Number,
		respondidos: [{conteudo: Schema.Types.ObjectId, id: Number}],
		last_login: Date,
		ativo: Boolean
	});

	return mongoose.model('User', userSchema);
}