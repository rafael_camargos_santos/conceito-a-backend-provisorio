module.exports = function(mongoose){

	var Schema = mongoose.Schema;

	var desafioSchema = new Schema({
		titulo: String,
		data: Date,
		numero: Number,
		texto: String,
		imagem: String,
		unidade: String,
		departamento: String,
		user_id: Schema.Types.ObjectId,
		empresa_id: Schema.Types.ObjectId,
		user_name: String,
		user_foto: String,
		curtidas: Number,
		comentarios: [{nome: String, comentario: String}]
	});

	return mongoose.model('Desafio', desafioSchema);
}