module.exports = function (nodeMailer){
	var env = process.env.NODE_ENV || 'production';

	var smtpConfigNossaMesa = {
	    host: 'mail.nossamesa.com.br',
	    port: 465,
	    tls: {
	        rejectUnauthorized: false
	    },
	    secure: true, // use SSL
	    auth: {
	        user: 'contato@nossamesa.com.br',
	        pass: 'nm2016!'
	    }
	};

	var smtpConfigSimples = {
	    host: 'mail.simplesmentebem.com.br',
	    port: 465,
	    tls: {
	        rejectUnauthorized: false
	    },
	    secure: true, // use SSL
	    auth: {
	        user: 'contato@simplesmentebem.com.br',
	        pass: 'sb2016!'
	    }
	};

	return{
		// constantes do projeto
		mongoURI : function(){
			if (env === 'development') {
			  return 'mongodb://localhost/conceito-a';
			}

			// production only
			else if (env === 'production') {
			  return 'mongodb://conceitoA:conceitoA@ec2-54-210-71-210.compute-1.amazonaws.com:27017/conceitoA';
			}
		},
		apiSecret : function(){
		// chave para validacao do token para usuarios comuns: appsimples-conceito-a-api-secret md5
			return 'e9b4e7b0a636963f1a543eb6f3dbc317';
		},
		apiSecretAdmin : function(){
		// chave para validacao do token para usuarios admin: appsimples-conceito-a-api-secret-admin md5
			return '125e17d6451bc544bdee957a478030bb';
		},
		saltRounds : function(){
			return 4;
		},
		s3AccessKey : function(){
			return 'AKIAJ7CRDDV6OIRPX7PA';
		},
		s3Secret : function(){
			return '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';
		},

		// Variaveis de negocio
		transporter : function(programa){
			if(programa == 1){
				var transporterNodeMailer = nodeMailer.createTransport(smtpConfigNossaMesa);
			}
			else{
				var transporterNodeMailer = nodeMailer.createTransport(smtpConfigSimples);
			}
			return transporterNodeMailer;
		},
		fotoPrograma : function(programa){
			if(programa == 2){
				return "s3.amazonaws.com/appsimples-conceito-a/especialista-simplesmente-bem.jpg";
			}
			else{
				return "s3.amazonaws.com/appsimples-conceito-a/especialista-nossa-mesa.jpg";
			}
		},
		nomePrograma : function(programa){
			if(programa == 1){
				return "Nossa Mesa";
			}
			else if(programa == 2){
				return "Simplesmente Bem";
			}
		}
	};
}
