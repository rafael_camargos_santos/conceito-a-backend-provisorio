'use strict';

/* Controllers */

angular.module('myApp.controller-programa', []).
controller('ProgramaController', ['$scope', 'localStorageService' , '$location' , 'FeedService' , 'AdmConteudoService' , 
    function($scope, localStorageService, $location, FeedService, AdmConteudoService) {
    $scope.return = function() {
        $window.history.back();
    }

    var empresa = localStorageService.get('empresa');

    var view = this;

    this.feed = [];

    FeedService.get(null, empresa)
            .success(function(data) {
                // console.dir(data);
                view.feed = data.response.feed.feed;
                for (var i in view.feed) {
                    var content = view.feed[i];
                    view.parseContent(content);
                }
            })
            .error(function(err) {
                window.location.href = '';
            });

    view.chooseContent = function (content) {
        console.log("choose Content" + content);
        AdmConteudoService.conteudo = content;
        if (content.tipo==0) {
            $location.path("alterar-quiz");
        }
        if (content.tipo==1) {
            $location.path("alterar-desafio");
        }
        if (content.tipo==2) {
            $location.path("alterar-conteudo");
        }
        if (content.tipo==3) {
            $location.path("alterar-complemento");
        }
    }

    view.parseContent = function (content) {
        if (content.tipo==0) {
            content.tipoNome = "Quiz";
            content.titulo = "Quiz " + content.semana;
            content.tipoSemana = " #" + content.semana;
        }
        if (content.tipo==1) {
            content.tipoNome = "Desafio";
            content.tipoSemana = " #" + content.semana;
        }
        if (content.tipo==2) {
            content.tipoNome = "Conteúdo";
            content.tipoSemana = " #" + content.semana;
        }
        if (content.tipo==3) {
            content.tipoNome = "Complemento";
            content.tipoSemana = " #" + content.semana;
        }
    }

    var programaID = localStorageService.get('programaID');
    $scope.programa = function() {
        if (programaID === 1) {
            return true;
        }
        return false;
    }

    
}]);
