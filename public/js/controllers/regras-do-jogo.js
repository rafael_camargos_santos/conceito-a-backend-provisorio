'use strict';

/* Controllers */

angular.module('myApp.controller-regras-do-jogo', []).
controller('RegrasDoJogoController', ['$scope', 'localStorageService', '$sce',
    function($scope, localStorageService, $sce) {

        var definicaoDoJogo = "é um jogo online divertido e desafiador. O jogo é disputado entre os funcionários de uma mesma empresa e vence quem tiver mais pontuação ao final de 12 fases. Cada fase apresenta 4 componentes: conteúdo, complemento, desafios e quiz. O participante conta com um especialista que pode responder dúvidas em relação aos temas das fases (conteúdo e complemento) ou sobre a realização do desafio.";

        this.ptsConteudo = "Um texto ou vídeo trazem informações que ajudam o participante no cumprimento do desafio. São gerados 50 pontos por conteúdo, clicando no botão “Pontuar”.";
        this.ptsDesafio = "Em cada fase deve ser cumprido um desafio proposto que pode ser a postagem de uma imagem ou de um texto no prazo determinado. Esse é o componente que mais gera pontos. São 100 pontos por desafio postado!";
        this.ptsQuiz = "Após o início do jogo, em toda segunda-feira durante 12 semanas, será lançado um quiz com uma pergunta e algumas opções de resposta. Não há uma reposta correta, mas responder ao quiz no dia de lançamento gera 50 pontos; responder em outro dia gera 20 pontos.";
        this.ptsCurtidas = "Os desafios postados pelos participantes podem receber curtidas. Cada curtida gera 1 ponto. Pode parecer pouco, mas quanto melhor for a postagem, maior a chance de ter várias curtidas.";
        this.ptsComplemento = "Complemento – esse é um componente que não gera pontos diretamente, mas trata do mesmo assunto do conteúdo e pode auxiliar o participante a postar um desafio mais interessante que gere mais curtidas";

        var programaID = localStorageService.get('programaID');

        $scope.programa = function() {
            if (programaID === 1) {
                return true;
            }
            return false;
        };

        $scope.to_trusted = function(html_code) {
            return $sce.trustAsHtml(html_code);
        };

        this.descricaoDoPrograma = function() {
            if (programaID === 1) {
                this.tituloDoPrograma = "Nossa Mesa  ";
            } else {
                this.tituloDoPrograma = "Simplesmente Bem ";
            }
            return (this.tituloDoPrograma.concat(definicaoDoJogo));
        }
    }
]);
