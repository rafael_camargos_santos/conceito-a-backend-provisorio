'use strict';

/* Controllers */

angular.module('myApp.controller-alterardesafio', []).
controller('AlterarDesafioController', ['$scope', 'localStorageService', 'AdmConteudoService',
    function($scope, localStorageService, AdmConteudoService) {

        var view = this;
        var programaID = localStorageService.get('programaID');

        this.desafio = AdmConteudoService.conteudo;

        view.success = false;
        view.error = false;
        view.loading = false;
        view.msg = "";

        this.save = function() {
            this.desafio.programa_id = programaID;
            view.loading = true;
            if (!angular.isUndefined(view.desafio.titulo)) {
                AdmConteudoService.postContent(this.desafio)
                    .success(function(data) {
                        if (data.success === true) {
                            view.sucesso();
                            view.msg = "As alterações foram salvas com sucesso.";
                        } else {
                            console.dir("erro no post");
                            view.erro();
                            view.msg = "Não foi possível salvar as alterações.";
                        };
                    })
                    .error(function(err) {
                        console.dir("erro no post");
                        view.erro();
                        view.msg = "Não foi possível salvar as alterações.";
                    });
            } else {
                console.dir("erro no post - título em branco");
                view.erro();
                view.msg = "Não foi possível salvar as alterações.";
            }
        }

        this.voltar = function() {
            view.success = false;
            view.error = false;
            view.loading = false;
            history.back();
        }

        this.sucesso = function() {
            view.loading = false;
            view.success = true;
        }

        this.erro = function() {
            view.loading = false;
            view.error = true;
        }


        $scope.programa = function() {
            if (programaID === 1) {
                return true;
            }
            return false;
        }

        $scope.return = function() {
            $window.history.back();
        }
    }
]);
