'use strict';

/* Controllers */

angular.module('myApp.controller-feed', []).
controller('FeedController', ['$location', '$scope', '$http', 'QuizzesService', 'LikesService', 'CommentsService', 'FeedService',
    'DesafiosService', 'localStorageService', 'PerfilService', '$sce',
    function($location, $scope, $http, QuizzesService, LikesService, CommentsService,
        FeedService, DesafiosService, localStorageService, PerfilService, $sce) {

        $scope.feedTipo = {
            options: [
                { tipo: 0, nome: "Quiz" },
                { tipo: 1, nome: "Desafio" },
                { tipo: 2, nome: "Conteudo" },
                { tipo: 3, nome: "Complemento" }
            ]
        };
        $scope.tipo = {};
        $scope.semana = {};
        $scope.post_id = {};
        var view = this;
        var userId = localStorageService.get('userID');
        var empresa = localStorageService.get('empresa');
        var programaID = localStorageService.get('programaID');
        var nome = localStorageService.get('nome');
        var curtido = false;
        var tempo_atual = Date.now();

        this.teste = function() {
            return true;
        }

        var setLikes = function() {
            for (var i in view.posts) {
                for (var index in view.likes) {
                    if (view.posts[i]._id == view.likes[index].id_post && view.likes[index].id_usuario == userId) {
                        view.posts[i].isLiked = true;
                    }
                }
            }
        }

        var setComentarios = function() {
            for (var i in view.posts) {
                if (view.posts[i].comentarios != null && view.posts[i].comentarios.length > 0) {
                    view.posts[i].comentarios.length = 0;
                } else {
                    view.posts[i].comentarios = [];
                }

                for (var index in view.comentarios) {
                    if (view.posts[i]._id == view.comentarios[index].id_post) {
                        var comentario = {};
                        comentario.name = view.comentarios[index].name;
                        comentario.comment = view.comentarios[index].comment;

                        view.posts[i].comentarios.push(comentario);
                    }
                }
            }
        }

        // Verifica post id no service caso tenha vindo do mapa
        var postSemana = FeedService.getPostSemana();
        if (postSemana != 0) {
            $scope.semana.semana = postSemana;
        }

        //Todos os posts do feed
        var getFeed = function() {
            console.log("Empresafeed" + empresa);
            FeedService.get(null, empresa)
                .success(function(data) {
                    view.posts = data.response.feed.feed;
                    console.dir(data);
                    view.likes = data.response.feed.likes;
                    view.comentarios = data.response.feed.comentarios;
                    view.start_empresa = data.response.empresa_inicio;
                    localStorageService.set('empresa_inicio', data.response.empresa_inicio);
                    var t_empresa = new Date(view.start_empresa);
                    var inicio_empresa = t_empresa.getTime();
                    setLikes();
                    setComentarios();
                    //Retorna o usuario onde tem o parametro dos posts que ele curtiu
                    PerfilService.getPerfil()
                        .success(function(data) {
                            view.respondidos = data.response.perfil.respondidos;
                            //Percorre todos os posts e adiciona parametros
                            for (var i in view.posts) {
                                view.posts[i].isRespondido = false;
                                if (view.posts[i].tipo == 1) {
                                    view.posts[i].responder = "Postar";
                                }else {
                                view.posts[i].responder = "Responder";}
                                if (((tempo_atual - inicio_empresa) / (1000 * 60 * 60 * 24)) < (view.posts[i].dia - 1)) {
                                    view.posts[i].enterFeed = false;
                                } else {
                                    view.posts[i].enterFeed = true;
                                }

                                //percorre todos os objetos no array de respondidos desse usuario
                                for (var index in view.respondidos) {
                                    if (view.respondidos[index].conteudo == view.posts[i]._id) {
                                        view.posts[i].isRespondido = true;
                                        view.posts[i].responder = '';
                                    }
                                }
                            }
                        })
                        .error(function(err) {
                            window.location.href = '';
                        });
                })
                .error(function(err) {
                    window.location.href = '';
                });
        };

        getFeed();

        FeedService.subscribe($scope, function() {
            // Handle notification
            $scope.tipo.tipo = undefined;
            $scope.semana.semana = undefined;
        });

        $scope.programa = function() {
            if (programaID === 1) {
                return true;
            }
            return false;
        }

        $scope.setPostIdValue = function(_id) {
            $scope.post_id = _id;
        }

        $scope.setTypeById = function(tipo) {
            $scope.tipo.tipo = tipo;
            if (tipo == undefined) {
                $scope.semana.semana = undefined;
            }
        }

        this.likePost = function(context) {
            curtido = false;
            for (var index in view.likes) {
                if (context._id == view.likes[index].id_post && view.likes[index].id_usuario == userId && context.isLiked) {
                    curtido = true;
                }
            }
            if (curtido == false) {
                LikesService.post(context._id, empresa, userId)
                    .success(function(data) {
                        view.likes.push({ id_post: context._id, id_usuario: userId });
                        context.curtidas++;
                        context.isLiked = true;
                        curtido = true;
                    })
                    .error(function(err) {
                        window.location.href = '';
                    });
            }
        }

        this.goToQuiz = function(id, isRespondido) {
            QuizzesService.setQuizId(id);
            QuizzesService.setRespondido(isRespondido);
            $location.path('quiz');
        }

        this.goToDesafio = function(desafio, isRespondido) {
            if (!isRespondido) {
                if (desafio.responder === 'Encerrado') {
                    $location.path('desafios');
                    return;
                }
                DesafiosService.setDesafio(desafio);
                $location.path('desafios-a-fazer');
            } else {
                $location.path('desafios');
            }
        }

        this.sendComentario = function(comentario, conteudo) {
            if (angular.copy(comentario) != null) {
                if (angular.copy(comentario).length > 0) {
                    CommentsService.post(conteudo._id, empresa, userId, comentario, new Date(), nome)
                        .success(function(data) {
                            view.comentarios.push(data.response.comentario);
                            setComentarios();
                        })
                        .error(function(err) {
                            console.dir(err);
                        });
                }
            }
        }

        var getRespondidos = function() {
            PerfilService.getPerfil()
                .success(function(data) {
                    respondidos = data.response.perfil.respondidos;
                    console.dir(respondidos);
                })
                .error(function(err) {
                    window.location.href = '';
                });

        }

        this.diasRestantes = function(desafio) {
            if(desafio.tipo == 1){
                var t_post2 = new Date(view.start_empresa);
                var tempo_post2 = t_post2.getTime();
                var tempoPrograma = (((tempo_atual - tempo_post2) / (1000 * 60 * 60 * 24)));
                var dif = Math.floor(tempoPrograma - desafio.dia);
                if (tempoPrograma < desafio.dia){
                    desafio.diasRestantes = 7;
                    return "Restam 7 dias"
                }
                else if (dif > 5) {
                    if (!desafio.isRespondido) desafio.responder = 'Encerrado';
                    desafio.diasRestantes = -1;
                    return "Desafio encerrado";
                } else if (dif - 6== -1) {
                    desafio.diasRestantes = 1;
                    return "Resta 1 dia"
                }
                else {
                    desafio.diasRestantes = -(dif - 6);
                    return "Restam " + -(dif - 6) + " dias.";
                }
            }
        }

        // this.mostraHTML = function(){
        //     var textos = document.getElementsByClassName("text-body");
        //     for (var i = 0; i < textos.length; i++) {
        //         (textos[i]).innerHTML = view.posts[i].conteudo;
        //     }
        // }

        $scope.to_trusted = function(html_code) {
            return $sce.trustAsHtml(html_code);
        }

        $scope.tituloPrograma = function() {
            if (programaID === 1) {
                return "Nossa Mesa";
            }
            return "Simplesmente Bem";
        }
    }
])
