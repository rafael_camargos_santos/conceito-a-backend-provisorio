'use strict';

/* Controllers */

angular.module('myApp.controller-quiz', []).
controller('QuizController', ['$scope', '$window', 'localStorageService', 'QuizzesService', 'PerfilService',
    function($scope, $window, localStorageService, QuizzesService, PerfilService) {

        var vm = this;
        var id = QuizzesService.getQuizId();
        var userId = localStorageService.get('userID');
        var programaID = localStorageService.get('programaID');
        var isAdm = localStorageService.get('isAdmin');

        var empresa_inicio = localStorageService.get('empresa_inicio');

        var t_post2 = new Date(empresa_inicio);
        var tempo_post2 = t_post2.getTime();
        var tempo_atual = Date.now();
        var tempoPrograma = Math.ceil((((tempo_atual - tempo_post2) / (1000 * 60 * 60 * 24))));

        vm.quiz = {};
        vm.confirm = false;
        vm.message = "";
        $scope.bool = false;
        $scope.messageFeedBack = "Obrigado pela resposta!";

        QuizzesService.get(id)
            .success(function(data) {
                vm.quiz = data.response.quiz;
                console.dir(vm.quiz);
                vm.checkRespondido(QuizzesService.getRespondido());
                // TODO: mudar modelo do banco e fazer contas de dias
                vm.quiz.restante = 2;
            })
            .error(function(err) {
                console.dir(err);
            });


        $scope.programa = function() {
            if (programaID === 1) {
                return true;
            }
            return false;
        }

        this.responder = function(answer, resposta) {
            if (!isAdm) {
                vm.quiz.answer = answer;
                vm.quiz.resposta = resposta;
                $scope.feedbackQuiz = true;
                if (vm.quiz.resposta != undefined) {
                    $scope.bool = false;
                    $scope.messageFeedBack = "Obrigado pela resposta!";
                    var diaCerto = false;
                    console.log(vm.quiz.dia);
                    console.log(tempoPrograma);
                    if(vm.quiz.dia == tempoPrograma){
                        diaCerto = true;
                    }

                    QuizzesService.post(userId, id, vm.quiz, diaCerto)
                        .success(function(data) {
                            PerfilService.atualizaPerfil();
                        })
                        .error(function(err) {
                            console.dir(err);
                        })
                }

                if (vm.quiz.resposta === undefined) {
                    $scope.bool = true;
                    $scope.messageFeedBack = "Escolha uma opção.";
                }
            }
        }

        this.checkRespondido = function(respondido) {
            vm.respondido = respondido;
            if (vm.respondido) {
                vm.confirm = true;
                vm.message = 'Você já respondeu este quiz!';
            }
        }

        $scope.decisionBack = function() {
            if ($scope.bool === false) {
                $window.history.back();
            }
        }

    }
]);
