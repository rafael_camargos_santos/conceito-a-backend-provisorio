'use strict';

/* Controllers */

angular.module('myApp.controller-adicionar-usuarios', []).
controller('AdicionarUsuariosController', ['$location', '$scope', '$http', 'EmpresaService', 'localStorageService',
    function($location, $scope, $http, EmpresaService, localStorageService) {

        var empresaID = localStorageService.get('empresaID');
        var view = this;
        var programaID = localStorageService.get('programaID');
        this.loading = false;
        this.inputList = [{}];

        EmpresaService.getEmpresaById(empresaID)
            .success(function(data) {
                view.empresa = data.response.empresa[0];
            })
            .error(function(err) {
                console.log(err);
            });

        $scope.programa = function() {
            if (programaID === 1) {
                return true;
            }
            return false;
        }

        this.adicionaNovoCampo = function() {
            this.inputList.push({});
        };

        this.voltar = function() {
            this.loading = false;
            history.back();
        }

        this.goToAdministrador = function(){
            $location.path('administrador');
        }

        this.saveUsers = function() {
            this.loading = true;
            var users = [];
            for (var index in this.inputList) {
                var input = this.inputList[index];
                if (input.emails && (input.departamento || input.unidade)) {
                    var emails = input.emails.split(',');
                    for (var i in emails) {
                        var email = emails[i];
                        email = email.replace(/\s/g, '');
                        if (validateEmail(email)) {
                            users.push({
                                'email': email,
                                'departamento': input.departamento,
                                'unidade': input.unidade,
                                'empresa': empresaID,
                                'empresa_nome': this.empresa.nome,
                                'programa_id': programaID
                            });
                        }
                    }
                }
            }
            if (users.length > 0) {
                EmpresaService.createUsers(users)
                    .success(function(data) {
                        $location.path('usuarios');
                    })
                    .error(function(err) {
                        console.log(err);
                    });
            }
            this.voltar();
        }

        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
    }
]);

//var listaAreas = ["Area 1", "Area 2", "Area 3"];
//var listaUnidades = ["Unidade 1", "Unidade 2", "Unidade 3"];
