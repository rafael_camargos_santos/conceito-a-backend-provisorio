'use strict';

/* Controllers */

angular.module('myApp.controller-alterarquiz', []).
controller('AlterarQuizController', ['$scope', 'localStorageService', 'AdmConteudoService',
    function($scope, localStorageService, AdmConteudoService) {
        var programaID = localStorageService.get('programaID');

        //$scope.data = new Date();
        //$scope.pergunta = {};
        //$scope.resposta = {};

        //$scope.respostas = [];
        //$scope.indexRespostas = [];
        //$scope.novoCampoResposta = false;

        this.quiz = AdmConteudoService.conteudo;

        this.tmpArray = arrayOfStrings(this.quiz.opcoes);

        var view = this;
        view.success = false;
        view.error = false;
        view.loading = false;
        view.msg = "";

        this.adicionaResposta = function() {
            this.tmpArray.push(" ");
        }

        this.removerResposta = function(resposta) {
            console.log("removerResposta: " + resposta);
            if (this.tmpArray.indexOf(resposta) != -1) {
                this.tmpArray.splice(this.tmpArray.indexOf(resposta), 1);
            }
        }

        this.saveQuiz = function() {
            view.loading = true;
            this.quiz.opcoes = saveArray(this.tmpArray);
            this.quiz.programa_id = programaID;
            if (!angular.isUndefined(view.quiz.conteudo)) {
                AdmConteudoService.postQuiz(this.quiz)
                    .success(function(data) {
                        if (data.success === true) {
                            view.sucesso();
                            view.msg = "As alterações foram salvas com sucesso.";
                        } else {
                            console.dir("erro no post");
                            view.erro();
                            view.msg = "Não foi possível salvar as alterações.";
                        };
                    })
                    .error(function(err) {
                        console.dir("erro no post");
                        view.erro();
                        view.msg = "Não foi possível salvar as alterações.";
                    });
            } else {
                console.dir("erro no post - pergunta em branco");
                view.erro();
                view.msg = "Não foi possível salvar as alterações.";
            }
        }

        this.sucesso = function() {
            view.loading = false;
            view.success = true;
        }

        this.erro = function() {
            view.loading = false;
            view.error = true;
        }

        this.voltar = function() {
            view.success = false;
            view.error = false;
            view.loading = false;
            history.back();
        }

        //Setup programa background

        $scope.programa = function() {
            if (programaID === 1) {
                return true;
            }
            return false;
        }



    }
]);

var arrayOfStrings = function(array) {
    var tmpArray = [];
    for (var index in array) {
        var content = array[index];
        tmpArray.push(content.resposta);
    }

    var returnArray = tmpArray.slice();
    return tmpArray;
}

var saveArray = function(array) {
    var saveArray = [];
    for (var index in array) {
        var string = array[index];
        if (string != "") {
            saveArray.push({ 'id': index++, 'resposta': string });
        }
    }
    return saveArray;
}
