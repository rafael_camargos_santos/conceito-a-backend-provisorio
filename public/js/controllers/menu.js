'use strict';

/* Menu Lateral Controller */

angular.module('myApp.controller-menu', [])
    .controller('MenuController', ['$scope', '$rootScope', '$window', '$document', '$location', 'localStorageService', 'PerfilService', 'ChatService', 'FeedService',
        function($scope, $rootScope, $window, $document, $location, localStorageService, PerfilService, ChatService, FeedService) {

            var view = this;
            var userID = localStorageService.get('userID');
            var isAdmin = localStorageService.get('isAdmin');
            var isEspecialista = localStorageService.get('isEspecialista');
            view.programaID = localStorageService.get('programaID');

            // seta nome da tab
            $scope.page = {};
            $scope.page.programa = '';
            $scope.isNossaMesa = false;
            $scope.isSimplesmente = false;
            $scope.showSideMenu = true;
            $scope.showtrigger = false;

            var isMobile = false;

            $scope.naoVisualizadasFlag = false;

            if (view.programaID == 1){
                $scope.page.programa = "Nossa Mesa";
                $scope.isNossaMesa = true;
                $scope.title = "Conceito A - Nossa Mesa";
            }
            else if(view.programaID == 2){
                $scope.page.programa = "Simplesmente Bem";
                $scope.isSimplesmente = true;
                $scope.title = "Conceito A - Simplesmente Bem"
            }

            if (!userID) {
                window.location.href = '';
            }
            var setPerfil = function() {
                if (localStorageService.get('nome')) {
                    $scope.nome = localStorageService.get('nome').split(" ");
                    $scope.departamento = localStorageService.get('departamento');
                    if (localStorageService.get('score')) {
                        $scope.score = localStorageService.get('score');
                    } else {
                        $scope.score = 0;
                    }
                    if (localStorageService.get('foto')) {
                        $scope.foto = localStorageService.get('foto') + '?r=' + Math.round(Math.random() * 999999);
                    } else {
                        $scope.foto = "/images/perfil_ok.png";
                    }
                }
                if(!isAdmin && !isEspecialista){
                    ChatService.getNaoVisualizadas(userID)
                    .success(function(data){
                        $scope.naoVisualizadas = data.response.nao_visualizadas;
                        if($scope.naoVisualizadas > 0) $scope.naoVisualizadasFlag = true;
                        else $scope.naoVisualizadasFlag = false;
                    })
                }
            }

            $rootScope.$on("CallSetPerfil", function(){
               setPerfil();
            });

            setPerfil();

            // Lida com modificacao no perfil 
            PerfilService.subscribe($scope, function() {
                // Handle notification
                console.log("recebeu");
                setPerfil();
            });

            // alterna entre views (urls)
            view.changeView = function(view) {
                $location.path(view); 
                if(isMobile){
                    hideMenu();
                }
            }

            view.goToFeed= function() {
                $location.path('feed');
                FeedService.notify();
                if(isMobile){
                    hideMenu();
                }
            }

            view.changeToChat = function() {
                if (isAdmin || isEspecialista) {
                    $window.alert('Acesso somente para usuários.');
                    return;
                }
                else {
                    if(isMobile){
                        hideMenu();
                    }
                    $location.path('chat');
                }
            }

            view.changeToRanking = function() {
                if (isAdmin || isEspecialista) {
                    $window.alert('Acesso somente para usuários.');
                    return;
                }
                else {
                    if(isMobile){
                        hideMenu();
                    }
                    $location.path('ranking');
                }
            }

            view.showMenu = function(){
                if($scope.showSideMenu){
                    hideMenu();
                }
                else{
                    showMenu();
                }
            }

            var hideMenu = function(){
                $scope.showSideMenu = false;
                $scope.showTrigger = true;
                $document[0].getElementById("trigger-side-menu").className = "trigger-side-menu-mobile";

                $document[0].getElementById("inside-view").className = "inside-view-mobile full-page";
            }

            var showMenu = function(){
                $scope.showSideMenu = true;
                $scope.showTrigger = false;
                $document[0].getElementById("trigger-side-menu").className = "trigger-side-menu";

                $document[0].getElementById("inside-view").className = "inside-view full-page";
            }

            if($window.innerWidth < 1080){
                $scope.showTrigger = true;
                isMobile = true;
                hideMenu();
            }

            angular.element($window).bind('scroll', function () {
                if($scope.showSideMenu && isMobile){
                    hideMenu();
                    $scope.$digest();
                }
            });

        }
    ]);
