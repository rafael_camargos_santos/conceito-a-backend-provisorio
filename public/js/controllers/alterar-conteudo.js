'use strict';

/* Controllers */

angular.module('myApp.controller-alterarconteudo', []).
controller('AlterarConteudoController', ['$scope', 'localStorageService', 'AdmConteudoService', 'S3Service',
    function($scope, localStorageService, AdmConteudoService, S3Service) {

        var view = this;
        var programaID = localStorageService.get('programaID');

        this.content = AdmConteudoService.conteudo;
        view.success = false;
        view.error = false;
        view.loading = false;
        view.msg = "";
        view.voltarPagina = false;

        view.indice = [0, -1, -1, -1];

        view.contErros = [0, -1, -1, -1];
        view.contSucessos = [0, 0, 0, 0];

        view.contErrosAnterior = [0, 0, 0, 0];
        view.contSucessosAnterior = [0, 0, 0, 0];

        this.save = function() {
            this.content.programa_id = programaID;
            this.loading = true;
            if (!angular.isUndefined(view.content.titulo)) {
                AdmConteudoService.postContent(this.content)
                    .success(function(data) {
                        if (data.success === true) {
                            view.sucesso("As alterações foram salvas com sucesso.");
                            view.voltarPagina = true;
                        } else {
                            console.dir("erro no post");
                            view.erro("Não foi possível salvar as alterações.");
                            view.voltarPagina = true;
                        };
                    })
                    .error(function(err) {
                        console.dir("erro no post");
                        view.erro("Não foi possível salvar as alterações.");
                        view.voltarPagina = true;
                    });
            } else {
                console.dir("erro no post - título em branco");
                view.erro("Não foi possível salvar as alterações.");
                view.voltarPagina = true;
            }
        }

        this.delete = function(id) {
            switch (id) {
                case 1:
                    view.content.imagem1 = "";
                    break;
                case 2:
                    view.content.imagem2 = "";
                    break;
                case 3:
                    view.content.imagem3 = "";
                    break;
                default:
                    break;
            }
        }

        $scope.programa = function() {
            if (programaID === 1) {
                return true;
            }
            return false;
        }

        // Upload de imagem S3
        this.files = { 'imagem1': [], 'imagem2': [], 'imagem3': [] };

        // Tratamento de erro do upload para S3:
        // var showFileSizeError = function() {
        //     console.dir("tamanho do arquivo excede o máximo permitido.");
        //     // TODO: mostrar erro
        // }

        // var showWrongExtensionError = function() {
        //     console.dir("Extensão nao permitida");
        //     // TODO: mostrar erro
        // }

        var showUploadSuccess = function(numImg) {
            view.contSucessos[numImg]++;
            if (view.contSucessos[numImg] + view.contErros[numImg] == view.indice[numImg] + 1) {
                if (view.contSucessos[numImg] != view.contSucessosAnterior[numImg]) {
                    view.contSucessosAnterior[numImg] = view.contSucessos[numImg];
                    view.contErrosAnterior[numImg] = view.contErros[numImg];
                    view.loading = false;
                    view.sucesso("O upload da imagem foi bem sucedido.");
                } else {
                    view.contSucessosAnterior[numImg] = view.contSucessos[numImg];
                    view.contErrosAnterior[numImg] = view.contErros[numImg];
                    view.loading = false;
                    view.erro("Houve um erro no upload. Certifique-se que o tamanho da imagem não excede o máximo permitido e que sua extensão é .jpg, .jpeg ou .png");
                };
                view.contSucessos[numImg] = 0;
                view.contErros[numImg] = 0;
            };
        };

        var showUploadError = function(numImg) {
            view.contErros[numImg]++;
            if ((view.indice[numImg] >= 0) && (view.contSucessos[numImg] + view.contErros[numImg] == view.indice[numImg] + 1)) {
                if (view.contErros[numImg] != view.contErrosAnterior[numImg]) {
                    view.contSucessosAnterior[numImg] = view.contSucessos[numImg];
                    view.contErrosAnterior[numImg] = view.contErros[numImg];
                    view.loading = false;
                    view.erro("Houve um erro no upload. Certifique-se que o tamanho da imagem não excede o máximo permitido e que sua extensão é .jpg, .jpeg ou .png");
                };
                view.contSucessos[numImg] = 0;
                view.contErros[numImg] = 0;
            };
        };

        var s3Url = S3Service.getS3URL();

        var desafioFilename1 = programaID + "/conteudos/" + this.content._id + "_1";
        this.options1 = {
            multiple: false,
            immediate: true,
            extensions: ["jpg", "jpeg", "png"],
            bucket: "appsimples-conceito-a",
            acl: "public-read",
            policyUrl: s3Url,
            filename: desafioFilename1,
            // on_filesize: function() { showFileSizeError() },
            // on_upload: function() { showUploadError() },
            // on_extension: function() { showWrongExtensionError() },
            on_success: function() {
                view.content.imagem1 = view.files.imagem1[view.indice[1]].real;
                showUploadSuccess(1);
            },
            on_error: function() {
                showUploadError(1);
            },
            on_uploadStart: function(){
                    view.loading = true;
            }
        }
        var desafioFilename2 = programaID + "/conteudos/" + this.content._id + "_2";
        this.options2 = {
            multiple: false,
            immediate: true,
            extensions: ["jpg", "jpeg", "png"],
            bucket: "appsimples-conceito-a",
            acl: "public-read",
            policyUrl: s3Url,
            filename: desafioFilename2,
            // on_filesize: function() { showFileSizeError() },
            // on_upload: function() { showUploadError() },
            // on_extension: function() { showWrongExtensionError() },
            on_success: function() {
                view.content.imagem2 = view.files.imagem2[view.indice[2]].real;
                showUploadSuccess(2);
            },
            on_error: function() {
                showUploadError(2);
            },
            on_uploadStart: function(){
                    view.loading = true;
            }
        }
        var desafioFilename3 = programaID + "/conteudos/" + this.content._id + "_3";
        this.options3 = {
            multiple: false,
            immediate: true,
            extensions: ["jpg", "jpeg", "png"],
            bucket: "appsimples-conceito-a",
            acl: "public-read",
            policyUrl: s3Url,
            filename: desafioFilename3,
            // on_filesize: function() { showFileSizeError() },
            // on_upload: function() { showUploadError() },
            // on_extension: function() { showWrongExtensionError() },
            on_success: function() {
                view.content.imagem3 = view.files.imagem3[view.indice[3]].real;
                showUploadSuccess(3);
            },
            on_error: function() {
                showUploadError(3);
            },on_uploadStart: function(){
                    view.loading = true;
            }

        }

        this.uploadImg = function(numImg) {
            view.indice[numImg]++;
            // view.loading = true;
        }

        this.sucesso = function(mensagem) {
            view.loading = false;
            view.success = true;
            view.msg = mensagem;
        }

        this.erro = function(mensagem) {
            view.loading = false;
            view.error = true;
            view.msg = mensagem;
        }

        this.voltar = function() {
            view.success = false;
            view.error = false;
            view.loading = false;
            if (view.voltarPagina) {
                history.back();
            }
        }
    }
]);
