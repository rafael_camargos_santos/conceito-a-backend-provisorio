'use strict';

/* Controllers */

angular.module('myApp.controller-alterarcomplemento', []).
controller('AlterarComplementoController', ['$scope', 'localStorageService', 'AdmConteudoService', 'S3Service',
    function($scope, localStorageService, AdmConteudoService, S3Service) {

        var view = this;
        var programaID = localStorageService.get('programaID');

        view.success = false;
        view.error = false;
        view.loading = false;
        view.confirm = false;
        view.msg = "";

        view.indice = -1;

        view.contErros = -1;
        view.contSucessos = 0;

        view.contErrosAnterior = 0;
        view.contSucessosAnterior = 0;

        view.voltarPagina = false;

        this.complemento = AdmConteudoService.conteudo;
        // console.dir(this.complemento._id);

        this.save = function() {
            view.loading = true;
            this.complemento.tipo = 3;
            this.complemento.programa_id = programaID;
            if (this.complemento.dia % 7 == 0) {
                this.complemento.semana = this.complemento.dia / 7;
            } else {
                this.complemento.semana = Math.trunc(this.complemento.dia / 7) + 1;
            }
            this.complemento.programa_id = programaID;
            console.dir(this.complemento);
            if (!angular.isUndefined(view.complemento.titulo)) {
                AdmConteudoService.postContent(this.complemento)
                    .success(function(data) {
                        if (data.success === true) {
                            view.voltarPagina = true;
                            view.sucesso("As alterações foram salvas com sucesso.");
                        } else {
                            console.dir("erro no post");
                            view.voltarPagina = true;
                            view.erro("Não foi possível salvar as alterações.");
                        };
                    })
                    .error(function(err) {
                        console.dir("erro no post");
                        view.voltarPagina = true;
                        view.erro("Não foi possível salvar as alterações.");
                    });
            } else {
                console.dir("erro no post - título em branco");
                view.voltarPagina = true;
                view.erro("Não foi possível salvar as alterações.");
            }
        }

        this.voltar = function() {
            view.success = false;
            view.error = false;
            view.loading = false;
            if (view.voltarPagina) {
                history.back();
            }
        }

        this.deletar = function() {
            view.loading = true;
            if (this.complemento.tipo === 3) {
                AdmConteudoService.deleteContent(this.complemento)
                    .success(function(data) {
                        if (data.success === true) {
                            view.voltarPagina = true;
                            view.sucesso("O complemento foi deletado com sucesso.");
                        } else {
                            console.dir("erro no post");
                            view.voltarPagina = true;
                            view.erro("Não foi possível deletar o complemento.");
                        };
                    })
                    .error(function(err) {
                        console.dir("erro no post");
                        view.voltarPagina = true;
                        view.erro("Não foi possível deletar o complemento.");
                    });
            }
        }

        $scope.programa = function() {
            if (programaID === 1) {
                return true;
            }
            return false;
        }

        this.files = [];

        // Tratamento de erro do upload para S3:
        // var showFileSizeError = function() {
        //     console.dir("tamanho do arquivo excede o máximo permitido.");
        //     // TODO: mostrar erro
        // }

        // var showWrongExtensionError = function() {
        //     console.dir("Extensão nao permitida");
        //     // TODO: mostrar erro
        // }

        var showUploadSuccess = function() {
            view.contSucessos++;
            view.complemento.midia = view.files[view.indice].real;
            if (view.contSucessos + view.contErros == view.indice + 1) {
                if (view.contSucessos != view.contSucessosAnterior) {
                    view.contSucessosAnterior = view.contSucessos;
                    view.contErrosAnterior = view.contErros;
                    view.loading = false;
                    view.sucesso("O upload da imagem foi bem sucedido.");
                } else {
                    view.contSucessosAnterior = view.contSucessos;
                    view.contErrosAnterior = view.contErros;
                    view.loading = false;
                    view.erro("Houve um erro no upload. Certifique-se que o tamanho da imagem não excede o máximo permitido e que sua extensão é .jpg, .jpeg ou .png");
                };
                view.contSucessos = 0;
                view.contErros = 0;
            };

        };

        var showUploadError = function() {
            view.contErros++;
            if ((view.indice >= 0) && (view.contSucessos + view.contErros == view.indice + 1)) {
                if (view.contErros != view.contErrosAnterior) {
                    view.contSucessosAnterior = view.contSucessos;
                    view.contErrosAnterior = view.contErros;
                    view.loading = false;
                    view.erro("Houve um erro no upload. Certifique-se que o tamanho da imagem não excede o máximo permitido e que sua extensão é .jpg, .jpeg ou .png");
                };
                view.contSucessos = 0;
                view.contErros = 0;
            };
        };

        var s3Url = S3Service.getS3URL();

        var desafioFilename = programaID + "/complementos/" + this.complemento._id;
        this.options = {
            multiple: false,
            immediate: true,
            extensions: ["jpg", "jpeg", "png"],
            bucket: "appsimples-conceito-a",
            acl: "public-read",
            policyUrl: s3Url,
            filename: desafioFilename,
            //on_filesize: function() { showFileSizeError() },
            //on_upload: function() { showUploadError() },
            //on_extension: function() { showWrongExtensionError() },
            on_success: function() { showUploadSuccess() },
            on_error: function() { showUploadError() },
            on_uploadStart: function(){
                view.loading = true;
            }
        }

        this.uploadImg = function() {
            view.indice++;
            // view.loading = true;
        }

        this.sucesso = function(mensagem) {
            view.loading = false;
            view.success = true;
            view.msg = mensagem;
        }

        this.erro = function(mensagem) {
            view.loading = false;
            view.error = true;
            view.msg = mensagem;
        }

        this.deleteFoto = function() {
            view.complemento.midia = "";
        }
    }
]);
