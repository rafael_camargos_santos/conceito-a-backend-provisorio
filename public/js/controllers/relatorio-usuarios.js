'use strict';

/* Controllers */

angular.module('myApp.controller-relatorio-usuarios', []).
controller('RelatorioUsuariosController', ['$scope', '$http', '$location', 'EmpresaService', 'localStorageService', 'FeedService',
    function($scope, $http, $location, EmpresaService, localStorageService, FeedService) {

        var view = this;
        var programaID = localStorageService.get('programaID');
        var empresaID = localStorageService.get('empresaID');

        EmpresaService.getEmpresaById(empresaID)
            .success(function(data) {
                view.empresa = data.response.empresa[0];
            })
            .error(function(err) {
                console.log(err);
            });

        EmpresaService.relatorioUsuarios(empresaID)
            .success(function(data) {
                view.usersInfo = data.response.usuarios;
            })
            .error(function(err) {
                console.log(err);
            });

        FeedService.get()
            .success(function(data) {
                view.feed = data.response.feed.feed;
            })
            .error(function(err) {
                console.log(err);
            });

        $scope.programa = function() {
            if (programaID === 1) {
                return true;
            }
            return false;
        }

        this.getSemanas = function(ctd) {
            var semanas = [];
            var strSemanas = "";
            if (ctd.length == 0) {
                return "-";
            } else {
                for (var i = 0; i < ctd.length; i++) {
                    var j = 0;
                    var naoEncontrou = true;
                    while (j < view.feed.length && naoEncontrou) {
                        if (view.feed[j]._id == ctd[i]) {
                            if (angular.isUndefined(view.feed[j].semana)) {
                                semanas.push(view.feed[j].numero);
                            } else {
                                semanas.push(view.feed[j].semana);
                            }
                            naoEncontrou = false;
                        } else {
                            j++;
                        }
                    }
                }
                semanas.sort(function(a,b) {
                    return a - b;
                });
                strSemanas += semanas[0].toString();
                for (var k = 1; k < semanas.length; k++) {
                    strSemanas += ", ";
                    strSemanas += semanas[k].toString();
                }
                return strSemanas;
            }
        }
    }
]);
