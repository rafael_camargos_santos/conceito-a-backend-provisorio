'use strict';

/* Controllers */

angular.module('myApp.controller-adm-usuarios', []).
controller('AdmUsuariosController', ['$scope', '$http', '$location', 'EmpresaService', 'localStorageService', 
    function($scope, $http, $location, EmpresaService, localStorageService) {

    var programaID = localStorageService.get('programaID');
    var empresaID = localStorageService.get('empresaID');
    var view = this;

    EmpresaService.getEmpresaById(empresaID)
        .success(function(data) {
            view.empresa = data.response.empresa[0];
        })
        .error(function(err) {
            console.log(err);
        });

    EmpresaService.getUsersEmpresa()
        .success(function(data) {
            view.usuarios = data.response.usuarios;
        })
        .error(function(err) {
            console.log(err);
        });

    $scope.programa = function() {
        if (programaID === 1) {
            return true;
        }
        return false;
    }

    view.addUsers = function() {
        $location.path('/adicionar-usuarios');
    }

    //salva usuario no service
    view.getUser = function(usuario) {
        EmpresaService.getUser(usuario);
        $location.path("editar-usuario");
    }

    $scope.return = function() {
        $window.history.back();
    }

}]);