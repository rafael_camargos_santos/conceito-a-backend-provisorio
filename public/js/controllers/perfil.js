'use strict';

/* Controllers */

angular.module('myApp.controller-perfil', []).
controller('PerfilController', ['$scope', '$http', '$location', 'PerfilService', 'DesafiosService', 'localStorageService', 'CommentsService', 'LikesService',
    function($scope, $http, $location, PerfilService, DesafiosService, localStorageService, CommentsService, LikesService) {
        var view = this;
        view.likes = [];
        view.comments = [];
        $scope.isPerfil = true;
        var isMyPerfil = PerfilService.isMyPerfil;
        var empresaID = localStorageService.get('empresa');
        var userID = localStorageService.get('userID');
        var programaID = localStorageService.get('programaID');
        var nome = localStorageService.get('nome');
        var curtido = false;
        view.empresa_nome = localStorageService.get('empresa_nome');

        var setLikes = function() {
            for (var i in view.desafios) {
                for (var index in view.likes) {
                    if (view.desafios[i]._id == view.likes[index].id_post && view.likes[index].id_usuario == userID) {
                        view.desafios[i].isLiked = true;
                    }
                }
            }
        }


        LikesService.get(empresaID)
            .success(function(data) {
                for (var index in data.response.likes) {
                    view.likes.push(data.response.likes[index]);
                }
            })
            .error(function(err) {
                window.location.href = '';
            })

        CommentsService.get(empresaID)
            .success(function(data) {
                for (var index in data.response.comments) {
                    view.comments.push(data.response.comments[index]);
                }
            })
            .error(function(err) {
                window.location.href = '';
            })

        DesafiosService.getUser(userID)
            .success(function(data) {
                view.desafios = data.response.desafios;
            })
            .error(function(err) {
                console.dir("Erro desafio");
            });


        PerfilService.getPerfil()
            .success(function(data) {
                view.user = data.response.perfil;
                localStorageService.set('empresa_nome', data.response.perfil.empresa_nome);
                view.empresa_nome = data.response.perfil.empresa_nome;
                if (data.response.perfil.score == null) {
                    view.user.score = "0";
                }
                view.user.foto = localStorageService.get('foto');
                if (!view.user.foto) {
                    view.user.foto = "/images/perfil_ok.png";
                }
                if (PerfilService.isMyPerfil) {
                    view.editarPerfil = 'Editar\nPerfil';
                    view.meuPerfil = 'Meu Perfil';
                    view.meusDesafios = 'Meus Desafios';
                } else {
                    view.editarPerfil = '';
                    view.meuPerfil = 'Perfil';
                    view.meusDesafios = 'Desafios de  ' + data.response.perfil.nome;
                }
                setLikes();
                PerfilService.atualizaPerfil();
                console.log("emitiu");
            })
            .error(function(err) {
                console.dir("Erro get perfil");
            });


        $scope.programa = function() {
            if (programaID === 1) {
                return true;
            }
            return false;
        }

        // this.setShow = function(post, show) {
        //     post.comentarioIsShow = show;
        // };

        // this.isShow = function(post) {
        //     return (post.comentarioIsShow === true);
        // };

        this.goToEditar = function() {
            $location.path('editar-perfil');
        }


        this.postLike = function(context, $event) {
            curtido = false;
            view.liked = false;
            for (var index in view.likes) {
                if (context._id == view.likes[index].id_post && view.likes[index].id_usuario == userID && context.isLiked) {
                    curtido = true;
                }
            }
            if (curtido == false) {
                curtido = true;
                view.likes.push({ id_post: context._id, id_usuario: userID });
                context.isLiked = true;
                context.curtidas++;
                DesafiosService.postLike(context._id, context.curtidas)
                    .success(function(data) {
                        LikesService.post(context._id, empresaID, userID)
                            .success(function(data) {})
                            .error(function(err) {
                                window.location.href = '';
                            });

                    })
                    .error(function(err) {
                        window.location.href = '';
                    });
            }
        }

        this.postComentario = function(context, comentario) {
            DesafiosService.postComentario(context._id, comentario, nome)
                .success(function(data) {
                    context.comentarios.push({ nome: nome, comentario: comentario });
                    CommentsService.post(context._id, empresaID, userID, comentario.comentario, new Date(), nome)
                        .success(function(data) {
                            console.dir(comentario);
                        })
                        .error(function(data) {
                            window.location.href = '';
                        })
                })
                .error(function(data) {

                })
        }

        $scope.getComentario = function(comentario, desafio) {
            if (angular.copy(comentario) != null) {
                if (angular.copy(comentario).length > 0) {
                    CommentsService.post(desafio._id, userID, empresaID, comentario, new Date(), nome)
                        .success(function(data) {
                            console.dir(data);
                        })
                        .error(function(err) {
                            console.dir(err);
                        });
                }
            }
        }

        $scope.editDesafio = function(desafio) {
            DesafiosService.setDesafio(desafio);
            $location.path('desafios-a-fazer');
        }

    }
]);
