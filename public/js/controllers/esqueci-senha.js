'use strict';

/* Controllers */

angular.module('myApp.controller-esqueci-senha', []).
controller('EsqueciSenhaController', ['$scope', '$http', 'EsqueciSenhaService', 'localStorageService',
    function($scope, $http, EsqueciSenhaService, localStorageService) {

        var view = this;
        view.error = false;
        view.success = false;

        // seta nome da tab
        view.programaID = localStorageService.get('programaID');
        console.log(view.programaID);

        $scope.page = {};
        $scope.page.programa = '';


        if (view.programaID == 1){
            $scope.page.programa = "Nossa Mesa";
        }
        else if(view.programaID == 2){
            $scope.page.programa = "Simplesmente Bem";
        }

        this.reset = function(email) {
            console.dir("TESTE");
            if (angular.copy(email) != null) {
                if (angular.copy(email).length > 0) {
                    EsqueciSenhaService.reset(email)
                        .success(function(data) {
                            if (data.success === false) {
                                console.dir("SUCCESS é false");
                                view.error = true;
                                return view.err = 'E-mail inválido';
                                // TODO: mostrar erro;
                            }
                            view.success = true;
                            view.message = data.message;
                        })
                        .error(function(err) {
                            console.dir("ERR");
                            // TODO: mostrar erro;
                        });
                }
            }
        }

        this.voltar = function() {
            view.success = false;
            history.back();
        }
    }
]);
