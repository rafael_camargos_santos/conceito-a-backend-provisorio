'use strict';

/* Controllers */

angular.module('myApp.controller-ferramentas', []).
controller('FerramentasController', ['$scope', '$location', 'localStorageService', 'FerramentasService', 'PerfilService', 'md5', '$auth',
    function($scope, $location, localStorageService, FerramentasService, PerfilService, md5, $auth) {

        $scope.senha = "";
        $scope.novaSenha = "";
        $scope.repNovaSenha = "";
        var userId = localStorageService.get('userID');
        var isAdmin = localStorageService.get('isAdmin');
        var isEspecialista = localStorageService.get('isEspecialista');
        var email = localStorageService.get('email');
        var programaID = localStorageService.get('programaID');

        $scope.programa = function() {
            if (programaID === 1) {
                return true;
            }
            return false;
        }

        this.isAdmin = function(){
            return isAdmin || isEspecialista;
        }

        this.changePassword = function(password, novaSenha, repNovaSenha) {
            if (!(angular.equals(novaSenha, repNovaSenha))) {
                window.alert("As senhas digitadas não coincidem.");
                $scope.novaSenha = "";
                $scope.repNovaSenha = "";
            } else {
                if (novaSenha.length < 5) {
                    window.alert(angular.copy("A nova senha deve conter mais que 5 caracteres."));
                    $scope.novaSenha = "";
                    $scope.repNovaSenha = "";
                } else {
                    var passwordMD5 = md5.createHash(password || '');
                    var novaSenhaMD5 = md5.createHash(novaSenha || '');
                    FerramentasService.post(passwordMD5, novaSenhaMD5, email, programaID)
                        .success(function(data) {
                            console.log(data);
                            if (data.success) {
                                window.alert(angular.copy("Senha atualizada."));
                            } else {
                                window.alert(angular.copy("Senha Errada!"));
                            }
                            PerfilService.atualizaPerfil();
                        })
                        .error(function(err) {
                            console.dir(err);
                        })
                }
            }
        };

        this.getContato = function() {
            if (programaID === 1) {
                return infoContatoNossaMesa;
            }
            return infoContatoSimplesmente;
        };

        // this.changePassword = function(password, newPassword){
        //     FerramentasService.post(password, newPassword, email)
        //         .success(function(data){
        //             console.dir("sucesso");
        //             PerfilService.atualizaPerfil();
        //         })
        //         .error(function(err){
        //             console.dir(err);
        //         })

        // }

        this.goToAdministrador = function(){
            $location.path('administrador');
        }

        this.goToRegras = function(){
            $location.path('regras-do-jogo');
        }

        this.logout = function() {
            localStorageService.set('userID', null);
            localStorageService.set('programaID', null);
            localStorageService.set('departamento', null);
            localStorageService.set('unidade', null);
            localStorageService.set('empresa', null);
            localStorageService.set('empresa_nome', null);
            localStorageService.set('empresaID', null);
            localStorageService.set('foto', null);
            localStorageService.set('email', null);
            localStorageService.set('nome', null);
            localStorageService.set('score', null);
            localStorageService.set('isAdmin', null);
            localStorageService.set('isEspecialista', null);
            localStorageService.set('empresa_inicio', null);
            $auth.logout();

            window.location.href = '/';

        }

    }
]);

var infoContatoNossaMesa = {
    email: "contato@nossamesa.com.br",
    site: "www.nossamesa.com.br",
    tel1: "11 3262-4969"
}
var infoContatoSimplesmente = {
    email: "contato@simplesmentebem.com.br",
    site: "www.simplesmentebem.com.br",
    tel1: "11 3262-4969"
}
