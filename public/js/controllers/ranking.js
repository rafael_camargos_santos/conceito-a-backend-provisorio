'use strict';

/* Controllers */

angular.module('myApp.controller-ranking', []).
controller('RankingController', ['$scope', '$http', 'RankingService', 'localStorageService',
    function($scope, $http, RankingService, localStorageService) {

        var userId = localStorageService.get('userID');
        var departamento = localStorageService.get('departamento');
        var programaID = localStorageService.get('programaID');
        var rankingGeral = [];
        var rankingArea = [];

        var view = this;

        view.foto = localStorageService.get('foto');


        RankingService.getRankGeral()
            .success(function(data) {
                for(var index in data.response.Ranking){
                    rankingGeral.push(data.response.Ranking[index]);
                }
                view.rankingGeral = rankingGeral;
                RankingService.getRankDepartamento(departamento)
                    .success(function(data) {
                      for(var index in data.response.Ranking){
                            if(!data.response.Ranking[index].nome){
                                console.dir(data.response.Ranking[index])
                                data.response.Ranking[index].nome = data.response.Ranking[index].email;
                            }
                            rankingArea.push(data.response.Ranking[index]);
                        }
                        view.rankingDepartamento = rankingArea;
                        RankingService.getRankUsuario(userId)
                            .success(function(data) {
                                view.rankingUserGeral = data.response.geral;
                                view.rankingUserDepartamento = data.response.departamento;
                            })
                            .error(function(err) {
                                // window.location.href = '';
                            });
                    })
                    .error(function(err) {
                        // window.location.href = '';
                    });
            })
            .error(function(err) {
                // window.location.href = '';
            });








        $scope.programa = function() {
            if (programaID === 1) {
                return true;
            }
            return false;
        }
    }
]);
