'use strict';

/* Controllers */

angular.module('myApp.controller-login', []).
controller('LoginController', ['$location', '$scope', '$auth', 'localStorageService', 'md5', 
    function($location, $scope, $auth, localStorageService, md5) {
    var vm = this;
    vm.error = false;

    var url = $location.host();
    $scope.page = {};
    $scope.page.programa = '';

    var programaID = localStorageService.get("programaID");

    //para ambientes de teste rodando local
    // if (url.includes("localhost")){
        // if(programaID == 1){
        //     window.location.href = 'index';
        // }
        // programaID = 1;
        // vm.programaID = 1;
        // $scope.page.programa = "Nossa Mesa";
        // $scope.title = "Conceito A - Nossa Mesa"
    // }

    //comentar abaixo para ambientes de teste:
    if (url.indexOf("nossamesa") != -1) {
        if(programaID == 1){
            window.location.href = 'index';
        }
        programaID = 1;
        vm.programaID = 1;
        $scope.page.programa = "Nossa Mesa";
        $scope.title = "Conceito A - Nossa Mesa"
    } 
    else if(url.indexOf("simplesmentebem") != -1) {
        if(programaID == 2){
            window.location.href = 'index';
        }
        programaID = 2;
        vm.programaID = 2;
        $scope.page.programa = "Simplesmente Bem";
        $scope.title = "Conceito A - Simplesmente Bem"
    }

    $scope.programa = function() {
            if (programaID === 1) {
                return true;
            }
            return false;
        }

    vm.login = function(email, password) {
        var credentials = {
                // email: "camila.mizutani@appsimples.com.br",
                // password: "9cc3fcf33215c003c2ee9179f0ed2574"
                //email: "matheus.venosa@usp.br",
                //password: "9c45808cc9041e00fd230498a907e9ac"
                }
                
        //Tirar o comentario abaixo para logar de vdd
        credentials.email = email;
        credentials.password = md5.createHash(password || '');
        credentials.programaID = programaID;


        // Use Satellizer's $auth service to login
        $auth.login(credentials)
            .then(function(data) {
                var usuario = data.data.usuario;

                if (usuario.programa_id == programaID) {
                    localStorageService.set('userID', usuario._id);
                    localStorageService.set('departamento', usuario.departamento);
                    localStorageService.set('unidade', usuario.unidade);
                    localStorageService.set('empresa', usuario.empresa);
                    localStorageService.set('foto', usuario.foto);
                    localStorageService.set('email', usuario.email);
                    localStorageService.set('nome', usuario.nome);
                    localStorageService.set('empresa_nome', usuario.empresa_nome);
                    localStorageService.set('score', usuario.score);
                    localStorageService.set('programaID', programaID);
                    if (usuario.empresa == '5744b4d122850afd48fa317e' || usuario.empresa == '5755b34bd12638d83f8b4567') {
                        localStorageService.set('isAdmin', true);
                    } 
                    else localStorageService.set('isAdmin', false);

                    if(usuario.empresa == '575706ecd126380e4f8b4567' || usuario.empresa == '575706d7d126380c4f8b4567'){
                        localStorageService.set('isEspecialista', true);
                        window.location.href = 'index';
                    } 
                    else localStorageService.set('isEspecialista', false);

                    // Redireciona para cadastro se for o primeiro login
                    if (usuario.primeiroLogin) {
                        window.location.href = 'cadastro';
                    } else window.location.href = 'index';
                
                }else {

                    vm.error = true;
                }
            })
            .catch(function(err) {
                console.dir(err);
                vm.error = true;
            });
        }

    $scope.goToEsqueciSenha = function() {
        window.location.href = '/esqueci-senha';
    }

    $scope.enterClicked = function() {
        vm.login($scope.user.email, $scope.user.senha);
        return false; 
    }
    
}]);

'use strict';

/* Controllers */
