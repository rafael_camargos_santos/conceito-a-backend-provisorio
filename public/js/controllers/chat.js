'use strict';

/* Controllers */

angular.module('myApp.controller-chat', []).
controller('ChatController', ['$scope', '$rootScope', '$http', 'ChatService', 'localStorageService',
    function($scope, $rootScope, $http, ChatService, localStorageService, MenuController) {

    var view = this;
    var userId = localStorageService.get('userID');
    var programaID = localStorageService.get('programaID');
    var foto = localStorageService.get('foto');
    var isUser = true;

    if(programaID == 1){
        view.foto_adm = "https://s3.amazonaws.com/appsimples-conceito-a/especialista-nossa-mesa.jpg";
    }
    else{
        view.foto_adm = "https://s3.amazonaws.com/appsimples-conceito-a/especialista-simplesmente-bem.jpg";
    }

    var isAdmin = localStorageService.get('isAdmin');
    var isEspecialista = localStorageService.get('isEspecialista');
    if (isAdmin || isEspecialista) {
        isUser = false;
        foto = view.foto_adm;
        if(ChatService.selectedChat.user_id!=null){
            userId = ChatService.selectedChat.user_id;
        }
    }

    ChatService.get(userId)
        .success(function(data){
            view.mensagens = data.response.chat.mensagens;
            view.foto = data.response.chat.user_photo;
            view.foto_conceito_a = data.response.chat.conceito_a_photo;
            $rootScope.$broadcast("CallSetPerfil", {});
        })
        .error(function(err) {
            window.location.href = '';
        });

        $scope.programa = function() {
            if (programaID === 1) {
                return true;
            }
            return false;
        }

        this.getMensagem = function(mensagem) {
            view.mensagens.push({ mensagem: mensagem, is_user: isUser });
            ChatService.post(mensagem, isUser, userId)
                .success(function(data) {
                })
                .error(function(err) {
                    window.location.href = '';
                });

            return true;
        }

        $scope.clearMensagem = function () {
            $scope.mensagem = "";
        };

        this.isAdmin = function() {
            return isAdmin;
        }
    }
]);
