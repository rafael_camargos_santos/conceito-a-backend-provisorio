'use strict';

/* Controllers */

angular.module('myApp.controller-mapa', []).
controller('MapaController', [ '$scope', '$location', 'localStorageService', 'FeedService', 'QuizzesService',
	function($scope, $location, localStorageService, FeedService, QuizzesService) {

	var programaID = localStorageService.get('programaID');
	var tempo_atual = Date.now();
	var view = this;
	var empresa = localStorageService.get('empresa');

    $scope.programa = function() {
        if (programaID === 1) {
            return true;
        }
        return false;
    }

    $scope.nome = localStorageService.get('nome').split(" ")[0];
   
    if(localStorageService.get('score')){
    	$scope.pontos = localStorageService.get('score');
    }else{
    	$scope.pontos = 0;
    }
	

	// seta botoes iniciais desativados
	$scope.semana1 = {};
	$scope.semana2 = {};
	$scope.semana3 = {};
	$scope.semana4 = {};
	$scope.semana5 = {};
	$scope.semana6 = {};
	$scope.semana7 = {};
	$scope.semana8 = {};
	$scope.semana9 = {};
	$scope.semana10 = {};
	$scope.semana11 = {};
	$scope.semana12 = {};

	$scope.semana1.desafio = '/images/desafio_azul2.png';
	$scope.semana1.quiz = '/images/quiz_azul2.png';
	$scope.semana1.conteudo = '/images/conteudo_azul2.png';

	$scope.semana2.desafio = '/images/desafio_verde2.png';
	$scope.semana2.quiz = '/images/quiz_verde2.png';
	$scope.semana2.conteudo = '/images/conteudo2_verde2.png';

	$scope.semana3.desafio = '/images/desafio_verm2.png';
	$scope.semana3.quiz = '/images/quiz_verm2.png';
	$scope.semana3.conteudo = '/images/conteudo3_verm2.png';

	$scope.semana4.desafio = '/images/desafio_azul2.png';
	$scope.semana4.quiz = '/images/quiz_azul2.png';
	$scope.semana4.conteudo = '/images/conteudo4_azul2.png';

	$scope.semana5.desafio = '/images/desafio_verde2.png';
	$scope.semana5.quiz = '/images/quiz_verde2.png';
	$scope.semana5.conteudo = '/images/conteudo5_verde2.png';

	$scope.semana6.desafio = '/images/desafio_verm2.png';
	$scope.semana6.quiz = '/images/quiz_verm2.png';
	$scope.semana6.conteudo = '/images/conteudo6_verm2.png';

	$scope.semana7.desafio = '/images/desafio_azul2.png';
	$scope.semana7.quiz = '/images/quiz_azul2.png';
	$scope.semana7.conteudo = '/images/conteudo7_azul2.png';

	$scope.semana8.desafio = '/images/desafio_verde2.png';
	$scope.semana8.quiz = '/images/quiz_verde2.png';
	$scope.semana8.conteudo = '/images/conteudo8_verde2.png';

	$scope.semana9.desafio = '/images/desafio_verm2.png';
	$scope.semana9.quiz = '/images/quiz_verm2.png';
	$scope.semana9.conteudo = '/images/conteudo9_verm2.png';

	$scope.semana10.desafio = '/images/desafio_azul2.png';
	$scope.semana10.quiz = '/images/quiz_azul2.png';
	$scope.semana10.conteudo = '/images/conteudo10_azul2.png';

	$scope.semana11.desafio = '/images/desafio_verde2.png';
	$scope.semana11.quiz = '/images/quiz_verde2.png';
	$scope.semana11.conteudo = '/images/conteudo11_verde2.png';

	$scope.semana12.desafio = '/images/desafio_verm2.png';
	$scope.semana12.quiz = '/images/quiz_verm2.png';
	$scope.semana12.conteudo = '/images/conteudo12_verm2.png';

	// pega posts do feed
	var posts = [];
	var quizzes = [];
	var desafios = [];
	var conteudos = [];
	FeedService.get(null,empresa)
        .success(function(data) {
            posts = data.response.feed.feed;
            view.start_empresa = data.response.empresa_inicio;
            var t_post = new Date(view.start_empresa);
            var tempo_post = t_post.getTime();
            for(var i = 0; i< posts.length ; i++){
            	if (((tempo_atual - tempo_post)/(1000*60*60*24)) >= (posts[i].dia - 1)){
	            	switch (posts[i].tipo) {
	            		case 0:
	            			quizzes.push(posts[i]);
	            			break;
	            		case 1:
	            			desafios.push(posts[i]);
	            			break;
	            		case 2:
	            			conteudos.push(posts[i]);
	            			break;
	            		default:
	            			break;
	            	}
	            }
            }
            setBtnAtivos();
        })
        .error(function(err) {
            window.location.href = '';
        });

    var setBtnAtivos = function(){
    	// seta botes ativos caso exista o post correspondente
	    // QUIZ
	    if(quizzes.length >= 1){
	    	$scope.semana1.quiz = '/images/quiz_azul1.png';
	    }
	    if(quizzes.length >= 2){
	    	$scope.semana2.quiz = '/images/quiz_verde1.png';
	    }
	    if(quizzes.length >= 3){
	    	$scope.semana3.quiz = '/images/quiz_verm1.png';
	    }
	    if(quizzes.length >= 4){
	    	$scope.semana4.quiz = '/images/quiz_azul1.png';
	    }
	    if(quizzes.length >= 5){
	    	$scope.semana5.quiz = '/images/quiz_verde1.png';
	    }
	    if(quizzes.length >= 6){
	    	$scope.semana6.quiz = '/images/quiz_verm1.png';
	    }
	    if(quizzes.length >= 7){
	    	$scope.semana7.quiz = '/images/quiz_azul1.png';
	    }
	    if(quizzes.length >= 8){
	    	$scope.semana8.quiz = '/images/quiz_verde1.png';
	    }
	    if(quizzes.length >= 9){
	    	$scope.semana9.quiz = '/images/quiz_verm1.png';
	    }
	    if(quizzes.length >= 10){
	    	$scope.semana10.quiz = '/images/quiz_azul1.png';
	    }
	    if(quizzes.length >= 11){
	    	$scope.semana11.quiz = '/images/quiz_verde1.png';
	    }
	    if(quizzes.length >= 12){
	    	$scope.semana12.quiz = '/images/quiz_verm1.png';
	    }

	    // DESAFIOS
	    if(desafios.length >= 1){
	    	$scope.semana1.desafio = '/images/desafio_azul1.png';
	    }
	    if(desafios.length >= 2){
	    	$scope.semana2.desafio = '/images/desafio_verde1.png';
	    }
	    if(desafios.length >= 3){
	    	$scope.semana3.desafio = '/images/desafio_verm1.png';
	    }
	    if(desafios.length >= 4){
	    	$scope.semana4.desafio = '/images/desafio_azul1.png';
	    }
	    if(desafios.length >= 5){
	    	$scope.semana5.desafio = '/images/desafio_verde1.png';
	    }
	    if(desafios.length >= 6){
	    	$scope.semana6.desafio = '/images/desafio_verm1.png';
	    }
	    if(desafios.length >= 7){
	    	$scope.semana7.desafio = '/images/desafio_azul1.png';
	    }
	    if(desafios.length >= 8){
	    	$scope.semana8.desafio = '/images/desafio_verde1.png';
	    }
	    if(desafios.length >= 9){
	    	$scope.semana9.desafio = '/images/desafio_verm1.png';
	    }
	    if(desafios.length >= 10){
	    	$scope.semana10.desafio = '/images/desafio_azul1.png';
	    }
	    if(desafios.length >= 11){
	    	$scope.semana11.desafio = '/images/desafio_verde1.png';
	    }
	    if(desafios.length >= 12){
	    	$scope.semana12.desafio = '/images/desafio_verm1.png';
	    }

	    // CONTEUDO
	    if(conteudos.length >= 1){
	    	$scope.semana1.conteudo = '/images/conteudo_azul1.png';
	    }
	    if(conteudos.length >= 2){
	    	$scope.semana2.conteudo = '/images/conteudo2_verde1.png';
	    }
	    if(conteudos.length >= 3){
	    	$scope.semana3.conteudo = '/images/conteudo3_verm1.png';
	    }
	    if(conteudos.length >= 4){
	    	$scope.semana4.conteudo = '/images/conteudo4_azul1.png';
	    }
	    if(conteudos.length >= 5){
	    	$scope.semana5.conteudo = '/images/conteudo5_verde1.png';
	    }
	    if(conteudos.length >= 6){
	    	$scope.semana6.conteudo = '/images/conteudo6_verm1.png';
	    }
	    if(conteudos.length >= 7){
	    	$scope.semana7.conteudo = '/images/conteudo7_azul1.png';
	    }
	    if(conteudos.length >= 8){
	    	$scope.semana8.conteudo = '/images/conteudo8_verde1.png';
	    }
	    if(conteudos.length >= 9){
	    	$scope.semana9.conteudo = '/images/conteudo9_verm1.png';
	    }
	    if(conteudos.length >= 10){
	    	$scope.semana10.conteudo = '/images/conteudo10_azul1.png';
	    }
	    if(conteudos.length >= 11){
	    	$scope.semana11.conteudo = '/images/conteudo11_verde1.png';
	    }
	    if(conteudos.length >= 12){
	    	$scope.semana12.conteudo = '/images/conteudo12_verm1.png';
	    }
    }

    //logica dos estados habilitar clique/redirecionamentos 

	    $scope.goToFeed = function(id) {
	    	if(conteudos.length>=id){
	    		var postSemana = conteudos[conteudos.length - id].semana;
	    		FeedService.setPostSemana(postSemana);
	    		$location.path('feed');
	    	}
	    };
	    $scope.goToDesafio = function(id) {
	    	if(desafios.length>=id){
	    		var postSemana = desafios[desafios.length - id].semana;
	    		FeedService.setPostSemana(postSemana);
	    		$location.path('feed');
	    	}
	    };
	    $scope.goToQuiz = function(id) {
	    	if(quizzes.length>=id){
	    		var quizSemana = quizzes[quizzes.length - id].semana;
	    		// QuizzesService.setQuizId(quizId);
	    		FeedService.setPostSemana(quizSemana);
	        	$location.path('feed');
	    	}
	    };

}]);