'use strict';

/* Controllers */

angular.module('myApp.controller-administrador', []).
    controller('AdministradorController', ['$scope', '$http' , '$location', 'EmpresaService', 'localStorageService', 
        function($scope, $http , $location, EmpresaService, localStorageService) {
    
    var programaID = localStorageService.get('programaID');
    var isAdmin = localStorageService.get('isAdmin');
    var isEspecialista = localStorageService.get('isEspecialista');
    var view = this;
    view.empresas = [];

    var getEmpresas = function(){
        EmpresaService.getEmpresas()
            .success(function(data) {
                view.empresas = data.response.empresa;
                for(var i in view.empresas){
                    if(view.empresas[i].ativo == true) view.empresas[i].status = "Desativar";
                    else view.empresas[i].status = "ativar";
                }
            })
            .error(function(err){
                console.log(err);
            });
    }

    getEmpresas();

    view.seeChats = function(){
        return isAdmin || isEspecialista;
    }
    
    view.isAdmin = function(){
        return isAdmin;
    }


    $scope.programa = function() {
        if (programaID === 1) {
            return true;
        }
        return false;
    }

    //salva empresa no service
    view.chooseEmpresa = function (empresa) {
        localStorageService.set('empresaID', empresa._id);
        EmpresaService.chooseEmpresa(empresa);
        //$location.path("usuarios");
        $location.path("empresa");
    }
    view.chooseEmpresaEditar = function (empresa) {
        EmpresaService.chooseEmpresa(empresa);
        $location.path("cadastro-empresa");
    }

    view.ativarDesativarEmpresa = function (empresa) {
        if(empresa.ativo == true){
            EmpresaService.desativarEmpresa(empresa);
        }
        else {
            EmpresaService.ativarEmpresa(empresa);
        }
        getEmpresas();
    }

    view.goTo = function(path){
        $location.path(path);
    }

}]);
