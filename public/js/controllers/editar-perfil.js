'use strict';

/* Controllers */

angular.module('myApp.controller-editar-perfil', []).
controller('EditarPerfilCtrl', ['$scope', 'localStorageService', 'PerfilService', '$location', 'S3Service',
    function($scope, localStorageService, PerfilService, $location, S3Service) {
    var view = this;
    var userID = localStorageService.get('userID');
    var programaID = localStorageService.get('programaID');
    var fotoPerfilURL;

    $scope.user = localStorageService.get('nome');
    $scope.url = localStorageService.get('foto') + '?r=' + Math.round(Math.random() * 999999);
    view.loading = false;

    // Tratamento de erro do upload para S3:
    var showFileSizeError = function(){
        console.dir("tamanho do arquivo excede o máximo permitido.");
        // TODO: mostrar erro
    }

    var showUploadError = function(){
        console.dir("erro no upload do arquivo.");
        // TODO: mostrar erro
    }

    var showWrongExtensionError = function(){
        console.dir("Extensao nao permitida");
        // TODO: mostrar erro
    }

    var profilePicFilename = programaID + "/" + userID + "/profile_pic";

    var s3Url = S3Service.getS3URL();

    $scope.getLoading = function(){
        return view.loading;
    }

    // Opcoes do upload para S3
    $scope.options = {
        multiple: false,
        immediate: true,
        extensions: ["jpg", "png"],
        bucket: "appsimples-conceito-a",
        acl: "public-read",
        policyUrl: s3Url,
        filename: profilePicFilename,
        on_filesize: function() {showFileSizeError()},
        on_upload: function() {showUploadError()},
        on_extension: function(){showWrongExtensionError()},
        on_success: function() {
            view.loading = false;
            fotoPerfilURL = $scope.files[0].real
            $scope.url = $scope.files[0].real + '?r=' + Math.round(Math.random() * 999999);
        },
        on_error: function(){
            view.loading = false;
        },
        on_uploadStart: function(){
            view.loading = true;
        }
    }

    // arquivos do S3
    $scope.files = [];

    $scope.getUser = function(nome) {
        if (angular.copy(nome).length > 0) {
            $scope.user = angular.copy(nome);

            var json = {};
                json._id = userID;
                json.nome = $scope.user;
                json.foto = fotoPerfilURL;

            PerfilService.post(json)
                .success(function(data) {
                    if (data.success === false) {
                        console.dir("erro no cadastro");
                        // TODO: mostrar erro;
                    };

                    PerfilService.atualizaPerfil();
                    $location.path('index');
                })
                .error(function(err) {
                    console.dir("erro no post");
                    // TODO: mostrar erro;
                });
        }
    }
}]);
