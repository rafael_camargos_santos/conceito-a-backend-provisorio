'use strict';

/* Controllers */

angular.module('myApp.controller-relatorio-programa', []).
controller('RelatorioProgramaController', ['$scope', '$http', '$location', 'EmpresaService', 'localStorageService',
    function($scope, $http, $location, EmpresaService, localStorageService) {

        var programaID = localStorageService.get('programaID');
        var empresaID = localStorageService.get('empresaID');
        var view = this;
        this.porcentagem = [];

        this.semanas = [
            { semana: 1 },
            { semana: 2 },
            { semana: 3 },
            { semana: 4 },
            { semana: 5 },
            { semana: 6 },
            { semana: 7 },
            { semana: 8 },
            { semana: 9 },
            { semana: 10 },
            { semana: 11 },
            { semana: 12 }
        ];

        $scope.programa = function() {
            if (programaID === 1) {
                return true;
            }
            return false;
        }

        EmpresaService.getEmpresaById(empresaID)
            .success(function(data) {
                view.empresa = data.response.empresa[0];
            })
            .error(function(err) {
                console.log(err);
            });

        EmpresaService.relatorioQuizzes(empresaID, programaID)
            .success(function(data) {
                //console.log(data);
                view.quizzes = data.response.quizzes;
                if (!angular.isUndefined(view.conteudos)) {
                    view.conteudos.sort(function(a, b) {
                        return a.semana - b.semana;
                    })
                }
                for (var i = 0; i < view.quizzes.length; i++) {
                    var total = 0;
                    for (var j = 0; j < view.quizzes[i].opcoes.length; j++) {
                        total += view.quizzes[i].opcoes[j].count;
                    }
                    view.porcentagem.push(total);
                }
            })
            .error(function(err) {
                console.log(err);
            });

        EmpresaService.relatorioDesafios(empresaID, programaID)
            .success(function(data) {
                view.desafios = data.response.desafios;
                view.desafios.sort(function(a, b) {
                        return a.semana - b.semana;
                    })
                    //console.log(data);
            })
            .error(function(err) {
                console.log(err);
            });

        EmpresaService.relatorioComplementos(empresaID, programaID)
            .success(function(data) {
                view.complementos = data.response.complementos;
                view.complementos.sort(function(a, b) {
                    return a.semana - b.semana;
                })
            })
            .error(function(err) {
                console.log(err);
            });

        EmpresaService.relatorioConteudos(empresaID, programaID)
            .success(function(data) {
                view.conteudos = data.response.conteudos;
                view.conteudos.sort(function(a, b) {
                        return a.semana - b.semana;
                    })
                    //console.log(data);
            })
            .error(function(err) {
                console.log(err);
            });

        this.apresentaPorcentagem = function(contOpcao, contTotal) {
            if (contOpcao == 0) {
                return 0;
            } else {
                return ((contOpcao / contTotal) * 100);
            }
        }
    }
]);
