'use strict';

/* Controllers */

angular.module('myApp.controller-cadastro', ['LocalStorageModule', 'angular.aws.s3', 'satellizer', 'myApp.perfilService', 'myApp.s3Service']).
controller('CadastroController', ['$scope', 'localStorageService', 'PerfilService', 'S3Service',
    function($scope, localStorageService, PerfilService, S3Service) {

        var userID = localStorageService.get('userID');
        var programaID = localStorageService.get('programaID');
        $scope.url = "//s3.amazonaws.com/appsimples-conceito-a/perfil_ok.png";

        // Tratamento de erro do upload para S3:
        var showFileSizeError = function() {
            console.dir("tamanho do arquivo excede o máximo permitido.");
            // TODO: mostrar erro
        }

        var showUploadError = function() {
            console.dir("erro no upload do arquivo.");
            // TODO: mostrar erro
        }

        var showWrongExtensionError = function() {
            console.dir("Extensao nao permitida");
            // TODO: mostrar erro
        }

        var profilePicFilename = programaID + "/" + userID + "/profile_pic";

        var s3Url = S3Service.getS3URL();

        // Opcoes do upload para S3
        $scope.options = {
            multiple: false,
            immediate: true,
            extensions: ["jpg", "png"],
            bucket: "appsimples-conceito-a",
            acl: "public-read",
            policyUrl: s3Url,
            filename: profilePicFilename,
            on_filesize: function() { showFileSizeError() },
            on_upload: function() { showUploadError() },
            on_extension: function() { showWrongExtensionError() },
            on_success: function() {
                $scope.url = $scope.files[0].real;
                console.dir($scope.url);
            },
        }

        // arquivos do S3
        $scope.files = [];

        $scope.user = {};

        $scope.getUser = function(nome) {
            if (angular.copy(nome).length > 0) {
                $scope.user = angular.copy(nome);


                var json = {};
                json._id = userID;
                json.nome = $scope.user;
                json.foto = $scope.url;

                PerfilService.post(json)
                    .success(function(data) {
                        if (data.success === false) {
                            console.dir("erro no cadastro");
                            // TODO: mostrar erro;
                        }else{
                            //TODO: Trocar o programa id e colocar a foto
                            localStorageService.set('foto', json.foto);
                            localStorageService.set('nome', $scope.user);
                            localStorageService.set('score', 0);
                             window.location.href = 'index';

                        }

                       
                    })
                    .error(function(err) {
                        console.dir("erro no post");
                        // TODO: mostrar erro;
                    });
            }
        }
        
        $scope.programa = function() {
            if (programaID === 1) {
                return true;
            }
            return false;
        }

        // seta nome da tab
        $scope.page = {};
        $scope.page.programa = '';

        if (programaID == 1){
            $scope.page.programa = "Nossa Mesa";
        }
        else if(programaID == 2){
            $scope.page.programa = "Simplesmente Bem";
        }
    }
]);
