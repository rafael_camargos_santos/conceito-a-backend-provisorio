'use strict';

/* Controllers */

angular.module('myApp.controller-empresa', []).
controller('EmpresaController', ['$scope', '$http', '$location', 'EmpresaService', 'localStorageService',
    function($scope, $http, $location, EmpresaService, localStorageService) {

        var view = this;

        var programaID = localStorageService.get('programaID');
        var empresaID = localStorageService.get('empresaID');
        
        var isAdmin = localStorageService.get('isAdmin');
        
        view.isAdmin = function() {
            return isAdmin;
        }

        EmpresaService.getEmpresaById(empresaID)
            .success(function(data) {
                view.empresa = data.response.empresa[0];
            })
            .error(function(err) {
                console.log(err);
            });


        $scope.programa = function() {
            if (programaID === 1) {
                return true;
            }
            return false;
        }

        // alterna entre views (urls)
        view.changeView = function(view) {
            $location.path(view); // path not hash
        }

    }
]);
