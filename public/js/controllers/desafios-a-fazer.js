'use strict';

/* Desafio Controller */

angular.module('myApp.controller-desafios-a-fazer', [])
    .controller('DesafiosAFazerController', ['$scope', '$location', 'DesafiosService', 'localStorageService', 'PerfilService', 'S3Service',
        function($scope, $location, DesafiosService, localStorageService, PerfilService, S3Service) {

            var view = this;
            var userID = localStorageService.get('userID');
            var nome = localStorageService.get('nome');
            var programaID = localStorageService.get('programaID');
            var empresa = localStorageService.get('empresa');
            var userFoto = localStorageService.get('foto');
            var nome = localStorageService.get('nome');
            var unidade = localStorageService.get('unidade');
            var departamento = localStorageService.get('departamento');
            var desafioFoto;
            var isAdm = localStorageService.get('isAdmin');

            view.loading = false;
            view.msg = "";

            $scope.photoHere = false;
            $scope.url = {};
            $scope.isEdit = true;
            $scope.desafio = DesafiosService.getDesafio();
            console.log($scope.desafio.diasRestantes);
            if($scope.desafio == undefined){
                window.location.href = '';
            }
            $scope.resposta = $scope.desafio.texto;
            $scope.url = $scope.desafio.imagem;
            $scope.files = [""];
            $scope.desafio._id = $scope.desafio._id;
            $scope.msgComplete = "Desafio editado!";
            $scope.msgPoints = "";
            if ($scope.desafio.isRespondido === false) {
                $scope.isEdit = false;
                $scope.msgComplete = "Obrigado por responder ao desafio!";
                $scope.msgPoints = " Você ganhou 100 pontos!";
            }


            // Tratamento de erro do upload para S3:
            var showFileSizeError = function() {
                console.dir("tamanho do arquivo excede o máximo permitido.");
                // TODO: mostrar erro
            }

            var showUploadError = function() {
                console.dir("erro no upload do arquivo.");
                // TODO: mostrar erro
            }

            var showWrongExtensionError = function() {
                console.dir("Extensao nao permitida");
                // TODO: mostrar erro
            }

            var uploading = function() {
                upou = false;
            }

            var desafioFilename = programaID + "/" + userID + "/desafio=" + $scope.desafio.semana;

            var s3Url = S3Service.getS3URL();



            // Opcoes do upload para S3
            $scope.options = {
                multiple: false,
                immediate: true,
                extensions: ["jpg", "jpeg", "png"],
                bucket: "appsimples-conceito-a",
                acl: "public-read",
                policyUrl: s3Url,
                filename: desafioFilename,
                on_uploadStart: function() {
                    view.loading = true;
                },
                on_filesize: function() { showFileSizeError() },
                on_upload: function() { showUploadError() },
                on_extension: function() { showWrongExtensionError() },
                on_success: function() {
                    desafioFoto = $scope.files[0].real;
                    $scope.url = $scope.files[0].real;
                    view.loading = false;
                },
                on_error: function() {
                    view.loading = false;
                },
                on_required: function() {
                    console.log("Passou aqui required");
                    view.loading = false;
                }

            }


            // arquivos do S3
            $scope.files = [];

            $scope.resposta;


            $scope.programa = function() {
                if (programaID === 1) {
                    return true;
                }
                return false;
            }

            this.cadastrar = function() {
                if ($scope.isEdit === false) {
                    if (!isAdm) {
                        var json = {};
                        json.user_id = userID;
                        json.titulo = $scope.desafio.titulo;
                        json.data = new Date();
                        json.numero = $scope.desafio.semana;
                        json.texto = $scope.resposta;
                        json.imagem = desafioFoto;
                        json.empresa_id = empresa;
                        json.user_name = nome;
                        json.user_foto = userFoto;
                        json.unidade = unidade;
                        json.departamento = departamento;
                        json.user_name = nome;
                        json.comentarios = [];
                        json.desafio_id = $scope.desafio._id;


                        DesafiosService.post(json)
                            .success(function(data) {
                                if (data.success === false) {
                                    console.dir("erro no post");
                                    // TODO: mostrar erro;
                                };
                                PerfilService.atualizaPerfil();
                                $scope.feedbackDesafio = true;
                                // $location.path('desafios');
                            })
                            .error(function(err) {
                                console.dir("erro no post");
                                // TODO: mostrar erro;
                            });
                    }
                } else {

                    var json = {};
                    json.texto = $scope.resposta;
                    if ($scope.url) json.imagem = $scope.url;
                    json.post_id = $scope.desafio._id;
                    DesafiosService.editarDesafio(json)
                        .success(function(data) {
                            if (data.success === false) {
                                console.dir("erro no post");
                                // TODO: mostrar erro;
                            };
                            PerfilService.atualizaPerfil();
                            $scope.feedbackDesafio = true;
                            //$location.path('perfil');
                        })
                        .error(function(err) {
                            console.dir(err);
                        })
                }
            }

            // alterna entre views (urls)
            view.changeView = function() {
                if ($scope.isEdit === false) {
                    $location.path('desafios');
                    return;
                }

                if ($scope.isEdit === true) {
                    $location.path('perfil');
                    return;
                }
            }

            var diasRestantes = function() {
                var diasRestantes = $scope.desafio.diasRestantes;
                if (diasRestantes == 7){
                    return "Restam 7 dias"
                }
                else if (diasRestantes == -1) {
                    return "Desafio encerrado";
                } else if (diasRestantes == 1) {
                    return "Resta 1 dia"
                }
                else {
                    return "Restam " + diasRestantes + " dias.";
                }
            }

            $scope.desafio.diasRestantes = diasRestantes();

        }
    ])
