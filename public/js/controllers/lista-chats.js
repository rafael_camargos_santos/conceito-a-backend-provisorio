'use strict';

/* Controllers */

angular.module('myApp.controller-listachats', []). 
controller('ListaChatsController', ['$scope', '$http' , '$location', 'localStorageService',  'ChatService' , 
	function($scope, $http, $location, localStorageService, ChatService) {
	var view = this;
    view.chatsTemp = [];
    var programaID = localStorageService.get('programaID')
    view.chats = [];
    
    ChatService.getAll(programaID)
            .success(function(data) {
                view.chatsTemp = data.response.chats;
                for (var i = 0; i < view.chatsTemp.length; i++) {
                    view.chats.push(view.chatsTemp[i]);
                }; 
            })
            .error(function(err){
                console.log(err);
            });

    view.chooseChat = function(chat) {
        //console.log("addUsers");
        ChatService.selectedChat = chat;
        $location.path('/chat');
    }

    var programaID = localStorageService.get('programaID');

    $scope.programa = function() {
        if (programaID === 1) {
            return true;
        }
        return false;
    }

}]);
