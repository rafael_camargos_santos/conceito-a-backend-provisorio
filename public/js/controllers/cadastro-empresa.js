'use strict';

/* Controllers */

angular.module('myApp.controller-cadastro-empresa', []).
controller('CadastroEmpresaController', ['$scope', '$http', '$location', 'EmpresaService', 'localStorageService',
    function($scope, $http, $location, EmpresaService, localStorageService) {

        var view = this;
        view.empresaInfo = EmpresaService.selectedEmpresa;

        view.success = false;
        view.error = false;
        view.loading = false;
        view.confirm = false;
        view.isNovaEmpresa = false;
        view.msg = "";

        this.mostraAreasExistentes = function(areasExistentes) {
            var lenAreasExistentes = areasExistentes.length;
            $scope.areas = [];
            $scope.indexAreas = [];
            $scope.novoCampoArea = false;

            if (lenAreasExistentes > 0) {
                $scope.areas.push(areasExistentes[0]);
                for (var i = 1; i < lenAreasExistentes; i++) {
                    if (areasExistentes[i] != "") {
                        $scope.novoCampoArea = true;
                        $scope.indexAreas.push($scope.areas.length);
                        $scope.areas.push(areasExistentes[i]);
                    }
                }
            }
        };

        this.mostraUnidadesExistentes = function(unidadesExistentes) {
            var lenUnidadesExistentes = unidadesExistentes.length;
            $scope.unidades = [];
            $scope.indexUnidades = [];
            $scope.novoCampoUnidade = false;

            if (lenUnidadesExistentes > 0) {
                $scope.unidades.push(unidadesExistentes[0]);
                for (var i = 1; i < lenUnidadesExistentes; i++) {
                    if (unidadesExistentes[i] != "") {
                        $scope.novoCampoUnidade = true;
                        $scope.indexUnidades.push($scope.unidades.length);
                        $scope.unidades.push(unidadesExistentes[i]);
                    }
                }
            }
        };

        if (angular.isUndefined(view.empresaInfo)) {
            // Cadastro de nova empresa
            view.isNovaEmpresa = true;
            $scope.nome = "";
            $scope.data = new Date();

            $scope.areas = [];
            $scope.indexAreas = [];
            $scope.novoCampoArea = false;

            $scope.unidades = [];
            $scope.indexUnidades = [];
            $scope.novoCampoUnidade = false;
        } else {
            // Atualização do cadastro da empresa
            $scope.nome = view.empresaInfo.nome;
            $scope.data = new Date(view.empresaInfo.data);
            var areasExistentes = view.empresaInfo.departamento;
            var unidadesExistentes = view.empresaInfo.unidade;
            view.mostraAreasExistentes(areasExistentes);
            view.mostraUnidadesExistentes(unidadesExistentes);
        };

        var programaID = localStorageService.get('programaID');

        $scope.programa = function() {
            if (programaID === 1) {
                return true;
            }
            return false;
        };

        this.adicionaArea = function() {
            if (($scope.indexAreas.length == ($scope.areas.length - 1)) && (!($scope.areas[$scope.areas.length - 1] == ""))) {
                $scope.novoCampoArea = true;
                $scope.indexAreas.push($scope.areas.length);
            }
        };

        this.adicionaUnidade = function() {
            if (($scope.indexUnidades.length == ($scope.unidades.length - 1)) && (!($scope.unidades[$scope.unidades.length - 1] == ""))) {
                $scope.novoCampoUnidade = true;
                $scope.indexUnidades.push($scope.unidades.length);
            }
        };

        $scope.salvar = function(nomeTemp, dataTemp) {
            view.loading = true;
            if (angular.isUndefined(view.empresaInfo)) {
                //Cadastro de nova empresa
                if ($scope.nome.length > 0) {
                    EmpresaService.cadastra($scope.nome, $scope.data, $scope.areas, $scope.unidades)
                        .success(function(data) {
                            if (data.success === true) {
                                view.sucesso();
                                view.msg = "A empresa foi cadastrada com sucesso.";
                            } else {
                                console.dir("erro no cadastro");
                                view.erro();
                                view.msg = "Não foi possível cadastrar a empresa.";
                            };

                        })
                        .error(function(err) {
                            console.dir("erro no cadastro");
                            view.erro();
                            view.msg = "Não foi possível cadastrar a empresa.";
                        });
                } else {
                    view.loading = false;
                    window.alert("Insira um nome para a empresa.");
                }
            } else {
                // Eliminação de quaisquer slots vazios dos vetores $scope.areas e $scope.unidades 
                var areasValidas = [];
                for (var i = 0; i < $scope.areas.length; i++) {
                    if (($scope.areas[i] != "") && ($scope.areas[i] != null)) {
                        areasValidas.push($scope.areas[i]);
                    }
                }
                var unidadesValidas = [];
                for (var i = 0; i < $scope.unidades.length; i++) {
                    if (($scope.unidades[i] != "") && ($scope.unidades[i] != null)) {
                        unidadesValidas.push($scope.unidades[i]);
                    }
                }
                //Atualização de uma empresa
                if (!(angular.isUndefined($scope.nome)) && $scope.nome.length > 0) {
                    EmpresaService.atualiza(view.empresaInfo._id, $scope.nome, $scope.data, areasValidas, unidadesValidas)
                        .success(function(data) {
                            if (data.success === true) {
                                view.sucesso();
                                view.msg = "A empresa foi atualizada com sucesso.";
                            } else {
                                console.dir("erro na atualização");
                                view.erro();
                                view.msg = "Não foi possível atualizar a empresa.";
                            };

                        })
                        .error(function(err) {
                            console.dir("erro na atualização");
                            view.erro();
                            view.msg = "Não foi possível atualizar a empresa.";
                        });
                } else {
                    view.loading = false;
                    window.alert("Insira um nome para a empresa.");
                }
            }
        };

        this.sucesso = function() {
            view.loading = false;
            view.success = true;
        };

        this.erro = function() {
            view.loading = false;
            view.error = true;
        };

        this.voltar = function() {
            this.success = false;
            this.error = false;
            this.loading = false;
            history.back();
        };

        this.deletarEmpresa = function() {
            this.loading = true;
            EmpresaService.deleteEmpresa (view.empresaInfo)
                .success(function(data) {
                    if (data.success === true) {
                        view.sucesso();
                        view.msg = "Empresa deletada com sucesso."
                    } else {
                        view.erro();
                        view.msg = "Não foi possível deletar a empresa.";
                    }
                })
                .error(function(err) {
                    console.dir("ERR");
                    view.erro();
                    view.msg = "Não foi possível deletar a empresa.";
                });
        };
    }
]);
