'use strict';

/* Desafio Controller */

angular.module('myApp.controller-desafios', []).
controller('DesafioController', ['$location', '$scope', '$http', 'DesafiosService', 'CommentsService', 'LikesService', 'localStorageService', 'PerfilService', 'EmpresaService',
    function($location, $scope, $http, DesafiosService, CommentsService, LikesService, localStorageService, PerfilService, EmpresaService) {

        $scope.filtro = {};
        var view = this;
        var empresaID = localStorageService.get('empresa');
        var userID = localStorageService.get('userID');
        var programaID = localStorageService.get('programaID');
        var nome = localStorageService.get('nome');
        var curtido = false;
        view.likes = [];
        view.comments = [];
        view.semanas = [
            { nome: 'Semana 1', numero: 1 },
            { nome: 'Semana 2', numero: 2 },
            { nome: 'Semana 3', numero: 3 },
            { nome: 'Semana 4', numero: 4 },
            { nome: 'Semana 5', numero: 5 },
            { nome: 'Semana 6', numero: 6 },
            // { nome: 'Semana 7', numero: 7 },
            // { nome: 'Semana 8', numero: 8 },
            // { nome: 'Semana 9', numero: 9 },
            // { nome: 'Semana 10', numero: 10 },
            // { nome: 'Semana 11', numero: 11 },
            // { nome: 'Semana 12', numero: 12 }
        ]
        $scope.isDesafios = true;

        var setLikes = function(){
            for (var i in view.desafios) {
                for (var index in view.likes) {
                    if (view.desafios[i]._id == view.likes[index].id_post && view.likes[index].id_usuario == userID) {
                        view.desafios[i].isLiked = true;
                    }
                }
            }
        }

        LikesService.get(empresaID)
            .success(function(data){
                for (var index in data.response.likes){
                    view.likes.push(data.response.likes[index]);
                }
            })
            .error(function(err){
                window.location.href = '';
            })

        CommentsService.get(empresaID)
            .success(function(data){
                for (var index in data.response.comments){
                    view.comments.push(data.response.comments[index]);
                }
            })
            .error(function(err){
                window.location.href = '';
            })

        DesafiosService.getEmpresa(empresaID)
            .success(function(data) {
                view.desafios = data.response.desafios;
                view.userID = userID;
                setLikes();
            })
            .error(function(err) {
                window.location.href = '';
            });

        EmpresaService.getEmpresaById(empresaID)
            .success(function(data) {
                view.departamento = data.response.empresa[0].departamento;
                view.unidade = data.response.empresa[0].unidade;
            })
            .error(function(err) {
                console.dir(err);
            });

        this.goUserPerfil = function(friendID) {
            PerfilService.chooseUser(friendID);
            // console.dir(PerfilService.selectedUser);
            $location.path('perfil');
        };

        // this.desafios = post_desafios;

        // this.setShow = function(context, show) {
        //     context.comentarioIsShow = show;
        // };

        // this.isShow = function(context) {
        //     return (context.comentarioIsShow === true);
        // };

        $scope.programa = function() {
            if (programaID === 1) {
                return true;
            }
            return false;
        }

        $scope.setFiltroById = function(fase, unidade, departamento) {
            $scope.filtro.numero = fase;
            $scope.filtro.unidade = unidade;
            $scope.filtro.departamento = departamento;
        }

        this.postLike = function(context, $event) {
            curtido = false;
            view.liked = false;
            for (var index in view.likes) {
                if (context._id == view.likes[index].id_post && view.likes[index].id_usuario == userID && context.isLiked) {
                    curtido = true;
                }
            }
            if (curtido == false) {
                curtido = true;
                view.likes.push({id_post: context._id, id_usuario: userID});
                context.isLiked = true;
                context.curtidas++;
                DesafiosService.postLike(context._id,context.curtidas)
                    .success(function(data){
                    LikesService.post(context._id, empresaID, userID)
                        .success(function(data) {
                        })
                        .error(function(err) {
                            window.location.href = '';
                        });

                    })
                    .error(function(err) {
                        window.location.href = '';
                    });
            }
        }

        this.postComentario = function(context,comentario){
            DesafiosService.postComentario(context._id, comentario, nome)
                .success(function(data){
                    context.comentarios.push({nome: nome, comentario:comentario});
                    CommentsService.post(context._id, empresaID, userID, comentario.comentario, new Date(), nome)
                        .success(function(data){
                            console.dir(comentario);
                        })
                        .error(function(data){
                            window.location.href = '';
                        })
                })
                .error(function(data){

                })
        }

        $scope.getComentario = function(comentario, desafio) {
            if (angular.copy(comentario) != null) {
                if (angular.copy(comentario).length > 0) {
                    CommentsService.post(desafio._id, userID, empresaID, comentario, new Date(), nome)
                        .success(function(data) {
                            console.dir(data);
                        })
                        .error(function(err) {
                            console.dir(err);
                        });
                }
            }
        }

    }
])
