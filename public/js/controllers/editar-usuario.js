'use strict';

/* Controllers */

angular.module('myApp.controller-editar-usuario', []).
controller('EditarUsuarioController', ['$scope', '$window', 'localStorageService', 'EmpresaService', 'PerfilService',
    function($scope, $window, localStorageService, EmpresaService, PerfilService) {

        var view = this;
        var programaID = localStorageService.get('programaID');
        view.confirm = false;
        view.success = false;
        view.error = false;
        view.loading = false;
        view.msg = "";

        $scope.programa = function() {
            if (programaID === 1) {
                return true;
            }
            return false;
        }

        view.empresa = EmpresaService.selectedEmpresa;
        view.usuario = EmpresaService.selectedUsuario;

        this.save = function() {
            this.loading = true;
            EmpresaService.updateUser(view.usuario)
                .success(function(data) {
                    if (data.success === true) {
                        view.sucesso();
                        view.msg = "Usuário editado com sucesso.";
                    } else {
                        view.erro();
                        view.msg = "Não foi possível editar o usuário.";
                    }
                })
                .error(function(err) {
                    console.dir("ERR");
                    view.erro();
                    view.msg = "Não foi possível editar o usuário.";
                });
        }

        this.delete = function() {
            this.loading = true;
            // console.log(view.usuario.nome + ' + ID = ' + view.usuario._id);
            EmpresaService.deleteUser(view.usuario)
                .success(function(data) {
                    if (data.success === true) {
                        view.sucesso();
                        view.msg = "Usuário deletado com sucesso."
                    } else {
                        view.erro();
                        view.msg = "Não foi possível deletar o usuário.";
                    }
                })
                .error(function(err) {
                    console.dir("ERR");
                    view.erro();
                    view.msg = "Não foi possível deletar o usuário.";
                });
        }
        
        this.sucesso = function() {
            view.loading = false;
            view.success = true;
        }

        this.erro = function() {
            view.loading = false;
            view.error = true;
        }

        this.voltar = function() {
            this.success = false;
            this.error = false;
            this.loading = false;
            history.back();
        }
    }
]);
