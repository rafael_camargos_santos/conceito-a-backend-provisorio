'use strict';

/* Directives */

angular.module('myApp.directives', [
    'myApp.controllers',
    'myApp.controller-desafios',
    'myApp.controller-quiz',
    'myApp.controller-ranking',
    'myApp.controller-perfil',
    'myApp.controller-chat',
    'myApp.controller-mapa',
    'myApp.controller-feed',
    'myApp.controller-cadastro',
    'myApp.controller-ferramentas',
    'myApp.controller-administrador',
    'myApp.controller-adm-usuarios',
    'myApp.controller-relatorio-programa',
    'myApp.controller-relatorio-usuarios',
    'myApp.controller-empresa',
    'myApp.controller-regras-do-jogo'
]).
directive('appVersion', function(version) {
    return function(scope, elm, attrs) {
        elm.text(version);
    };
}).
directive('ngPostfoto', function() {
    function link($scope, elem, attrs, ctrl) {
        $scope.show = true;

        $scope.isShow = function() {
            return !$scope.show;
        }

        $scope.setShow = function() {
            return $scope.show = !$scope.show;
        }
    }

    return {
        restrict: 'E',
        templateUrl: 'directives/desafiosItem.html',
        scope: true,
        link: link
    }
}).
directive('ngFeedquiz', function() {
    return {
        restrict: 'E',
        templateUrl: 'directives/feedQuiz.html'
    }
}).
directive('ngFeeddesafios', function() {
    return {
        restrict: 'E',
        templateUrl: 'directives/feedDesafios.html'
    }
}).
directive('ngFeedcomplemento', function() {
    function link($scope, elem, attrs, ctrl) {
        $scope.show = true;
        $scope.openFlag = false;

        $scope.isShow = function() {
            return !$scope.show;
        }

        $scope.setShow = function() {
            return $scope.show = !$scope.show;
        }

        $scope.openComplemento = function() {
            $scope.openFlag = !$scope.openFlag;
        };
    }

    return {
        restrict: 'E',
        templateUrl: 'directives/feedComplemento.html',
        scope: true,
        link: link
    }
}).
directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if (event.which === 13) {
                scope.$apply(function() {
                    scope.$eval(attrs.ngEnter, { 'event': event });
                });

                event.preventDefault();
            }
        });
    };
}).
directive('ngFeedconteudo', ['FeedService', 'localStorageService', 'PerfilService',
    function(FeedService, localStorageService, PerfilService) {
        function link($scope, elem, attrs, ctrl) {
            var user_id = localStorageService.get('userID');
            $scope.read = '';
            $scope.count = 0;
            $scope.openFlag = false;
            $scope.show = true;
            $scope.isRead = false;
            $scope.checkout = true;

            $scope.getRead = function(check) {
                if ($scope.checkout === check) {
                    if ($scope.isRead === true && angular.equals($scope.read, 'Ver')) {
                        $scope.read = 'Pontuar';
                        return $scope.read;
                    }
                }

                if ($scope.checkout != check) {
                    if ($scope.isRead === false && check === false) {
                        $scope.isRead = true;
                        $scope.read = 'Ver';
                        return $scope.read;
                    }
                }

                return $scope.read;
            }

            $scope.setRead = function() {
                if ($scope.count <= 2) {
                    $scope.count++;
                    $scope.isRead = true;
                }
                if ($scope.count == 2) {
                    $scope.read = '';
                    FeedService.post($scope.post_id, true, user_id)
                        .success(function(data) {
                            PerfilService.atualizaPerfil();
                        })
                        .error(function(err) {
                            window.location.href = '';
                        });
                } else {
                    $scope.read = '';
                }
                if ($scope.count == 1) {
                    $scope.read = 'Pontuar';
                }
                $scope.openFlag = !$scope.openFlag;
                $scope.getRead($scope.isRead);
            };

            $scope.isShow = function() {
                return !$scope.show;
            }

            $scope.setShow = function() {
                return $scope.show = !$scope.show;
            }
        }
        return {
            restrict: 'E',
            templateUrl: 'directives/feedConteudo.html',
            scope: true,
            link: link
        }
    }
]);
