'use strict';

angular.module('myApp.esqueciSenhaService', [])

// each function returns a promise object 
.factory('EsqueciSenhaService', ['$http', function($http) {
    var EsqueciSenhaService = {
        reset: reset
    };
    
    function reset(email) {
        return $http.post('/api/reset-password', {"email": email})
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                return err;
            });
    }

    return EsqueciSenhaService;

}]);