'use strict';

angular.module('myApp.commentsService', [])

// each function returns a promise object 
.factory('CommentsService', ['$http', 'localStorageService', function($http, localStorageService) {
    var CommentsService = {
        post: post,
        get: get,
    };

    return CommentsService;


    function post(post, empresa, userId, comentario, date, nome) {
        var programaID = localStorageService.get('programaID');

        return $http.post('/api/addComment', { id_post : post, id_usuario : userId, id_empresa : empresa, 
            comment: comentario, date: date, name: nome, programa_id: programaID})
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                return err;
            });
    }

    function get(empresa){        
        return $http.get('/api/comments', {empresa: empresa})
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                return err;
            });
    }

}]);
