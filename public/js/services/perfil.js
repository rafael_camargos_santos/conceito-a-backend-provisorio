'use strict';

angular.module('myApp.perfilService', [])

    .factory('PerfilService', ['$rootScope', '$http', 'localStorageService', 
        function($rootScope, $http, localStorageService) {

        var perfilService = {};
        var userID = localStorageService.get('userID');

        perfilService.post = function post(user){
            return $http.post('/api/user', user)
                .success(function(data){
                    return data;
                })
                .error(function(err){
                    return err;
                });
        }

        perfilService.selectedUser;
        perfilService.isMyPerfil;

        perfilService.getPerfil = function get() {
            if (perfilService.selectedUser == null) {
                perfilService.selectedUser = userID;
                perfilService.isMyPerfil = true;
            } else {
                if (perfilService.selectedUser == userID) {
                    perfilService.isMyPerfil = true;
                } else {
                    perfilService.isMyPerfil = false;
                }
            }
            return $http.get('/api/user/id=' + perfilService.selectedUser);
        };

        perfilService.chooseUser = function(chooseUserID) {
            perfilService.selectedUser = chooseUserID;

        }

        perfilService.atualizaPerfil = function(){
            var userID = localStorageService.get('userID');
            this.getPerfil(userID)
                .success(function(data) {
                    var perfil = data.response.perfil;

                    localStorageService.set('userID', perfil._id);
                    localStorageService.set('programaID', perfil.programa_id);
                    localStorageService.set('departamento', perfil.departamento);
                    localStorageService.set('empresa', perfil.empresa);
                    localStorageService.set('foto', perfil.foto);
                    localStorageService.set('email', perfil.email);
                    localStorageService.set('nome', perfil.nome);
                    localStorageService.set('score', perfil.score);
                    perfilService.notify();
                })
        }

        perfilService.subscribe = function(scope, callback) {
            var handler = $rootScope.$on('alteracao-perfil', callback);
            scope.$on('$destroy', handler);
        },

        perfilService.notify =  function() {
            $rootScope.$emit('alteracao-perfil');
        }
  
        return perfilService;
        
    }]);
