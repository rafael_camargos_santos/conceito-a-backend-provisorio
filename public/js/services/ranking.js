'use strict';

angular.module('myApp.rankingService', [])

// each function returns a promise object 
.factory('RankingService', ['$http', 'localStorageService', function($http, localStorageService) {
    var RankingService = {
        getRankGeral: getRankGeral,
        getRankDepartamento: getRankDepartamento,
        getRankUsuario: getRankUsuario,
        postRankUsuarios: postRankUsuarios
    };

    return RankingService;


    function getRankGeral() {
        var empresa = localStorageService.get('empresa');
        var userID = localStorageService.get('userID');
        return $http.get('/api/ranking-geral/empresa=' + empresa + '/userID=' + userID)
            .success(function(data) {
                if (data.success === false) {
                    return data.message;
                };
                return data.response;
            })
            .error(function(err) {
                return err;
            });
    }

    function getRankDepartamento(departamento) {
        var empresa = localStorageService.get('empresa');
        var userID = localStorageService.get('userID');
        return $http.get('/api/ranking-departamento/empresa=' + empresa + '/departamento=' + departamento + '/userID=' + userID)
            .success(function(data) {
                if (data.success === false) {
                    return data.message;
                };
                return data.response;
            })
            .error(function(err) {
                return err;
            });
    }

    function getRankUsuario(userId) {
        return $http.get('/api/ranking-usuario?id=' + userId)
            .success(function(data) {
                if (data.success === false) {
                    return data.message;
                };
                return data.response;
            })
            .error(function(err) {
                return err;
            });
    }

    function postRankUsuarios(departamento) {
        return $http.post('/api/ranking-usuarios/departamento=' + departamento)
            .success(function(data) {
                if (data.success === false) {
                    return data.message;
                };
                return data.response;
            })
            .error(function(err) {
                return err;
            });
    }

}]);
