'use strict';

angular.module('myApp.feedService', [])

// each function returns a promise object 
.factory('FeedService', ['$rootScope', '$http', 'localStorageService', function($rootScope, $http, localStorageService) {

    var programaID = localStorageService.get('programaID');
    var FeedService = {};

    FeedService.postSemana = 0;

    FeedService.getPostSemana = function() {
        var tempPostSemana = this.postSemana;
        this.postSemana = 0;
        return tempPostSemana;
    }

    FeedService.setPostSemana = function(semana) {
        this.postSemana = semana;
    }


    FeedService.get = function get(semana, emp_id) {
        var url = '/api/feed';
        url = url + '?programa_id=' + programaID;
        if (emp_id != null) {
            url = url + '&emp_id=' + emp_id;
            if (semana != null) {
                url = url + '&semana=' + semana;
            }
        }

        return $http.get(url)
            .success(function(data) {
                if (data.success === false) {
                    return data.message;
                };
                return data.response;
            })
            .error(function(err) {
                return err;
            });
    }

    FeedService.post = function post(_id, answer, user_id) {
        return $http.post('/api/conteudo', { conteudo_id: _id, answer: answer, user_id: user_id })
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                return err;
            });
    }

    FeedService.deletar = function deletar() {
        return $http.delete('/api/feed/id=' + id)
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                return err;
            });
    }

    FeedService.subscribe = function(scope, callback) {
        var handler = $rootScope.$on('get-feed', callback);
        scope.$on('$destroy', handler);
    },

    FeedService.notify =  function() {
        $rootScope.$emit('get-feed');
    }

    return FeedService;

}]);
