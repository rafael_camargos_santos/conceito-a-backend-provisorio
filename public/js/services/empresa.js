'use strict';

angular.module('myApp.empresaService', [])

.factory('EmpresaService', ['$http', 'localStorageService', function($http, localStorageService) {

    var empresaService = {};
    var programaID = localStorageService.get('programaID');

    empresaService.selectedEmpresa;
    empresaService.selectedUsuario;

    empresaService.getEmpresas = function get() {
        var url = '/api/empresa' + '?programa_id=' + programaID;
        return $http.get(url);
    };

    empresaService.chooseEmpresa = function(empresa) {
        empresaService.selectedEmpresa = empresa;
        localStorageService.set('empresaID', empresa._id);
    }

    empresaService.chooseEmpresaEditar = function(empresa) {
        empresaService.selectedEmpresa = empresa;
        localStorageService.set('empresaID', empresa._id);
    }

    empresaService.getEmpresaById = function(id) {
        return $http.get('/api/empresa-usuario?id=' + id);
    }

    empresaService.getUsersEmpresa = function() {
        var empresaID = localStorageService.get('empresaID');

        return $http.get('/api/user?id=' + empresaID);
    }

    empresaService.getUser = function(usuario) {
        empresaService.selectedUsuario = usuario;
    }

    empresaService.cadastra = function(nomeNew, dataNew, departamentoNew, unidadeNew) {
        return $http.post('/api/empresa', { nome: nomeNew, data: dataNew, departamento: departamentoNew, unidade: unidadeNew, programa_id: programaID })
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                return err;
            });
    }

    empresaService.atualiza = function(empresaId, nomeNew, dataNew, departamentoNew, unidadeNew) {
        return $http.post('/api/empresa', { _id: empresaId, nome: nomeNew, data: dataNew, departamento: departamentoNew, unidade: unidadeNew })
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                return err;
            });
    }

    empresaService.getUsersEmpresa = function() {
        var empresaID = localStorageService.get('empresaID');
        return $http.get('/api/user?id=' + empresaID);
    }

    empresaService.createUsers = function(users) {
        if (users.length > 0) {
            return $http.post('/api/create-accounts', { "usuarios": users });
        }
    }

    empresaService.updateUser = function(user) {
        return $http.post('/api/user', user);
    }

    empresaService.deleteUser = function deleteUser(user) {
        console.log(user.nome + " + " + user._id);
        return $http.delete('/api/delete-accounts?user_id=' + user._id)
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                return err;
            });
    }

    empresaService.deleteEmpresa = function deleteEmpresa(empresa) {
        return $http.delete('/api/delete-empresa?empresa=' + empresa._id)
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                console.log(err);
                return err;
            });
    }

    empresaService.desativarEmpresa = function(empresa) {
        return $http.get('/api/desativar-empresa?empresa=' + empresa._id)
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                console.log(err);
                return err;
            });
    }

    empresaService.ativarEmpresa = function(empresa) {
        return $http.get('/api/ativar-empresa?empresa=' + empresa._id)
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                console.log(err);
                return err;
            });
    }

    empresaService.relatorioUsuarios = function(empresaId) {
        return $http.get('/api/relatorio-usuarios?empresa=' + empresaId)
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                console.log(err);
                return err;
            });
    }

    empresaService.relatorioQuizzes = function(empresaId, programaId) {
        return $http.post('/api/relatorio-quizzes', {programa_id: programaId, empresa: empresaId})
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                console.log(err);
                return err;
            });
    }

    empresaService.relatorioDesafios = function(empresaId, programaId) {
        return $http.post('/api/relatorio-desafios', {programa_id: programaId, empresa: empresaId})
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                console.log(err);
                return err;
            });
    }

        empresaService.relatorioComplementos = function(empresaId, programaId) {
        return $http.post('/api/relatorio-complementos', {programa_id: programaId, empresa: empresaId})
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                console.log(err);
                return err;
            });
    }

        empresaService.relatorioConteudos = function(empresaId, programaId) {
        return $http.post('/api/relatorio-conteudos', {programa_id: programaId, empresa: empresaId})
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                console.log(err);
                return err;
            });
    }
    return empresaService;

}]);
