'use strict';

angular.module('myApp.ferramentasService', [])

// each function returns a promise object 
.factory('FerramentasService', ['$http', function($http) {
    var FerramentasService = {
        post: post,
    };
    
    function post(password, newPassword, email, programaID) {
        return $http.post('/api/change-password', {password: password, newPassword: newPassword, 
            email: email, programa_id: programaID})
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                return err;
            });
    }

    return FerramentasService;

}]);