'use strict';

angular.module('myApp.admConteudoService', [])

// each function returns a promise object 
.factory('AdmConteudoService', ['$http', function($http) {
    var AdmConteudoService = {
        conteudo: {},
        postQuiz: postQuiz,
        postContent: postContent,
        deleteContent: deleteContent
    };

    function postQuiz(quiz) {
        return $http.post('/api/quiz-adm', quiz)
            .success(function(data) {
                return data;
                console.dir(data);
            })
            .error(function(err) {
                return err;
            });
    }

    function postContent(content) {
        return $http.post('/api/conteudo-adm', content)
            .success(function(data) {
                return data;
                console.dir(data);
            })
            .error(function(err) {
                return err;
            });
    }

    function deleteContent(content) {
        return $http.delete('/api/conteudo-adm?id=' + content._id)
            .success(function(data) {
                return data;
                console.dir('DELETADO');
            })
            .error(function(err) {
                return err;
            });
    }

    /*function post(password, newPassword, email, userId) {
        return $http.post('/api/change-password', {password: password, newPassword: newPassword, email: email})
            .success(function(data) {
                return data;
                console.dir(data);
            })
            .error(function(err) {
                return err;
            });
    }*/

    return AdmConteudoService;

}]);
