'use strict';

angular.module('myApp.s3Service', [])

// each function returns a promise object 
.factory('S3Service', ['$http', function($http) {
    var S3Service = {};
    
    S3Service.s3URL = 'http://ec2-54-210-71-210.compute-1.amazonaws.com:8080/api/s3Write';

    S3Service.getS3URL = function(){
        return this.s3URL;
    }

    return S3Service;

}]);