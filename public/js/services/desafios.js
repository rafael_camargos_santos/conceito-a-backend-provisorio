'use strict';

angular.module('myApp.desafiosService', [])

    // each function returns a promise object 
    .factory('DesafiosService', ['$http', function($http) {
        var desafiosService = {};

        desafiosService.desafio;

        desafiosService.setDesafio = function(desafio){
            this.desafio = desafio;
        }

        desafiosService.getDesafio = function(){
            return this.desafio;
        }
        
        desafiosService.userSelected;

        desafiosService.userSelected = function (user){//Usuário do desafio
            this.userSelected = user;
        }

        desafiosService.get = function get(){
            return $http.get('/api/desafios')
                .success(function(data){
                    return data.response.desafios;
                })
                .error(function(err){
                    return err;
                });
        }

        desafiosService.post = function post(desafio){
            return $http.post('/api/desafios', desafio)
                .success(function(data){
                    return data;
                })
                .error(function(err){
                    return err;
                });
        }

        desafiosService.editarDesafio = function editarDesafio(desafio){
            return $http.post('/api/edit-desafios', desafio)
                .success(function(data){
                    return data;
                })
                .error(function(err){
                    return err;
                });
        }

        desafiosService.postLike = function post(desafio_id,curtidas){
            return $http.post('/api/desafios', {desafio_id: desafio_id, curtidas : curtidas})
                .success(function(data){
                    return data;
                })
                .error(function(err){
                    return err;
                });
        }

        desafiosService.postComentario = function post(desafio_id,comentario, nome){
            return $http.post('/api/desafios-comentario', {post_id: desafio_id, comentario : comentario, nome: nome})
                .success(function(data){
                    return data;
                })
                .error(function(err){
                    return err;
                });
        }

        desafiosService.deletar = function deletar(){
            return $http.delete('/api/desafios/id=' + id)
                .success(function(data){
                    return data;
                })
                .error(function(err){
                    return err;
                });
        }

        desafiosService.getUser = function getUser(userID){
            return $http.get('/api/desafios?user_id='+userID)
                .success(function(data){
                    return data.response.desafios;
                })
                .error(function(err){
                    return err;
                });
        }

        desafiosService.getEmpresa = function getEmpresa(empresaID){
            return $http.get('/api/desafios?empresa_id='+empresaID)
                .success(function(data){
                    return data.response.desafios;
                })
                .error(function(err){
                    return err;
                });
        }
        return desafiosService;

    }]);
