'use strict';

angular.module('myApp.quizzesService', [])

.factory('QuizzesService', ['$http', function($http) {

    var quizService = {};

    quizService.quizId;
    quizService.respondido;

    quizService.getQuizId = function() {
        return this.quizId;
    }

    quizService.setQuizId = function(id){
    	this.quizId = id;
    }

    quizService.getRespondido = function() {
        return this.respondido;
    }

    quizService.setRespondido = function(resp) {
        this.respondido = resp;
    }
    
    quizService.get = function get(id){ 
    	return $http.get('/api/quiz?id=' + id)
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                return err;
            });
    };

    quizService.post = function post(userId, quiz_id, quiz, diaCerto){
        return $http.post('/api/quiz', {user_id: userId, answer: quiz.answer, quiz_id: quiz_id, resp_id: quiz.resposta, dia_certo: diaCerto})
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                return err;
            });
    }
    
    return quizService;
        
    }]);

