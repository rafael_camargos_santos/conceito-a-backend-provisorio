'.jsuse strict';

angular.module('myApp.chatService', [])

// each function returns a promise object 
.factory('ChatService', ['$http', function($http) {
    var ChatService = {
        get: get,
        getNaoVisualizadas: getNaoVisualizadas,
        post: post,
        getAll: getAll,
        selectedChat : {}
    };

    return ChatService;


    function get(userId) {
        return $http.get('/api/chat/user=' + userId)
            .success(function(data) {
                if (data.success === false) {
                    return data.message;
                };
                return data.response;
            })
            .error(function(err) {
                return err;
            });
    }

    function getNaoVisualizadas(userId) {
        return $http.get('/api/chat/nao-visualizadas/user=' + userId)
            .success(function(data) {
                if (data.success === false) {
                    return data.message;
                };
                return data.response.nao_visualizadas;
            })
            .error(function(err) {
                return err;
            });
    }

    function post(mensagem, is_user, userId) {
        return $http.post('/api/chat', {mensagem: mensagem, is_user: is_user, user_id: userId})
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                return err;
            });
    }

    function getAll(programa_id) {
        return $http.get('/api/allChat?programa_id=' + programa_id);
            /*.success(function(data) {
                if (data.success === false) {
                    return data.message;
                };
                return data.response;
            })
            .error(function(err) {
                return err;
            });*/
    }

}]);