'use strict';

angular.module('myApp.likesService', [])

// each function returns a promise object 
.factory('LikesService', ['$http', 'localStorageService', function($http, localStorageService) {
    var LikesService = {
        post: post,
        get: get,
    };

    return LikesService;


    function post(post, empresa, userId) {
        var programaID = localStorageService.get('programaID');

        return $http.post('/api/likes', {id_post: post, id_empresa: empresa, id_usuario: userId, programa_id: programaID})
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                return err;
            });
    }

    function get(empresa){
        return $http.get('/api/likes', {empresa: empresa})
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                return err;
            });
    }

}]);
