//Application module (myApp) dentro dele voce instacia as dependencia
angular.module('myApp', [
    'ngRoute',
    'satellizer',
    'LocalStorageModule',
    'angular.aws.s3',
    'angular-md5',
    'youtube-embed',
    'luegg.directives',
    'myApp.controllers',
    'myApp.filters',
    'myApp.services',
    'myApp.directives',
    'myApp.controller-desafios',
    'myApp.controller-quiz',
    'myApp.controller-mapa',
    'myApp.controller-feed',
    'myApp.controller-ferramentas',
    'myApp.controller-cadastro',
    'myApp.controller-login',
    'myApp.controller-editar-perfil',
    'myApp.controller-menu',
    'myApp.controller-cadastro-empresa',
    'myApp.controller-administrador',
    'myApp.controller-alterarcomplemento',
    'myApp.controller-alterarconteudo',
    'myApp.controller-alterardesafio',
    'myApp.controller-alterarquiz',
    'myApp.controller-desafios-a-fazer',
    'myApp.controller-adm-usuarios',
    'myApp.controller-adicionar-usuarios',
    'myApp.controller-programa',
    'myApp.controller-listachats',
    'myApp.controller-editar-usuario',
    'myApp.controller-esqueci-senha',
    'myApp.controller-relatorio-programa',
    'myApp.controller-relatorio-usuarios',
    'myApp.controller-empresa',
    'myApp.controller-regras-do-jogo',
    'myApp.desafiosService',
    'myApp.perfilService',
    'myApp.quizzesService',
    'myApp.feedService',
    'myApp.rankingService',
    'myApp.chatService',
    'myApp.empresaService',
    'myApp.ferramentasService',
    'myApp.likesService',
    'myApp.commentsService',
    'myApp.admConteudoService',
    'myApp.esqueciSenhaService',
    'myApp.s3Service'
]).
config(['$routeProvider', '$locationProvider', '$authProvider', function($routeProvider, $locationProvider, $authProvider) {
    // Satellizer configuration that specifies which API
    // route the JWT should be retrieved from
    $authProvider.loginUrl = '/api/login'; //end point do login

    $routeProvider.
    when('/index', {
        templateUrl: 'partials/mapa.html',
        controller: 'IndexCtrl'
    }).
    when('/', {
        templateUrl: 'index.html',
        controller: 'IndexCtrl'
    }).
    when('/perfil', {
        templateUrl: 'partials/perfil.html',
        controller: 'IndexCtrl'
    }).
    when('/editar-perfil', {
        templateUrl: 'partials/editar-perfil.html',
        controller: 'EditarPerfilCtrl'
    }).
    when('/mapa', {
        templateUrl: 'partials/mapa.html',
        controller: 'MapaController'
    }).
    when('/feed', {
        templateUrl: 'partials/feed.html',
        controller: 'AddPostCtrl'
    }).
    when('/chat', {
        templateUrl: 'partials/chat.html',
        controller: 'ReadPostCtrl'
    }).
    when('/desafios', {
        templateUrl: 'partials/desafios.html',
        controller: 'DesafioController',
        service: 'DesafiosService'
    }).
    when('/quiz', {
        templateUrl: 'partials/quiz.html',
        controller: 'DeletePostCtrl'
    }).
    when('/ranking', {
        templateUrl: 'partials/ranking.html',
        controller: 'DeletePostCtrl'
    }).
    when('/ferramentas', {
        templateUrl: 'partials/ferramentas.html',
        controller: 'DeletePostCtrl'
    }).
    when('/administrador', {
        templateUrl: 'partials/administrador.html',
        controller: 'DeletePostCtrl'
    }).
    when('/cadastro-empresa', {
        templateUrl: 'partials/cadastroEmpresa.html',
        controller: 'DeletePostCtrl'
    }).
    when('/painel-empresas', {
        templateUrl: 'partials/painelEmpresas.html',
        controller: 'DeletePostCtrl'
    }).
    when('/usuarios', {
        templateUrl: 'partials/usuarios.html',
        controller: 'AdmUsuariosController'
    }).
    when('/desafios-a-fazer', {
        templateUrl: 'partials/desafios-a-fazer.html',
        controller: 'DesafiosAFazerController as DaFCtrl'
    }).
    when('/programa', {
        templateUrl: 'partials/programa.html',
        controller: 'DeletePostCtrl'
    }).
    when('/alterar-quiz', {
        templateUrl: 'partials/alterar-quiz.html',
        controller: 'DeletePostCtrl'
    }).
    when('/alterar-desafio', {
        templateUrl: 'partials/alterar-desafio.html',
        controller: 'DeletePostCtrl'
    }).
    when('/alterar-conteudo', {
        templateUrl: 'partials/alterar-conteudo.html',
        controller: 'DeletePostCtrl'
    }).
    when('/alterar-complemento', {
        templateUrl: 'partials/alterar-complemento.html',
        controller: 'DeletePostCtrl'
    }).
    when('/adicionar-usuarios', {
        templateUrl: 'partials/adicionarUsuarios.html',
        controller: 'DeletePostCtrl'
    }).
    when('/lista-chats', {
        templateUrl: 'partials/lista-chats.html',
        controller: 'DeletePostCtrl'
    }).
    when('/editar-usuario', {
        templateUrl: 'partials/editar-usuario.html',
        controller: 'DeletePostCtrl'
    }).
    when('/esqueci-senha', {
        templateUrl: 'esqueci-senha.html',
        controller: 'DeletePostCtrl'
    }).
    when('/relatorio-programa', {
        templateUrl: 'partials/relatorio-programa.html',
        controller: 'DeletePostCtrl'
    }).
    when('/relatorio-usuarios', {
        templateUrl: 'partials/relatorio-usuarios.html',
        controller: 'DeletePostCtrl'
    }).
    when('/empresa', {
        templateUrl: 'partials/empresa.html',
        controller: 'DeletePostCtrl'
    }).
    when('/regras-do-jogo', {
        templateUrl: 'partials/regras-do-jogo.html',
        controller: 'DeletePostCtrl'
    }).
    otherwise({
        redirectTo: '/'
    });
    $locationProvider.html5Mode(true);
}]);
