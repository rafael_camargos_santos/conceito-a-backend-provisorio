'use strict';

/* Controllers */

angular.module('myApp.controllers', []).
controller('IndexCtrl', function IndexCtrl($scope) {}).
controller('AddPostCtrl', function AddPostCtrl($scope) {}).
controller('ReadPostCtrl', function ReadPostCtrl($scope) {}).
controller('EditPostCtrl', function EditPostCtrl($scope) {}).
controller('DeletePostCtrl', function DeletePostCtrl($scope) {}).
controller('PostTestController', function() {
    this.comentarios = post_comentarios;
    this.read = 'Ver';
    this.count = 0;
    this.openFlag = false;

    // myStyle={'max-height':'100%', 'display':'block'}

    this.setShow = function(context, show) {
        context.comentarioIsShow = show;
    };

    this.setRead = function(context) {
        this.count++;
        if (this.count == 1) {
            this.read = 'Pontuar';
        } else {
            this.read = '';
        }
        this.openFlag = !this.openFlag;
    };

    this.isShow = function(context) {
        return (context.comentarioIsShow === true);
    };
    this.setLike = function(post) {
        if (post.comentarios.isLiked === true) {
            post.comentarios.like = post.comentarios.like - 1;
            post.comentarios.isLiked = false;
        } else {
            post.comentarios.like = post.comentarios.like + 1;
            post.comentarios.isLiked = true;
        }
    };
}).
controller('PostTestController2', function() {
    this.comentarios = post_comentarios2;

    this.setShow = function(context, show) {
        context.comentarioIsShow = show;
    };

    this.isShow = function(context) {
        return (context.comentarioIsShow === true);
    };
    this.setLike = function(post) {
        if (post.comentarios.isLiked === true) {
            post.comentarios.like = post.comentarios.like - 1;
            post.comentarios.isLiked = false;
        } else {
            post.comentarios.like = post.comentarios.like + 1;
            post.comentarios.isLiked = true;
        }
    };
}).
controller('PostTestController3', function() {
    this.comentarios = post_comentarios3;

    this.setShow = function(context, show) {
        context.comentarioIsShow = show;
    };

    this.isShow = function(context) {
        return (context.comentarioIsShow === true);
    };
    this.setLike = function(post) {
        if (post.comentarios.isLiked === true) {
            post.comentarios.like = post.comentarios.like - 1;
            post.comentarios.isLiked = false;
        } else {
            post.comentarios.like = post.comentarios.like + 1;
            post.comentarios.isLiked = true;
        }
    };
});

var post_comentarios = {
    like: 5,
    isLiked: false,
    comentario: [
        { nome: "Camila", comentario: "Fiquei com água na boca de pensar na comida da minha tia Lúcia. Hummm..." },
        { nome: "Ricardo", comentario: "Até gosto de cozinhar, o duro é ter tempo para fazer as refeições com a família." }
    ],
    comentarioIsShow: false
};

var post_comentarios2 = {
    like: 16,
    isLiked: true,
    comentario: [
        { nome: "Victor", comentario: "Ô João, me convida aí para esse açaí com peixe!!" },
        { nome: "João", comentario: "Quem nunca comeu açaí com peixe frito não sabe o que está perdendo." },
        { nome: "Rafael", comentario: "Melhor comida da vida: Galinhada com pequi!!" }
    ],
    comentarioIsShow: false
};

var post_comentarios3 = {
    like: 10,
    isLiked: true,
    comentario: [
        { nome: "Luciano", comentario: "Nossa! Nunca mais compro “suco de caixinha” para as meninas!" }
    ],
    comentarioIsShow: false
};
