module.exports = function (moduleAccount){
  
	var controllers = moduleAccount.controllers;

	return function(router){
	    router.post("/login", function(req, res){
	    	controllers.login.post(req, res);
	    });
	}
}