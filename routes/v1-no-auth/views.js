module.exports = function (){
  
	return function(router){
	    router.get("/index", function(req, res){
    		res.render('index.html');
	    });
	    router.get("/", function(req, res){
    		res.render('login.html');
	    });
	    router.get("/cadastro", function(req, res){
    		res.render('cadastro.html');
	    });
	    router.get("/esqueci-senha", function(req, res){
    		res.render('esqueci-senha.html');
	    });
	    router.get("/partials/:name", function(req, res){
    		var name = req.params.name;
  			res.render('partials/' + name);
	    });
	    router.get("/directives/:name", function(req, res){
    		var name = req.params.name;
  			res.render('directives/' + name);
	    });
	    router.get("*", function(req, res){
    		res.render('index.html');
	    });
	}
}