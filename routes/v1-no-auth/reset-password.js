module.exports = function (moduleAccount){
  
	var controllers = moduleAccount.controllers;

	return function(router){
	   	router.post("/reset-password", function(req, res){
	    	controllers.resetPassword.reset(req, res);
	    });
	}
}