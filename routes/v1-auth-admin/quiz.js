module.exports = function (moduleQuiz){
  
	var controllers = moduleQuiz.controllers;

	return function(router){
	    router.post("/quiz-adm", function(req, res){
	    	controllers.quizAdm.post(req, res);
	    });
	   	router.delete("/quiz-adm", function(req, res){
	    	controllers.quizAdm.delete(req, res);
	    });
	}
}