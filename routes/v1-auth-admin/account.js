module.exports = function (moduleAccount){
  
	var controllers = moduleAccount.controllers;

	return function(router){
	    router.post("/create-accounts", function(req, res){
	    	controllers.createAccounts.post(req, res);
	    });
	    router.delete("/delete-accounts", function(req, res){
	    	controllers.deleteAccounts.delete(req, res);
	    });
	}
}