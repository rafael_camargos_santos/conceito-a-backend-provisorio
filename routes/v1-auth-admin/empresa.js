module.exports = function (moduleEmpresa){
  
	var controllers = moduleEmpresa.controllers;

	return function(router){
	    router.post("/empresa", function(req, res){
	    	controllers.empresa.post(req, res);
	    });
	    router.get("/empresa", function(req, res){
	    	controllers.empresa.get(req, res);
	    });
	   	router.delete("/delete-empresa", function(req, res){
	    	controllers.empresa.delete(req, res);
	    });
	    router.get("/desativar-empresa", function(req, res){
	    	controllers.empresa.desativar(req, res);
	    });
	    router.get("/ativar-empresa", function(req, res){
	    	controllers.empresa.ativar(req, res);
	    });
	}
}