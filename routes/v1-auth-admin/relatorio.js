module.exports = function (moduleRelatorio){
  
	var controllers = moduleRelatorio.controllers;

	return function(router){
	    router.get("/relatorio-usuarios", function(req, res){
	    	controllers.relatorio.getUsuarios(req, res);
	    });
	    router.post("/relatorio-quizzes", function(req, res){
	    	controllers.relatorio.getQuizzes(req, res);
	    });
	    router.post("/relatorio-desafios", function(req, res){
	    	controllers.relatorio.getDesafios(req, res);
	    });
	    router.post("/relatorio-complementos", function(req, res){
	    	controllers.relatorio.getComplementos(req, res);
	    });
	    router.post("/relatorio-conteudos", function(req, res){
	    	controllers.relatorio.getConteudos(req, res);
	    });
	}
}