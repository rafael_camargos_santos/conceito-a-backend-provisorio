module.exports = function (moduleChatEmpresa){
  
	var controllers = moduleChatEmpresa.controllers;

	return function(router){
	    router.get("/allChat", function(req, res){
	    	controllers.chatEmpresa.get(req, res);
	    });
	}
}