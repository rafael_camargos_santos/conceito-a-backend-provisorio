module.exports = function (moduleConteudo){
  
	var controllers = moduleConteudo.controllers;

	return function(router){
	    router.post("/conteudo-adm", function(req, res){
	    	controllers.conteudoAdm.post(req, res);
	    });
	   	router.delete("/conteudo-adm", function(req, res){
	    	controllers.conteudoAdm.delete(req, res);
	    });
	}
}