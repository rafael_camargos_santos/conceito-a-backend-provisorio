module.exports = function (moduleLikes){
  
	var controllers = moduleLikes.controllers;

	return function(router){
	    router.post("/likes", function(req, res){
	    	controllers.likes.post(req, res);
	    });
	   	router.get("/likes", function(req, res){
	    	controllers.likes.get(req, res);
	    });
	}
}