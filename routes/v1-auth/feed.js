module.exports = function (moduleFeed){
  
	var controllers = moduleFeed.controllers;

	return function(router){
	    router.get("/feed", function(req, res){
	    	controllers.feed.get(req, res);
	    });
	}
}