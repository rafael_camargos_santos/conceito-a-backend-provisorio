module.exports = function (moduleChat){
  
	var controllers = moduleChat.controllers;

	return function(router){
	    router.get("/chat/user=:userID", function(req, res){
	    	controllers.chat.get(req, res);
	    });
	    router.get("/chat/nao-visualizadas/user=:userID", function(req, res){
	    	controllers.chat.getNaoVisualizadas(req, res);
	    });
	    router.post("/chat", function(req, res){
	    	controllers.chat.post(req, res);
	    });
	}
}