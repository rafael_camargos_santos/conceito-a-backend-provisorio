module.exports = function (moduleDesafios){
  
	var controllers = moduleDesafios.controllers;

	return function(router){
	    router.post("/desafios", function(req, res){
	    	controllers.desafio.post(req, res);
	    });
	    router.get("/desafios", function(req, res){
	    	controllers.desafio.get(req, res);
	    });
	   	router.delete("/desafios", function(req, res){
	    	controllers.desafio.delete(req, res);
	    });
	    router.post("/desafios-comentario", function(req, res){
	    	controllers.desafio.postComentario(req, res);
	    });
	   	router.post("/edit-desafios", function(req, res){
	    	controllers.desafio.editDesafio(req, res);
	    });
	}
}