module.exports = function (moduleConteudo){
  
	var controllers = moduleConteudo.controllers;

	return function(router){
	    router.post("/conteudo", function(req, res){
	    	controllers.conteudo.post(req, res);
	    });
	    router.get("/conteudo", function(req, res){
	    	controllers.conteudo.get(req, res);
	    });
	}
}