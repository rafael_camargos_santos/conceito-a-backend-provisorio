module.exports = function (moduleComments){//controller -> moduleComments
  
	var controllers = moduleComments.controllers;

	return function(router){
	    router.post("/addComment", function(req, res){
	    	controllers.comments.post(req, res);
	    });
	   	router.get("/comments", function(req, res){
	    	controllers.comments.get(req, res);
	    });
	}
}