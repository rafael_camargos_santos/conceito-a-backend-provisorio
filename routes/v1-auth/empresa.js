module.exports = function (moduleEmpresaUsuario){
  
	var controllers = moduleEmpresaUsuario.controllers;

	return function(router){
	    router.get("/empresa-usuario", function(req, res){
	    	controllers.empresaUser.get(req, res);
	    });
	}
}