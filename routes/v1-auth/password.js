module.exports = function (moduleAccount){
  
	var controllers = moduleAccount.controllers;

	return function(router){
	    router.post("/change-password", function(req, res){
	    	controllers.changePassword.post(req, res);
	    });
	}
}