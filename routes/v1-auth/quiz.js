module.exports = function (moduleQuiz){
  
	var controllers = moduleQuiz.controllers;

	return function(router){
	    router.post("/quiz", function(req, res){
	    	controllers.quiz.post(req, res);
	    });
	    router.get("/quiz", function(req, res){
	    	controllers.quiz.get(req, res);
	    });
	}
}