module.exports = function (moduleRank){
  
	var controllers = moduleRank.controllers;

	return function(router){
	    router.get("/ranking-geral/empresa=:empresa/userID=:userID", function(req, res){
	    	controllers.rank.getRankGeral(req, res);
	    });
	   	router.get("/ranking-departamento/empresa=:empresa/departamento=:departamento/userID=:userID", function(req, res){
	    	controllers.rank.getRankDepartamento(req, res);
	    });
	   	router.post("/ranking-usuarios/departamento=:departamento", function(req, res){
	    	controllers.rank.postRankUsuarios(req, res);
	    });
	   	router.get("/ranking-usuario", function(req, res){
	    	controllers.rank.getRankUsuario(req, res);
	    });
	}
}