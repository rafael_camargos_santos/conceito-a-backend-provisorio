module.exports = function (moduleUser){
  
  var controllers = moduleUser.controllers;

  return function(router){
	router.get("/user/id=:id", function(req, res){
	    controllers.user.getUserProfile(req, res);
    });
    router.post("/user", function(req, res){
	    controllers.user.updateUser(req, res);
    });
    router.get("/user", function(req, res){
    	controllers.user.getUserPorEmpresa(req, res);
    })
  }

}