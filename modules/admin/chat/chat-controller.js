module.exports = function (schema){

  var Chat = schema.chat;
  var ObjectID = schema.mongoose.Types.ObjectId;

  return {
    get: function (req, res){

      Chat.find({programa_id: req.query.programa_id}, function(err, chats){
          if (err) throw err;

          if(chats){
            return res.json({success: true, message: "Chats encontrados", response: {'chats': chats}});
          }
          else return res.json({success: false, message: "Nenhum chat encontrado"});
      }).sort({updated:-1})
    }
  }
}