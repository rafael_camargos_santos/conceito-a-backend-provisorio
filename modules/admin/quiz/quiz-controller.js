module.exports = function (schema){

  var Quiz = schema.quiz;
  var ObjectID = schema.mongoose.Types.ObjectId;

  return {
    post: function (req, res){
      var query = {};
      if(req.body._id) query._id = req.body._id;
      else{
        var quiz = new  Quiz(req.body);
        query._id = quiz._id;
      }
      
      var update = req.body;

      Quiz.findOneAndUpdate(query, update, {upsert: true, new: true}, function(err) {
          if (err) throw err;
          return res.json({success: true, message: "Quiz postado!"});
        });
    },
    delete: function (req, res){
      var query = {};
      if(req.query.id) query._id = req.query.id;
      Quiz.findByIdAndRemove(query._id, function(err,quiz){
        if (err) throw err;
        if (quiz){
          return res.json({success: true, message: "Quiz removido", response: {quiz: quiz}});
        }
        else return res.json({success: false, message: "Não foi possível remover o quiz"});
      })
    }
  }
}