module.exports = function (schema, bcrypt, config, crypto, user){

  var Account = schema.account;
  var User = schema.user;
  const saltRounds = config.saltRounds();
  var callCreateUser = user.controllers.user.createUser;
  var emailsNaoCadastrados = [];

  var sendEmail = function (email, password, programa, callback){
    // setup e-mail data with unicode symbols
    console.log(programa);
    if (programa == 1){
      var mailOptions = {
          from: 'contato@nossamesa.com.br',
          to: email, // list of receivers
          subject: 'Bem-vindo ao Nossa Mesa',
           // Subject line
          // text: 'Hello world ', // plaintext body
          text: 'Bem-vindo ao programa Nossa Mesa. \nAcesse o site: www.nossamesa.com.br, utilize seu e-mail como login e a senha ' + password  + ' para entrar.\nAgradecemos a sua participação!', // html body
      };
    }else{
        var mailOptions = {
          from: 'contato@simplesmentebem.com.br',
          to: email, // list of receivers
          subject: 'Bem-vindo ao Simplesmente Bem',
           // Subject line
          // text: 'Hello world ', // plaintext body
          text: 'Bem-vindo ao programa Simplesmente Bem. \nAcesse o site: www.simplesmentebem.com.br, utilize seu e-mail como login e a senha ' + password  + ' para entrar.\nAgradecemos a sua participação!', // html body
      };
    }

    var transporter = config.transporter(programa);

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            return console.log(error);
        }

        console.log('Message sent: ' + info.response);

        callback(error);
    });
  }

  var createUser = function(user, callback){
    var account = {};
    callCreateUser(user, function(userID){
      if(userID==false) {
        emailsNaoCadastrados.push(user.email);
        callback();
        return;
      }

      account.email = user.email.toLowerCase();
      account.userID = userID;
      account.programa_id = user.programa_id;
      // seta admin true caso seja das empresas de administradores ou especialistas
      if (user.empresa == '5744b4d122850afd48fa317e' || user.empresa == '5755b34bd12638d83f8b4567'
        || user.empresa == '575706ecd126380e4f8b4567' || user.empresa == '575706d7d126380c4f8b4567'){
        account.admin = true;
      }

      // cria nova senha
      var password = Math.floor(Math.random() * (9999999 - 100000 + 1)) + 100000;
      var md5 = crypto.createHash('md5').update(password.toString()).digest("hex");

      // encrypta senha
      bcrypt.hash(md5, saltRounds, function(err, hash) {
        account.password = hash;
        var accountDB = new Account(account);
        accountDB.save(function (err) {
          if (err) throw err;

          sendEmail(account.email, password, account.programa_id, function(err){
            if (err) throw err;

            callback();
            return;
          });
        });
      });
    });
  }

  var iterateUsers = function(usersArray, callback){
    var size = usersArray.length;
    var count = 0;

    function processUser(user) {

      if(count >= size) {
        callback(); // All done!
        return;
      }

      createUser(user, function(err) {
        count++;
        processUser(usersArray[count]);
      });

    }

    processUser(usersArray[count]);

  }


  return {
    post: function (req, res) {
      emailsNaoCadastrados = [];
      var usersArray = req.body.usuarios;

      iterateUsers(usersArray, function(){
        if(emailsNaoCadastrados.length>0){
          return res.json({success: true, message: 'Alguns emails já estavam cadastrados: ' + emailsNaoCadastrados});
        }
        else return res.json({success: true, message: 'Usuários cadastrados com sucesso!'});
      });
    }
  }
};