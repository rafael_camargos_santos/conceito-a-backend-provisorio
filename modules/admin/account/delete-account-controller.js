module.exports = function(schema) {

    var Account = schema.account;
    var User = schema.user;

    return {
        delete: function(req, res) {
            console.dir(req.query);
            var query = {};
            var query2 = {};
            if (req.query.user_id) {
                query2._id = req.query.user_id;
                query.userID = req.query.user_id;

                Account.findOneAndRemove(query, function(err, account) {
                    if (err) throw err;
                    if (account) {
                        if (req.query.user_id) query2._id = req.query.user_id;
                        User.findOneAndRemove(query2, function(err, dbUser) {
                            if (err) throw err;
                            if (dbUser) {
                                return res.json({ success: true, message: "Conta e usuário removidos", response: { account: account, usuario: dbUser } });
                            } else return res.json({ success: false, message: "Conta removida, usuário não removido", response: { account: account } });
                        })
                    } else return res.json({ success: false, message: "Não foi possível remover a conta e o usuário" });
                })
            }
        }
    }
}
