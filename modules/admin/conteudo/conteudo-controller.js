module.exports = function (schema){

  var Conteudo = schema.conteudo;
  var ObjectID = schema.mongoose.Types.ObjectId;

  return {
    post: function (req, res){
      var query = {};
      if(req.body._id) query._id = req.body._id;
      else{
        var conteudo = new Conteudo(req.body);
        query._id = conteudo._id;
      }
      
      var update = req.body;
      Conteudo.findOneAndUpdate(query, update, {upsert: true, new: true}, function(err) {
          if (err) throw err;
          return res.json({success: true, message: "Conteudo postado!"});
        });
    },
    delete: function (req, res){
      var query = {};
      if(req.query.id) query._id = req.query.id;
      Conteudo.findByIdAndRemove(query._id, function(err,conteudo){
        if (err) throw err;
        if (conteudo){
          return res.json({success: true, message: "Conteudo removido", response: {conteudo: conteudo}});
        }
        else return res.json({success: false, message: "Não foi possível remover o conteudo"});
      })
    }
  }
}