module.exports = function (schema){

  var Empresa = schema.empresa;
  var User = schema.user;
  var ObjectID = schema.mongoose.Types.ObjectId;

  return {
    post: function (req, res){
      var query = {};
      var query_user = {};
      if(req.body._id) {
        query._id = req.body._id;
      }else{
        var empresa = new Empresa(req.body);
        query._id = empresa._id;
      }
      query_user.empresa = query._id;
      var update = req.body;
      update.ativo = true;
      User.update(query_user, {empresa_nome: req.body.nome}, function(err){
        if (err) throw err;
        Empresa.findOneAndUpdate(query, update, {upsert: true, new: true}, function(err) {
          if (err) throw err;
          return res.json({success: true, message: "Empresa postada!"});
        });
      })
    },
    get: function (req, res){
      var query = {};

      if (req.query.id) query._id = req.query.id;//Isso pega o ?id=ksdjbvisgviusdbfjsaidfgjhsqkdfjhsbd
      if (req.query.programa_id) query.programa_id = req.query.programa_id;//Isso pega o ?id=ksdjbvisgviusdbfjsaidfgjhsqkdfjhsbd

      Empresa.find(query, function(err, empresa){
          if (err) throw err;

          if(empresa){
            return res.json({success: true, message: "Empresa encontrada", response: {empresa: empresa}});
          }
          else return res.json({success: false, message: "Nenhuma empresa encontrada para este usuário"});
      })
    },
    delete: function(req, res){
      var query = {};

      if (req.query.empresa){
        query._id = req.query.empresa;
        Empresa.findOneAndRemove(query, function(err, empresa){
          if (err) throw err;
          if (empresa){
            return res.json({success: true, message: "Empresa removida", response: {empresa: empresa}});
          }else return res.json({success: false, message: "Empresa não encontrada"});
        })
      }else return res.json({success: false, message: "Faltou o id da empresa"});
    },
    desativar: function(req, res){
      var query = {};

      if (req.query.empresa){
        query._id = req.query.empresa;
        var update = {ativo: false};
        Empresa.findOneAndUpdate(query, update, function(err, empresa){
          if (err) throw err;

          if (empresa){

            var userQuery = {};
            userQuery.empresa = req.query.empresa;

            User.update(userQuery, update, {multi: true}, function(err){
              if (err) throw err;

              return res.json({success: true, message: "Empresa desativada"});
            });
          }
          else return res.json({success: false, message: "Empresa não encontrada"});
        })
      }else return res.json({success: false, message: "Faltou o id da empresa"});
    },
    ativar: function(req, res){
      var query = {};

      if (req.query.empresa){
        query._id = req.query.empresa;
        var update = {ativo: true};
        Empresa.findOneAndUpdate(query, update, function(err, empresa){
          if (err) throw err;

          if (empresa){

            var userQuery = {};
            userQuery.empresa = req.query.empresa;

            User.update(userQuery, update, {multi: true}, function(err){
              if (err) throw err;

              return res.json({success: true, message: "Empresa desativada"});
            });
          }
          else return res.json({success: false, message: "Empresa não encontrada"});
        })
      }else return res.json({success: false, message: "Faltou o id da empresa"});
    }
  }
}