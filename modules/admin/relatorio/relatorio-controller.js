module.exports = function (schema){

  var User = schema.user;
  var Chat = schema.chat;
  var Quiz = schema.quiz;
  var Desafio = schema.desafio;
  var Conteudo = schema.conteudo;
  var Likes = schema.likes;
  var Comments = schema.comments;

  var ObjectID = schema.mongoose.Types.ObjectId;
  var users = [];

  var getUser = function(userDb, callback){
    var user = {};
    // pega informacoes basicas
    user.nome = userDb.nome;
    user.departamento = userDb.departamento;
    user.unidade = userDb.unidade;
    user.score = userDb.score;

    // calcula numero de cada tipo de conteudo
    user.conteudos = [];
    user.quizzzes = [];
    user.desafios = [];
    for (i=0 ; i<userDb.respondidos.length ; i++){
      if(userDb.respondidos[i].id == 0){
        user.desafios.push(userDb.respondidos[i].conteudo);
      }
      else if(userDb.respondidos[i].id == -1){
        user.conteudos.push(userDb.respondidos[i].conteudo);
      }
      else{
        user.quizzzes.push(userDb.respondidos[i].conteudo);
      }
    }

    // pega mensagens trocadas com o especialista
    var query = {};
    query.user_id = userDb._id;
    Chat.findOne(query, function(err, chatDb){
      if (err) throw err;

      if(chatDb){
        user.numeroChats = chatDb.mensagens.length;
      }
      else user.numeroChats = 0;

      users.push(user);
      callback();
    });
  }

  var iterateUsers = function(usersArray, callback){
    var size = usersArray.length;
    var count = 0;

    function processUser(user) {

      if(count >= size) {
        callback(); // All done!
        return;
      }

      getUser(user, function(err) {
        count++;
        processUser(usersArray[count]);
      });
    }

    processUser(usersArray[count]);

  }

  return {
    getUsuarios: function (req, res){
      users = [];
      var query = {};
      if(req.query.empresa){
        query.empresa = req.query.empresa;
        User.find(query, function(err, usersDb){
          if (err) throw err;

          if(usersDb){
            iterateUsers(usersDb, function(){
              return res.json({success: true, message: "Usuarios encontrados", response: {usuarios: users}});
            });
          }
          else return res.json({success: false, message: "Nenhum usuarios encontrado para essa empresa"});
        })
      }
      else return res.json({success: false, message: "Parâmetro empresa obrigatório"});
    },

    getDesafios: function(req, res){
      // parametros
      var programa_id = req.body.programa_id;
      var empresa = req.body.empresa;

      var desafios = [];

      // pega desafios
      var queryDesafios = {};
      queryDesafios.programa_id = programa_id;
      queryDesafios.tipo = 1;
      var campos = { titulo: 1, semana :1};
      Conteudo.find(queryDesafios, campos, function(err, conteudos) {
        if (err) throw err;

        if (conteudos) {
          for(i=0 ; i<conteudos.length; i++){
            desafios[i] = {};
            desafios[i].nome = conteudos[i].titulo;
            desafios[i].semana = conteudos[i].semana;
            desafios[i].postagens = 0;
          }
        }

        // pega postagens de desafio
        var queryEmpresaDesafio = {};
        queryEmpresaDesafio.empresa_id = empresa;
        var campos = {numero : 1};
        Desafio.find(queryEmpresaDesafio, campos, function(err, desafiosDb) {
          if (err) throw err;

          for(i=0 ; i<desafios.length ; i++){
            for(j=0 ; j<desafiosDb.length ; j++){
              if(desafiosDb[j].numero == desafios[i].semana){
                desafios[i].postagens++;
              }
            }
          }

          return res.json({
              success: true,
              message: "Desafios encontrados",
              response: { desafios }
          });
        });
      });
    },

    getComplementos: function(req, res){
      // parametros
      var programa_id = req.body.programa_id;
      var empresa = req.body.empresa;

      var complementos = [];

      // pega Complementos
      var queryComplementos = {};
      queryComplementos.programa_id = programa_id;
      queryComplementos.tipo = 3;
      campos = {titulo: 1, _id: 1, semana: 1};
      Conteudo.find(queryComplementos, campos, function(err, complementosDb) {
        if (err) throw err;

        if (complementosDb) {
          for(i=0 ; i<complementosDb.length; i++){
            complementos[i] = {};
            complementos[i].nome = complementosDb[i].titulo;
            complementos[i].id = complementosDb[i]._id.toString();
            complementos[i].semana = complementosDb[i].semana;
            complementos[i].comentarios = 0;
            complementos[i].likes = 0;
          }
        }

        var queryEmpresa = {};
        queryEmpresa.id_empresa = empresa;
        campos = {id_post:1};
        // pega comentarios dos complementos
        Comments.find(queryEmpresa, campos, function(err, comentarios) {
          if (err) throw err;

          if (comentarios) {
            for(i=0 ; i<complementos.length ; i++){
              for(j=0 ; j<comentarios.length ; j++){
                if(comentarios[j].id_post == complementos[i].id){
                  complementos[i].comentarios++;
                }
              }
            }
          }

          // pega likes dos complementos
          Likes.find(queryEmpresa, campos, function(err, likes) {
            if (err) throw err;

            if (likes) {
              for(i=0 ; i<complementos.length ; i++){
                for(j=0 ; j<likes.length ; j++){
                  if(likes[j].id_post == complementos[i].id){
                    complementos[i].likes++;
                  }
                }
              }
            }

            return res.json({
                success: true,
                message: "Complementos encontrados",
                response: { complementos }
            });
          });
        });
      });
    },

    getConteudos: function(req, res){
      // parametros
      var programa_id = req.body.programa_id;
      var empresa = req.body.empresa;

      var conteudos = [];

      // pega Conteudos
      var queryConteudos = {};
      queryConteudos.programa_id = programa_id;
      queryConteudos.tipo = 2;
      campos = {titulo: 1, _id: 1, semana: 1};
      Conteudo.find(queryConteudos, campos, function(err, conteudosDb) {
        if (err) throw err;

        if (conteudosDb) {
          for(i=0 ; i<conteudosDb.length; i++){
            conteudos[i] = {};
            conteudos[i].nome = conteudosDb[i].titulo;
            conteudos[i].id = conteudosDb[i]._id.toString();
            conteudos[i].semana = conteudosDb[i].semana;
            conteudos[i].comentarios = 0;
            conteudos[i].likes = 0;
            conteudos[i].visualizacoes = 0;
          }
        }

        var queryEmpresa = {};
        queryEmpresa.id_empresa = empresa;
        campos = {id_post:1};
        // pega comentarios dos conteudos
        Comments.find(queryEmpresa, campos, function(err, comentarios) {
          if (err) throw err;

          if (comentarios) {
            for(i=0 ; i<conteudos.length ; i++){
              for(j=0 ; j<comentarios.length ; j++){
                if(conteudos[i] != null){
                  if(comentarios[j].id_post == conteudos[i].id){
                    conteudos[i].comentarios++;
                  }
                }
              }
            }
          }

          // pega likes dos conteudos
          Likes.find(queryEmpresa, campos, function(err, likes) {
            if (err) throw err;

            if (likes) {
              for(i=0 ; i<conteudos.length ; i++){
                for(j=0 ; j<likes.length ; j++){
                  if(conteudos[i] != null){
                    if(likes[j].id_post == conteudos[i].id){
                      conteudos[i].likes++;
                    }
                  }
                }
              }
            }

            var queryEmpresaUsuarios = {};
            queryEmpresaUsuarios.empresa = empresa;
            campos = {respondidos:1};
            // pega visualizacoes dos conteudos
            User.find(queryEmpresaUsuarios, campos, function(err, usuarios) {
              if (err) throw err;

              if (usuarios) {
                for(i=0 ; i<conteudos.length ; i++){
                  for(j=0 ; j<usuarios.length ; j++){
                    for(k=0 ; k<usuarios[j].respondidos.length ; k++){
                      if(usuarios[j].respondidos[k].conteudo.toString() == conteudos[i].id){
                        conteudos[i].visualizacoes++;
                      }
                    }
                  }
                }
              }

              return res.json({
                  success: true,
                  message: "conteudos encontrados",
                  response: { conteudos }
              });
            });
          });
        });
      });
    },

    getQuizzes: function(req, res){
      // parametros
      var programa_id = req.body.programa_id;
      var empresa = req.body.empresa;

      var quizzes = [];

      // pega quizzes
      var queryquizzes = {};
      queryquizzes.programa_id = programa_id;
      Quiz.find(queryquizzes, function(err, quizzesDb) {
        if (err) throw err;

        if (quizzesDb) {

          for(i=0 ; i<quizzesDb.length; i++){
            quizzes[i] = {};
            quizzes[i].nome = quizzesDb[i].conteudo;
            quizzes[i].id = quizzesDb[i]._id.toString();
            quizzes[i].semana = quizzesDb[i].semana;
            quizzes[i].opcoes = [];
            for(j=0 ; j < quizzesDb[i].opcoes.length ; j++){
              quizzes[i].opcoes[j] = {};
              quizzes[i].opcoes[j].resposta = quizzesDb[i].opcoes[j].resposta;
              quizzes[i].opcoes[j].count = 0;
            }
          }
        }

        // pega respostas dos quizzes
        var queryEmpresaUsuarios = {};
        queryEmpresaUsuarios.empresa = empresa;
        campos = {respondidos:1};
        User.find(queryEmpresaUsuarios, campos, function(err, usuarios) {
          if (err) throw err;

          if (usuarios) {
            for(i=0 ; i<quizzes.length ; i++){
              for(j=0 ; j<usuarios.length ; j++){
                for(k=0 ; k<usuarios[j].respondidos.length ; k++){
                  if(usuarios[j].respondidos[k].conteudo.toString() == quizzes[i].id){
                    if(quizzes[i].opcoes[usuarios[j].respondidos[k].id]){
                      quizzes[i].opcoes[usuarios[j].respondidos[k].id].count++;
                    }
                  }
                }
              }
            }
          }
          return res.json({
              success: true,
              message: "quizzes encontrados",
              response: { quizzes }
          });
        });
      });
    }          
  }
}
