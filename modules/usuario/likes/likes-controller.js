  
module.exports = function (schema,config){

  var Likes = schema.likes;
  var User = schema.user;

  return {
    post: function(req, res){
      var id_post = req.body.id_post;
      var id_empresa = req.body.id_empresa;
      var id_usuario = req.body.id_usuario;
      var programa_id = req.body.programa_id;
      var post_owner_id = req.body.post_owner_id;

      var query = {id_post : id_post, id_usuario: id_usuario};
      var update = { id_post : id_post,id_usuario : id_usuario, id_empresa : id_empresa, programa_id : programa_id};

      Likes.findOneAndUpdate(query, update,{upsert: true, new: true}, function(err){
        if (err) throw err;

        return res.json({success: true, message: 'Like contabilizado'});

      });

    },
    get: function(req,res){
      var query = {};
      query = req.body.empresa;
      Likes.find(query,function(err,likes){
        if (err) throw err;

        if (likes){
          return res.json({success: true, message: 'Likes', response: {likes: likes}});
        }
        else return res.json({success: false, message: 'Likes não encontrados'});
      })
    }
  }
}