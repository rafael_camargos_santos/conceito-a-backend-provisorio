module.exports = function (schema){

  var User = schema.user;

  return {
    getRankGeral: function (req, res){
      var empresa = req.params.empresa;
      var userID = req.params.userID;
      var query = {};
      var arrayRank = [];
      var arrayFinal = [];

      query.empresa = empresa;

      //pega todos os usuários
      User.find(query, function(err, usuarios){
          if (err) throw err;
          
          if(usuarios){
            
            //coloca os usuarios em arrayRank
            for (var score in usuarios){
              arrayRank.push(usuarios[score]);
            }

            //ordena o arrayRank pelo score
            arrayRank.sort(function(a,b){
              return b.score - a.score;
            });

            for (i = 0; i < 20 && arrayRank[i] != null; i++){
              arrayFinal[i] = {nome: arrayRank[i].nome, score: arrayRank[i].score};
            }
            for (i = 0;arrayRank[i] != null; i++){
              if (arrayRank[i]._id == userID){
                scoreUser = i + 1;
              }
            }
            User.findOne({'_id': userID}, function(err, dbUser){
              if (err) throw err;
              if (dbUser){
                dbUser.rankingGeral = scoreUser;
                dbUser.save(function(err){
                  if (err) throw err;
                  return res.json({success: true, message: "Ranking montado e ordenado",
                    response: {Ranking: arrayFinal}});
                })
              }else return res.json({success: false, message: "Não foi possível montar o ranking"});

            })

          }else return res.json({success: false, message: "Não foi possível montar o ranking"});
      });
    },
    getRankDepartamento: function(req, res){
      var empresa = req.params.empresa;
      req.params.departamento;
      var userID = req.params.userID;
      var query = {};
      var arrayRank = [];
      var arrayFinal = [];

      query.departamento = req.params.departamento;
      query.empresa = empresa;
      //pega todos os usuários
      User.find(query, function(err, usuarios){
          if (err) throw err;
           
          if(usuarios){
            
            //coloca os usuarios em arrayRank
            for (var score in usuarios){
              arrayRank.push(usuarios[score]);
            }

            //ordena o arrayRank pelo score
            arrayRank.sort(function(a,b){
              return b.score - a.score;
            });
            for (i = 0; i < 20 && arrayRank[i] != null; i++){
              arrayFinal[i] = {nome: arrayRank[i].nome, score: arrayRank[i].score};
            }
            for (i = 0;arrayRank[i] != null; i++){
              if (arrayRank[i]._id == userID){
                scoreUser = i + 1;
              }
            }
            User.findOne({'_id': userID}, function(err, dbUser){
              if (err) throw err;
              if (dbUser){
                dbUser.rankingDepartamento = scoreUser;
                dbUser.save(function(err){
                  if (err) throw err;
                  return res.json({success: true, message: "Ranking montado e ordenado",
                    response: {Ranking: arrayFinal}});
                })
              }else return res.json({success: false, message: "Não foi possível montar o ranking"});

            })
          }
              
          else return res.json({success: false, message: "Não foi possível montar o ranking"});
        });
    },
    getRankSemanal: function(req,res){
      var query = {};
      var arrayRank = [];
      var arrayFinal = [];

      //pega todos os usuários
      User.find(query, function(err, usuarios){
          if (err) throw err;
          
          if(usuarios){
            
            //coloca os usuarios em arrayRank
            for (var score in usuarios){
              arrayRank.push(usuarios[score]);
            }

            //ordena o arrayRank pelo score
            arrayRank.sort(function(a,b){
              return b.score_semanal - a.score_semanal;
            });

            for (i = 0; i < 20 && arrayRank[i] != null; i++){
              arrayFinal[i] = {nome: arrayRank[i].nome};
            }

            return res.json({success: true, message: "Ranking montado e ordenado",
                 response: {Ranking: arrayFinal}});
          }
            
          else return res.json({success: false, message: "Não foi possível montar o ranking"});
      });
    },
    postRankUsuarios: function(req,res){
      req.params.departamento;
      var query = {};
      var arrayRank = [];
      var arrayDepartamento = [];

      //Classifica pelo ranking geral
      User.find(query, function(err, usuarios){
          if (err) throw err;
           
          if(usuarios){
            
            //coloca os usuarios em arrayRank
            for (var score in usuarios){
              arrayRank.push(usuarios[score]);
            }
            //ordena o arrayRank pelo score
            arrayRank.sort(function(a,b){
              return b.score - a.score;
            });
            //Salva a posição de cada um no ranking
            for (i = 0; arrayRank[i] != null; i++){
              arrayRank[i].rankingGeral = i + 1;
            }
            query.departamento = req.params.departamento;
            //Classifica pelo ranking do departamento
            User.find(query, function(err, users){
                if (err) throw err;
                 
                if(users){
                  
                  //coloca os usuarios em arrayDepartamentos
                  for (var score in usuarios){
                    arrayDepartamento.push(users[score]);
                  }
                  //ordena o arrayDepartamentos pelo score
                  arrayDepartamento.sort(function(a,b){
                    return b.score - a.score;
                  });
                  //Salva a posição de cada um no ranking
                  for (i = 0; arrayDepartamento[i] != null; i++){
                    arrayDepartamento[i].rankingDepartamento = i + 1;
                  }
                  console.log(arrayRank);
                  User.update(function(err){
                    if (err) throw err;
                    return res.json({success: true, message: "Ranking postado"});       
                  })
                  
                }
                    
                else return res.json({success: false, message: "Não foi possível postar o ranking"});
            });
          }
              
        else return res.json({success: false, message: "Não foi possível postar o ranking"});
      });

    },
    getRankUsuario: function(req,res){
      var query = {};

      query._id = req.query.id;

      User.findOne(query, function(err, user){
        if (err) throw err;

        if (user){
          return res.json({success: true, message: "Rankings encontrados", response: {geral: user.rankingGeral,
           departamento: user.rankingDepartamento}});
        }
        else{
          res.json({success: false, message: "Não foi possível encontrar o usuário"});
        }
      })
    }
  }
}