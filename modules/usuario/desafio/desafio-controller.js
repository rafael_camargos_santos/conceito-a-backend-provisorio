module.exports = function(schema) {

    var User = schema.user;
    var Desafio = schema.desafio;
    var ObjectID = schema.mongoose.Types.ObjectId;
    return {
        post: function(req, res) {
            var query = {};
            if (req.body.user_id){
                query._id = req.body.user_id;
                //cria o desafio
                var desafio = new Desafio(req.body);
                //procura o usuário
                User.findOne(query, function(err, dbUser) {
                    if (err) throw err;
                    if (dbUser) {
                        //salva informações do usuário no desafio
                        desafio.user_name = dbUser.nome;
                        desafio.empresa_id = dbUser.empresa;
                        desafio.user_foto = dbUser.foto;
                        desafio.numero = req.body.numero;
                        desafio.unidade = dbUser.unidade;
                        desafio.departamento = dbUser.departamento;
                        desafio.curtidas = 0;
                        desafio.save(function(err) {
                            if (err) throw err;
                            //acumula os pontos
                            query.respondidos = dbUser.respondidos;
                            i = 0;
                            while (query.respondidos[i] != null) {
                                if (query.respondidos[i] == req.body.quiz_id) {
                                    return res.json({ success: false, message: "Usuário já postou este desafio" });
                                }
                                i++;
                            }
                            dbUser.respondidos.push({conteudo: req.body.desafio_id, id: 0});
                            dbUser.score += 100;
                            dbUser.save(function(err) {
                                if (err) throw err;

                                return res.json({ success: true, message: "Pontos e resposta adicionados", response: { pontos: dbUser.score, resposta: desafio } });
                            })
                        })
                    }
                })
            }
            else if (req.body.desafio_id){
                query._id = req.body.desafio_id;
                Desafio.findOne(query, function(err,desafio){
                    if (err) throw err;
                    if (desafio){
                        if (req.body.curtidas) desafio.curtidas = req.body.curtidas;
                        desafio.save(function(err){
                            if (err) throw err;

                            var queryPoints = {_id:desafio.user_id};

                            User.findOne(queryPoints, function(err, dbUser) {
                                if (err) throw err;
                                if (dbUser) {
                                    dbUser.score += 1;
                                    dbUser.save(function(err) {
                                        if (err) throw err;
                                        return res.json({ success: true, message: "Curtidas atualizadas"});
                                    });
                                    
                                } else {
                                    return res.json({ success: true, message: "Curtidas atualizadas"});
                                }
                            });

                            
                        })
                    }
                    else return res.json({ success: false, message: "Curtidas não foram atualizadas"});
                })
            }
        },
        get: function(req, res) {
            var query = {};
            var array = [];
            if (req.query.user_id) query.user_id = req.query.user_id;
            if (req.query.empresa_id) query.empresa_id = req.query.empresa_id;

            Desafio.find(query, function(err, desafios) {
                if (err) throw err;

                if (desafios) {
                    for (var key in desafios) {
                        array.push(desafios[key]);
                    }
                    array.sort(function(a, b) {
                        return b.numero - a.numero;
                    })
                    return res.json({ success: true, message: "Desafios encontrados", response: { desafios: array } });
                } else return res.json({ success: false, message: "Nenhum desafio encontrado para esse usuário" });
            })
        },
        postComentario: function(req,res){
            var query = {};
            if (req.body.post_id){
                query._id = req.body.post_id;
                Desafio.findOne(query,function(err,desafio){
                    if (err) throw err;

                    if (desafio){
                        if (req.body.comentario){
                            desafio.comentarios.push({nome: req.body.nome, comentario: req.body.comentario});
                            desafio.save(function(err){
                                if(err) throw err;

                                return res.json({success: true, message: "Comentario postado", response: {desafio: desafio}});
                            })
                        }else return res.json({success: false, message: "Não foi possível postar o comentario"});
                    }else return res.json({success: false, message: "Não foi possível postar o comentario"});
                })
            }else return res.json({success: false, message: "Não foi possível postar o comentario"});
        },
        editDesafio: function(req,res) {
            var query = {};
            if (req.body.post_id){
                query._id = req.body.post_id;
                Desafio.findOne(query, function(err,desafio){
                    if (err) throw err;

                    if (desafio){
                        if (req.body.imagem) desafio.imagem = req.body.imagem;
                        desafio.texto = req.body.texto;
                        desafio.save(function(err){
                            if (err) throw err;

                            return res.json({success: true, message: "Desafio editado", response: {desafio: desafio}});
                        })
                    }else return res.json({success: false, message: "Desafio não encontrado."});
                })
            }
        }
    }
}
