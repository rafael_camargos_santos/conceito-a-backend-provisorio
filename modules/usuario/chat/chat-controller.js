module.exports = function (schema, config){

  var Chat = schema.chat;
  var Account = schema.account;
  var User = schema.user;
  var Empresa = schema.empresa;
  var ObjectID = schema.mongoose.Types.ObjectId;

  return {
    post: function(req, res){
      var userId = req.body.user_id;
      var isUser = req.body.is_user;

      if(isUser==true){
        var update = {$inc: {nao_respondidas: 1}, $push: {mensagens: {mensagem: req.body.mensagem, is_user: isUser}}};
      }
      else{
        var update = {nao_respondidas: 0, $inc: {nao_visualizadas: 1}, $push: {mensagens: {mensagem: req.body.mensagem, is_user: isUser}}};
      }

      var query = {user_id : userId};
      Chat.findOne(query, function(err, dbChat){
        if (err) throw err;

        if (dbChat){
          Chat.update(query, update, function (err) {
            if (err) throw err;
            return res.json({success: true, message: 'Mensagem enviada'});
          });
        }
      });
    },
    get: function (req, res){
      var userId = new ObjectID(req.params.userID);
      var query = {user_id : userId};

      Chat.findOne(query, function(err, chat){
          if (err) throw err;

          if(!chat){

            // Usuario ainda nao utilizou o chat, criar chat inicial
            var chatInicial = new Chat();
            var queryUser = {_id : userId};
            User.findOne(queryUser, function(err, dbUser){
              if (err) throw err;

              if(!dbUser) return res.json({success: false, message: "Usuario não encontrado"});
              
              else {
                chatInicial.user_id = userId;
                chatInicial.user_email = dbUser.email;
                chatInicial.programa_id = dbUser.programa_id;
                chatInicial.user_photo = dbUser.foto;
                chatInicial.conceito_a_photo = config.fotoPrograma(dbUser.programa_id);
                chatInicial.nao_visualizadas = 0;
                chatInicial.nao_respondidas = 0;
                var nomePrograma = config.nomePrograma(dbUser.programa_id);
                chatInicial.mensagens = [{mensagem: "Bem vindo ao suporte do " + nomePrograma, is_user: false}];

                Empresa.findOne({_id : dbUser.empresa}, function(err, dbEmpresa){
                  if (err) throw err;

                  if(dbEmpresa) chatInicial.empresa = dbEmpresa.nome;

                  chatInicial.save(function (err) {
                    if (err) throw err;

                    return res.json({success: true, message: "Mensagens encontradas", 
                      response: {chat: chatInicial}});
                  });
                });

                
              }
            });
          }

          else {
            var update = {nao_visualizadas: 0};

            Chat.update(query, update, function (err) {
              if (err) throw err;
              
              return res.json({success: true, message: "Mensagens encontradas", response: {chat: chat}}); 
            });
          }        
      });
    },
    getNaoVisualizadas: function (req, res){
      var userId = new ObjectID(req.params.userID);
      var query = {user_id : userId};

      Chat.findOne(query, function(err, chat){
          if (err) throw err;

          if(!chat){
            return res.json({success: true, message: "Número de mensagens não visualizadas", 
              response: {nao_visualizadas: 0}});
          }

          else {
            return res.json({success: true, message: "Número de mensagens não visualizadas", 
              response: {nao_visualizadas: chat.nao_visualizadas}}); 
          }        
      });
    }
  }
}