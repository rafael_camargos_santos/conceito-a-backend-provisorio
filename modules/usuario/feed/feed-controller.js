module.exports = function(schema) {

    var Conteudo = schema.conteudo;
    var Quiz = schema.quiz;
    var Likes = schema.likes;
    var Comments = schema.comments;
    var Empresa = schema.empresa;

    var ObjectID = schema.mongoose.Types.ObjectId;

    var countById = function(id, array) {
        var array2 = [];

        for (var index in array) {
            var object = array[index];
            if (object.id_post.equals(id)) {

                array2.push(object);
            }
        }
        return array2.length;
    }

    return {
        get: function(req, res) {
            var queryQuiz = {};
            var queryConteudo = {};
            var queryCommentLike = {};
            var array = [];
            var final = {};
            var emp = {};
            //pega todos os quiz

            if (req.query.programa_id != null) {
                if (req.query.programa_id) queryQuiz.programa_id = req.query.programa_id;
                if (req.query.programa_id) queryConteudo.programa_id = req.query.programa_id;
                if (req.query.programa_id) queryCommentLike.programa_id = req.query.programa_id;
            }
            if (req.query.emp_id != null) {
                if (req.query.emp_id) emp._id = req.query.emp_id;
            }
            if (req.query.semana != null) {
                if (req.query.semana) queryQuiz.semana = req.query.semana;
                if (req.query.semana) queryConteudo.semana = req.query.semana;
            }
            if (req.query.programa_id == null || req.query.emp_id == null){
                return res.json({ success: false, message: "Parâmetros nulos" });
            }
            Quiz.find(queryQuiz, function(err, quiz) {

                if (err) throw err;

                if (!quiz) {
                    return res.json({ success: false, message: "Nada foi encontrado" });
                }
                final.feed = [];
                //coloca os quiz em arrayQuiz
                for (var index in quiz) {

                    var modelQuiz = quiz[index]._doc;
                    final.feed.push(modelQuiz);
                }

                //pega todos os conteudos
                Conteudo.find(queryConteudo, function(err, conteudo) {
                    if (err) throw err;

                    if (conteudo) {
                        //final.push(conteudo);
                        //coloca os conteudos em arrayConteudo
                        for (var index in conteudo) {
                            var modelContent = conteudo[index]._doc
                            final.feed.push(modelContent);
                        }

                        //ordena o array pelo dia
                        final.feed.sort(function(a, b) {
                            if(a.dia == b.dia){
                                if(a.tipo == 2){
                                    return 1;
                                }
                            }
                            return b.dia - a.dia;
                        })

                        Likes.find(queryCommentLike, function(err, likes) {
                            if (err) throw err;
                            if (likes) {
                                final.likes = likes;
                                for (var index in final.feed) {
                                    var content = final.feed[index];
                                    if (content.tipo == 2 || content.tipo == 3) {
                                        content.curtidas = Number;
                                        content.curtidas = countById(content._id, likes);
                                    }
                                }
                            }

                            Comments.find(queryCommentLike, function(err, comentarios) {

                                if (err) throw err;

                                if (comentarios) {
                                    final.comentarios = comentarios;
                                    Empresa.findOne(emp, function(err, company) {
                                        if (err) throw err;

                                        if (company) {
                                            console.dir(company.nome);
                                            return res.json({
                                                success: true,
                                                message: "Quizzes e Conteudos encontrados e ordenados",
                                                response: { feed: final, empresa_inicio: company.data }
                                            });
                                        }
                                    });
                                } 
                            });
                        });

                    } else return res.json({ success: false, message: "Quizzes encontrados e ordenados, conteudos não encontrados" });

                });


            });
        }
    }
}
