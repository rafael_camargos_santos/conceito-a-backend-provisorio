module.exports = function (schema,config){

  var Comments = schema.comments;
  return {
    post: function(req, res){
      var comment = new Comments(req.body);


      comment.save(function(err, comentario){
        if (err) throw err;
        return res.json({success: true, message: 'Comentario adicionado!', response: {comentario: comentario}});
      });

    },
    get: function(req,res){
      var query = {};
      query = req.body.empresa;
      Comments.find(query,function(err,comments){
        if (err) throw err;

        if (comments){
          return res.json({success: true, message: 'Comentarios', response: {comments: comments}});
        }
        else return res.json({success: false, message: 'Comentarios não encontrados'});
      })
    }
  }
}