module.exports = function (schema){

  var User = schema.user;
  var Conteudo = schema.conteudo;
  var ObjectID = schema.mongoose.Types.ObjectId;

  return {
    //precisa dar um jeito de ver se a pessoa viu o conteudo para contar os pontos
    post: function(req,res){
      var query = {};
      
      query._id = req.body.user_id;
      //busca o usuário
      User.findOne(query, function(err,dbUser){
        if (err) throw err;

        if (dbUser){
          //verifica se o conteudo foi visto
          if (req.body.answer){
            query.respondidos = dbUser.respondidos;
            i = 0;
            while (query.respondidos[i] != null){
              if (query.respondidos[i].conteudo == req.body.conteudo_id){
                return res.json({success: false, message: "Usuário já viu o conteudo"});
              }
              i++;
            }
            dbUser.respondidos.push({conteudo: req.body.conteudo_id, id: -1});
            dbUser.score += 50;
            dbUser.save(function(err){
              if (err) throw err;

              return res.json({success: true, message: "Pontos adicionados", response: {pontos: dbUser.score}});
            })
          }
          else{
            return res.json({success: false, message: "Usuário não viu o conteúdo"});
          }
        }
        else return res.json({succes: false, message: "Usuário não encontrado"});
      })
    },
    get: function (req, res){
      var query = {};
      if(req.query.id) query._id = new ObjectID(req.query.id);

      Conteudo.find(query, function(err, conteudos){
          if (err) throw err;

          if(conteudos){
            return res.json({success: true, message: "Conteudos encontrados", response: {conteudos: conteudos}});
          }
          else return res.json({success: false, message: "Nenhum conteudo encontrado para esse usuário"});
      });
    }
  }
}