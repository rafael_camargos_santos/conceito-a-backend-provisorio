module.exports = function (schema){

  var User = schema.user;
  var Quiz = schema.quiz;
  var ObjectID = schema.mongoose.Types.ObjectId;

  return {
    post: function(req,res){
      var query = {};      
      query._id = req.body.user_id;
      //busca o usuário
      User.findOne(query, function(err,dbUser){
        if (err) throw err;

        if (dbUser){
          //verifica se o usuário respondeu o quiz
          if (req.body.answer){
            query.respondidos = dbUser.respondidos;
            i = 0;
            while (query.respondidos[i]){
              if (query.respondidos[i].conteudo == req.body.quiz_id){
                return res.json({success: false, message: "Usuário já respondeu o quiz"});
              }
              i++;
            }
            //precisa ajustar com a data
            dbUser.respondidos.push({conteudo: req.body.quiz_id, id: req.body.resp_id});
            var diaCerto = req.body.dia_certo;
            console.log(diaCerto);
            if(diaCerto){
              dbUser.score += 50;
            }
            else dbUser.score += 20;
            dbUser.save(function(err){
              if (err) throw err;

              return res.json({success: true, message: "Pontos adicionados", response: {pontos: dbUser.score}});
            })         
          }
          else{
            return res.json({success: false, message: "Usuário não respondeu o quiz"});
          }
        }
        else {
          return res.json({succes: false, message: "Usuário não encontrado"});
        }
      })
    },
    get: function (req, res){
      var query = {};
      if(req.query.id){
        query._id = req.query.id;
        Quiz.findOne(query, function(err,quiz){
          if (err) throw err;

          if(quiz){
            return res.json({success: true, message: "Quiz encontrado", response: {quiz: quiz}});
          }
          else return res.json({success: false, message: "Nenhum quiz encontrado para esse usuário"});
        })

      }
      else{
        Quiz.find(query, function(err, quiz){
            if (err) throw err;

            if(quiz){
              return res.json({success: true, message: "Quizzes encontrados", response: {quizzes: quiz}});
            }
            else return res.json({success: false, message: "Nenhum quiz encontrado para esse usuário"});
        })
      }
    }  }
}