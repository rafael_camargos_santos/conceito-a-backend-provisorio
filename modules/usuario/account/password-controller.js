module.exports = function (schema, bcrypt, config, crypto){

  var Account = schema.account;
  var transporter = config.transporter();
  const saltRounds = config.saltRounds();

  var sendEmail = function (email, password, callback){
    // setup e-mail data with unicode symbols
    var mailOptions = {
        to: email, // list of receivers
        subject: 'Nova senha Conceito A',
         // Subject line
        text: 'Sua senha foi alterada.\n' +'A nova senha é: ' + password +'\n\nwww.conceitoa.com.br', // plaintext body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            return console.log(error);
        }
        callback(error);
    });
  }


  var changePassword = function(password, newPassword, mail, programa_id, res){
    var query = {};
    
    query.email = mail;
    query.programa_id = programa_id
    //Encontra a conta a ser modificada no banco de dados
    Account.findOne(query, function(err,acc){
      //Verifica se o password digitado bate com o da conta
      bcrypt.compare(password, acc.password, function(err,match){
        if (err) throw err;

        if (match){
          bcrypt.hash(newPassword, saltRounds, function(err, hash){
            newPassword = hash;
            //coloca o novo password na acc
            acc.password = newPassword;
            acc.save(function(err){
              if (err) throw err;
              return res.json({success: true, message: "Senha modificada com sucesso", response: {password: acc.password}});
            })
          });
        }
        //retorna false se o password não bater
        else return res.json({success: false, message: "Não foi possível modificar a senha"});
      });
    });
  }

  var resetPassword = function(body, res){
    var query = {};
    query = body;
    if (body) {
      Account.findOne(query, function(err,acc){
      if (err) throw err;

      if (acc){
        //Cria uma nova senha
        var password = Math.floor(Math.random() * (9999999 - 100000 + 1)) + 100000;
        var md5 = crypto.createHash('md5').update(password.toString()).digest("hex");
        console.dir("Encontrou a acc");
        //encrypta a nova senha
        bcrypt.hash(md5, saltRounds, function(err,hash){
          if (err) throw err;
          acc.password = hash;
          acc.save(function(err){
            if (err) throw err;
            sendEmail(acc.email, password, function(err){
              if (err) throw err;
              return res.json({success: true, message: "Senha resetada com sucesso", response: {password: acc.password}});
            })
           })

        })
      }
      else{
        return res.json({success: false, message: "Não foi possível resetar a senha"});
      }
    })
    }
    else return res.json({success: false, message: "Não foi possível encontrar um email"});
  }

  
  return {
    post: function (req, res) {
      changePassword(req.body.password, req.body.newPassword, req.body.email, req.body.programa_id, res);
    },
    reset: function(req,res){
      resetPassword(req.body,res);
    }
  }
}