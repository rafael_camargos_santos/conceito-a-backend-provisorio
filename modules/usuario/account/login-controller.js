module.exports = function (schema, bcrypt, jwt, config){

  var Account = schema.account;
  var User = schema.user;

   var postUsersRanking = function (departamento, usuario){
      var query = {};
      var arrayRank = [];
      var arrayDepartamento = [];

      query.empresa = usuario.empresa;
      //Classifica pelo ranking geral
      User.find(query, function(err, usuarios){
          if (err) throw err;
           
          if(usuarios){
            
            //coloca os usuarios em arrayRank
            for (var score in usuarios){
              arrayRank.push(usuarios[score]);
            }
            //ordena o arrayRank pelo score
            arrayRank.sort(function(a,b){
              return b.score - a.score;
            });
            //Salva a posição de cada um no ranking
            for (i = 0; arrayRank[i] != null; i++){
              arrayRank[i].rankingGeral = i + 1;
            }
            query.departamento = departamento;
            //Classifica pelo ranking do departamento
            User.find(query, function(err, users){
                if (err) throw err;
                 
                if(users){
                  
                  //coloca os usuarios em arrayDepartamentos
                  for (var score in usuarios){
                    arrayDepartamento.push(users[score]);
                  }
                  //ordena o arrayDepartamentos pelo score
                  arrayDepartamento.sort(function(a,b){
                    return b.score - a.score;
                  });
                  //Salva a posição de cada um no ranking
                  for (i = 0; arrayDepartamento[i] != null; i++){
                    arrayDepartamento[i].rankingDepartamento = i + 1;
                  }
                  var update = {};
                  User.findOne(usuario, function(err, dbUser){
                    if (err) throw err;
                    if (dbUser){
                      for (i = 0; arrayDepartamento[i] != null; i++){
                        if (arrayDepartamento[i].nome == dbUser.nome){
                          dbUser.rankingDepartamento = arrayDepartamento[i].rankingDepartamento;
                        }
                      }
                      for (i = 0; arrayRank[i] != null; i++){
                        if (arrayRank[i].nome == dbUser.nome){
                          dbUser.rankingGeral = arrayRank[i].rankingGeral;
                        }
                      }
                      dbUser.save(function(err){
                        if (err) throw err;
                        return true;
                      })
                    }
                    else return false;
                  })
                }
                    
                else return false;
            });
          }
              
        else return false;
      });
  }
  var setPrimeiroLogin = function(dbUser){
    dbUser.primeiroLogin = false;
    dbUser.save(function(err){
      if (err) throw err;

    });
  }

  return {
    post: function (req, res) {
      var email = req.body.email;
      var programa_id = req.body.programaID;

      // Procura usuario no banco
      var query = {email: email, programa_id: programa_id};

      Account.findOne(query, function(err, account) {

        if (err) throw err;

        if (!account) {
          // account nao encontrado
          return res.json({success: false, message: 'Falha na autenticação. Conta não encontrada.'});

        } 
        else if (account) {
          // compara passwords (com criptografia)
          bcrypt.compare(req.body.password, account.password, function(err, match) {
            if(!match){
              // passwords nao batem
              return res.json({success: false, message: 'Falha na autenticação. Senha incorreta.'});
            }
            else{
                // account e password batem
              var tokenSecret;
              if(account.admin==true) tokenSecret = config.apiSecretAdmin();
              else tokenSecret = config.apiSecret();
              // cria token
              var token = jwt.sign(account.toObject(), tokenSecret);

              User.findOne(query, function(err,dbUser){
                  if (err) throw err;

                  if(dbUser){
                    if(dbUser.ativo){
                      postUsersRanking(dbUser.departamento, dbUser);
                      dbUser.last_login = new Date();
                      // copia objeto usuario para passar o parametro primeiroLogin antes da alteracao
                      var userResposta = new User(dbUser);
                      if(dbUser.primeiroLogin) setPrimeiroLogin(dbUser);
                      dbUser.save(function(err){
                        if (err) throw err;
                          return res.json({
                          success : true,
                          message : 'sucesso no login.',
                          token : token,
                          usuario: userResposta
                        });
                      })
                    }
                    else return res.json({success: false, message: 'Falha na autenticação. Usuário inativo.'});
                  }
                  else return res.json({success: false, message: 'Falha na autenticação. Usuário não encontrado.'});
              })
            }
          });
        }
      });
    }
 }
}