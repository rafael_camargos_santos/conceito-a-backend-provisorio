module.exports = function (schema, bcrypt){

  var User = schema.user;
  var ObjectID = schema.mongoose.Types.ObjectId;

    var postUsersRanking = function (departamento, usuario){
      var query = {};
      var arrayRank = [];
      var arrayDepartamento = [];
      query.empresa = usuario.empresa
      //Classifica pelo ranking geral
      User.find(query, function(err, usuarios){
          if (err) throw err;
           
          if(usuarios){
            
            //coloca os usuarios em arrayRank
            for (var score in usuarios){
              arrayRank.push(usuarios[score]);
            }
            //ordena o arrayRank pelo score
            arrayRank.sort(function(a,b){
              return b.score - a.score;
            });
            //Salva a posição de cada um no ranking
            for (i = 0; arrayRank[i] != null; i++){
              arrayRank[i].rankingGeral = i + 1;
            }
            query.departamento = departamento;
            //Classifica pelo ranking do departamento
            User.find(query, function(err, users){
                if (err) throw err;
                 
                if(users){
                  
                  //coloca os usuarios em arrayDepartamentos
                  for (var score in usuarios){
                    arrayDepartamento.push(users[score]);
                  }
                  //ordena o arrayDepartamentos pelo score
                  arrayDepartamento.sort(function(a,b){
                    return b.score - a.score;
                  });
                  //Salva a posição de cada um no ranking
                  for (i = 0; arrayDepartamento[i] != null; i++){
                    arrayDepartamento[i].rankingDepartamento = i + 1;
                  }
                  var update = {};
                  User.findOne(usuario, function(err, dbUser){
                    if (err) throw err;
                    if (dbUser){
                      for (i = 0; arrayDepartamento[i] != null; i++){
                        if (arrayDepartamento[i]._id == dbUser._id){
                          dbUser.rankingDepartamento = arrayDepartamento[i].rankingDepartamento;
                        }
                      }
                      for (i = 0; arrayRank[i] != null; i++){
                        if (arrayRank[i]._id == dbUser._id){
                          dbUser.rankingGeral = arrayRank[i].rankingGeral;
                        }
                      }
                      dbUser.save(function(err){
                        if (err) throw err;
                        return true;
                      })
                    }
                    else return false;
                  })
                }
                    
                else return false;
            });
          }
              
        else return false;
      });
  }
  return {
    createUser: function (user, callback) {
      user.email = user.email.toLowerCase();

      var query = {email : user.email, programa_id : user.programa_id};

      User.findOne(query, function(err, dbUser) {
        if (err) throw err;

        if (dbUser){
            // Email bate com algum usuario da base do mesmo programa
            callback(false);
          }
        // email e usuario disponiveis
        else{
          user.score = 0;
          user.nome = (user.email.split("@"))[0];
          user.ativo = true;
          var userDB = new User(user);
          userDB.save(function (err) {
            if (err) throw err;

            callback(userDB._id);
          });
        }
      });
    },
    getUserProfile: function (user, res){
      user.params.id;
      var query = {};
      query._id = user.params.id;

      User.findOne(query, function(err, dbUser){
          if (err) throw err;

          if(dbUser){
            postUsersRanking(dbUser.departamento, dbUser);
            return res.json({success: true, message: "Perfil encontrado", response: {perfil: dbUser}});
          }
          else return res.json({success: false, message: "Nenhum perfil encontrado"});
      })
    },
    
    updateUser: function(req, res){
      var query = {};
      var update = req.body;
      if(req.body._id){
        query._id = req.body._id;
        User.findOneAndUpdate(query,update, function(err,dbUser){
          if (err) throw err;
          return res.json({success: true, message: "Usuario atualizado", response: {usuario: dbUser}});
        })
      }else return res.json({success: false, message: "Faltam parâmetros"});
    },
    getUserPorEmpresa: function(req,res){
      var query = {};
      var tempo_atual = Date.now();
      if (req.query.id) query.empresa = req.query.id;

      User.find(query, function(err,dbUser){
        if (err) throw err;

        if (dbUser){
          for (var index in dbUser){
            if (((tempo_atual - dbUser[index].last_login) / (1000 * 60 * 60 * 24)) > 14){
              dbUser[index]._doc.alert = '!';
            }else dbUser[index]._doc.alert = '.';
           }
          return res.json({success: true, message: "Usuarios da empresa", response: {usuarios: dbUser}});
        }
        else{
          return res.json({success: false, message: "Usuarios não encontrados"});
        }
      })
    }
  }

}
