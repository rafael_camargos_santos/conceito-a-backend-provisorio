module.exports = function (schema){

  var Empresa = schema.empresa;
  var ObjectID = schema.mongoose.Types.ObjectId;

  return {
    get: function (req, res){
      var query = {};

      if (req.query.id) query._id = req.query.id;

      Empresa.find(query, function(err, empresa){
          if (err) throw err;

          if(empresa){
            return res.json({success: true, message: "Empresa encontrada", response: {empresa: empresa}});
          }
          else return res.json({success: false, message: "Nenhuma empresa encontrada para este usuário"});
      })
    }
  }
}